package com.ph.jumpdigital.alet.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.NotificationsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by luigigo on 12/10/15.
 */
public class RatingActivity extends Activity {

    Dialog dialog;
    RatingBar rating;
    EditText edtComment;
    TextView txtOrderId;
    static String strComment, strRating, strOrderId;
    JSONObject jsonObject;
    String TAG = "RatingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ratings_layout);

        showRatingDialog();

    }

    public void showRatingDialog() {
        dialog = new Dialog(RatingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_rating);
        dialog.setCancelable(true);

        rating = (RatingBar) dialog.findViewById(R.id.ratingBar);
        LayerDrawable stars = (LayerDrawable) rating.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.theme_color), PorterDuff.Mode.SRC_ATOP);
//        stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.theme_color), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_ATOP);
        edtComment = (EditText) dialog.findViewById(R.id.edtComment);
        txtOrderId = (TextView) dialog.findViewById(R.id.txtOrderId);
        txtOrderId.setText(NotificationsModel.getRatingsOrderId());

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                dialog.dismiss();
                RatingActivity.this.finish();
            }
        });

        Button btnRating = (Button) dialog.findViewById(R.id.rateBtn);
        btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strComment = edtComment.getText().toString();
                strRating = String.valueOf(rating.getRating());
                strOrderId = txtOrderId.getText().toString();
                postRate(strComment, strRating, strOrderId);
            }
        });

        dialog.show();
    }

    private void postRate(String comment, String rating, String orderid) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("order_id", orderid);
        params.put("comment", comment);
        params.put("rating", rating);
        Log.i(TAG, params.toString());

        String url = Constants.apiURL + Constants.rating;
        Constants.volleyTAG = "rating";
        VolleyRequest request = new VolleyRequest(this,
                Constants.postMethod, url,
                params, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                RatingActivity.this, true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(this);

    }

    private Response.ErrorListener createRequestErrorListener() {

        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Volley-Error" + error.getLocalizedMessage() + error.getMessage());
                try {
                    if (error.networkResponse != null) {
                        CustomUtils.hideDialog();
                        String responseBody = new String(error.networkResponse.data, "UTF-8");
                        jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");

                        String errorCode = jsonObject.getString("error_code");
                        String errorDesc = jsonObject.getString("error_description");

                        Log.i("errorCode:", errorCode);
                        Log.i("errorDesc:", errorDesc);
                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();
                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                Toast.makeText(RatingActivity.this, currentDynamicValue, Toast.LENGTH_SHORT).show();
                            }
                            Log.i("Error-JSONResponse", "" + errorResult.toString());
                        }

                        Log.e("jsonObject", errorResult + "");
                    } else {
                        Toast.makeText(RatingActivity.this, "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.e("errorEncoding", "" + e);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Volley-Success" + response.toString());
                CustomUtils.hideDialog();
                Toast.makeText(RatingActivity.this, "Thank you for rating our service", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        };
    }

}
