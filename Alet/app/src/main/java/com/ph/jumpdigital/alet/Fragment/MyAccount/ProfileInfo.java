package com.ph.jumpdigital.alet.Fragment.MyAccount;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Fragment.OrderPlacement.OrderNewAddressFragment;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.MultipartRequest;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.SharedPref;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.AlcoholTypeModel;
import com.ph.jumpdigital.alet.model.UserModel;
import com.pkmmte.view.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by luigigo on 9/22/15.
 */
public class ProfileInfo extends Fragment implements View.OnClickListener {

    public static int page = 0;
    public static JSONObject jsonObject, jsonObj;
    static JSONArray jsonArr;
    static AlcoholTypesAdapter adapter;
    static UserModel userModel = new UserModel();
    ArrayList<HashMap<String, String>> arraylist;
    int[] layouts, buttons;
    View mainView;
    LayoutInflater _layoutInflater;
    ViewGroup _container;
    ListView listview, lvAlcoholTypes;
    Button btnNext, btnAddAddress;
    CircularImageView imageProfile;
    Spinner spnGender;
    EditText edtFirstname, edtLastname, edtMobile;
    EditText edtCurrentPass, edtNewPass, edtConfirmPass;
    String firstname, lastname, gender, mobile;
    String currentPassword, newPassword, confirmPassword;
    HashMap<String, String> params;
    String url;

    private int PICK_IMAGE_REQUEST = 1, RESULT_OK = -1, REQUEST_CROP_ICON = 2;
    private Bitmap bitmap;
    Intent i;
    SharedPref sharedPref;
    Uri filePath;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(false);
        //getAddress();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this._layoutInflater = inflater;
        this._container = container;
        // Toast.makeText(getActivity(), "Token: " + sharedPref.getAUTHToken(), Toast.LENGTH_SHORT).show();
        sharedPref = new SharedPref(getActivity());
        layouts = new int[]{
                R.layout.profile_info,
                R.layout.profile_changeemail,
                R.layout.profile_preferredalcohol,
                R.layout.profile_changepassword,
                R.layout.profile_saved_address
        };

        buttons = new int[]{
                R.id.btnNext1,
                R.id.btnNext2,
                R.id.btnNext3,
                R.id.btnNext4
        };

        mainView = _layoutInflater.inflate(layouts[page], _container, false);
        setupPage();
        return mainView;

    }

    public void setupPage() {
        if (page < 4) {
            btnNext = (Button) mainView.findViewById(buttons[page]);
            btnNext.setOnClickListener(this);
        }

        if (page == 0) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Profile Info");
            edtFirstname = (EditText) mainView.findViewById(R.id.edtFirstname);
            edtLastname = (EditText) mainView.findViewById(R.id.edtLastname);
            spnGender = (Spinner) mainView.findViewById(R.id.spnGender);
            edtMobile = (EditText) mainView.findViewById(R.id.edtMobile);
            imageProfile = (CircularImageView) mainView.findViewById(R.id.profile_image);
            imageProfile.setOnClickListener(this);

            getUserInfo();
        } else if (page == 2) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Preferences");
            lvAlcoholTypes = (ListView) mainView.findViewById(R.id.lvAlcoholTypes);

        } else if (page == 3) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Change Password");
            edtCurrentPass = (EditText) mainView.findViewById(R.id.edtCurrentPassword);
            edtNewPass = (EditText) mainView.findViewById(R.id.edtNewPassword);
            edtConfirmPass = (EditText) mainView.findViewById(R.id.edtConfirmPassword);

        } else if (page == 4) {
            btnAddAddress = (Button) mainView.findViewById(R.id.btnAddAddress);
            btnAddAddress.setOnClickListener(this);
            getAddress();
            setHasOptionsMenu(true);
            CustomUtils.showDialog(getActivity());

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnNext1:
                Log.i("Firstname: ", edtFirstname.getText().toString());
                Log.i("Lastname: ", edtLastname.getText().toString());
                Log.i("Gender: ", spnGender.getSelectedItem().toString());
                Log.i("Mobile: ", edtMobile.getText().toString());

                firstname = edtFirstname.getText().toString();
                lastname = edtLastname.getText().toString();
                gender = spnGender.getSelectedItem().toString();
                mobile = edtMobile.getText().toString();

                if (firstname.isEmpty() || lastname.isEmpty() || mobile.isEmpty()) {
                    CustomUtils.toastMessage(getActivity(), "One of the field is empty");
                    edtFirstname.setError("One of the field is empty");
                    edtLastname.setError("One of the field is empty");
                    edtMobile.setError("One of the field is empty");

                } else {
                    userModel.setFirstname(firstname);
                    userModel.setLastname(lastname);
                    userModel.setGender(gender);
                    userModel.setMobile(mobile);

                    params = new HashMap<String, String>();
                    params.put("first_name", firstname);
                    params.put("last_name", lastname);
                    params.put("gender", UserModel.getGender());
                    params.put("mobile_number", mobile);

//
                    url = Constants.apiURL + Constants.updateUserProfile;
                    Constants.volleyTAG = "updateinfo";
//                    VolleyRequest request = new VolleyRequest(getActivity(),
//                            Constants.putMethod, url,
//                            (HashMap<String, String>) params, null,
//                            createRequestSuccessListener(),
//                            createRequestErrorListener(),
//                            getActivity(), true, Constants.volleyTAG);
//                    AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);

                    if (bitmap == null) {
                        imageProfile.buildDrawingCache();
                        bitmap = imageProfile.getDrawingCache();
                    }

                    CustomUtils.showDialog(getActivity());
                    MultipartRequest mr = new MultipartRequest(getActivity(), url, params, createRequestErrorListener(), createRequestSuccessListener(), bitmap);
                    AppController.getInstance().addToRequestQueue(mr, Constants.volleyTAG);
                }


                break;

            case R.id.profile_image:
                showFileChooser();

                break;

            case R.id.btnNext3:

                ArrayList<AlcoholTypeModel> atm = adapter._arraylist;
                JSONArray jsonArr = new JSONArray();
                JSONObject jsonParams = new JSONObject();

                try {
                    for (int i = 0; i < atm.size(); i++) {
                        AlcoholTypeModel alcoholTypeModel = atm.get(i);
                        if (alcoholTypeModel.isSelected()) {
                            Log.i("ID: ", alcoholTypeModel.getId());
                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put("product_type_id", alcoholTypeModel.getId());
                            jsonArr.put(jsonObj);
                        }
                    }

                    if (jsonArr.length() > 0) {
                        Log.i("JSONArray", jsonArr.toString());
                        jsonParams.accumulate("alcohol_type", jsonArr);

                        url = Constants.apiURL + Constants.preferred_alcohol_types;
                        Log.i("URL: ", url);
                        Constants.volleyTAG = "preferred_alcohol";
                        VolleyRequest request = new VolleyRequest(getActivity(),
                                Constants.postMethod, url,
                                null, jsonParams,
                                createRequestSuccessListener(),
                                createRequestErrorListener(),
                                getActivity(), true, Constants.volleyTAG);
                        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
                        CustomUtils.showDialog(getActivity());
                    } else {
                        Log.i("No value", jsonArr.toString());
                        CustomUtils.toastMessage(getActivity(), "Nothing to update");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.btnNext4:
                currentPassword = edtCurrentPass.getText().toString();
                newPassword = edtNewPass.getText().toString();
                confirmPassword = edtConfirmPass.getText().toString();

                if (currentPassword.isEmpty() || newPassword.isEmpty() || confirmPassword.isEmpty()) {
                    CustomUtils.toastMessage(getActivity(), "One of the field is empty");
                } else if (!confirmPassword.equals(newPassword)) {
                    CustomUtils.toastMessage(getActivity(), "Password does not match");

                } else {
                    params = new HashMap<String, String>();
                    params.put("old_password", currentPassword);
                    params.put("new_password", newPassword);
                    params.put("password_confirmation", confirmPassword);

                    url = Constants.apiURL + Constants.changePassword;
                    Log.i("URL:", url);
                    Constants.volleyTAG = "update";
                    VolleyRequest request = new VolleyRequest(getActivity(),
                            Constants.putMethod, url,
                            (HashMap<String, String>) params, null,
                            createRequestSuccessListener(),
                            createRequestErrorListener(),
                            getActivity(), true, Constants.volleyTAG);
                    AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
                    CustomUtils.showDialog(getActivity());
                    // Request.postRequest(url, (HashMap<String, String>) params, getActivity());
                    // Toast.makeText(getActivity(), "4 " + sharedPref.getAUTHToken(), Toast.LENGTH_SHORT).show();

                }
                break;

            case R.id.btnAddAddress:
                OrderNewAddressFragment.classIndicator = "fromProfileInfoFragment";
                CustomUtils.loadFragment(new OrderNewAddressFragment(), true, getActivity());
                break;
        }

        Log.i("PAGE", "" + page);

    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private void getAlcoholTypes() {
        url = Constants.apiURL + Constants.getAllProductTypes;
        Log.i("URL: ", url);
        Constants.volleyTAG = "getalcoholtypes";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());
    }

    public void getAddress() {
        listview = (ListView) getActivity().findViewById(R.id.reusableListView);
        url = Constants.apiURL + Constants.displayAddress;
        Constants.volleyTAG = "getaddress";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
    }

    public void getUserInfo() {
        url = Constants.apiURL + Constants.showSpecificUser;
        Constants.volleyTAG = "getuserinfo";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onStart() {
        if (page == 4) {
            getAddress();
        }
        if (page == 2) {
            getAlcoholTypes();
        }
        super.onStart();
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());


                            if (Constants.volleyTAG.equals("getuserinfo")) {
                                Log.i("GETUSERINFO1", Constants.volleyTAG);
                                String errorResult = jsonObject.getJSONObject("error").getString("message");

                                if (errorResult.equals("Invalid user")) {
                                    //CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                    Toast.makeText(getActivity(), errorResult, Toast.LENGTH_SHORT).show();
                                    Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                    gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    ActivityCompat.finishAffinity(getActivity());
                                    getActivity().finish();
                                    startActivity(gotoLogin);
                                } else {
                                    CustomUtils.hideDialog();
//                                        CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                                }
                            } else {

                                JSONArray errorResult = jsonObject.getJSONArray("error");
                                String errorCode = jsonObject.getString("error_code");
                                String errorDesc = jsonObject.getString("error_description");

                                Log.i("errorCode:", errorCode);
                                Log.i("errorDesc:", errorDesc);

                                //get errror by index
                                for (int i = 0; i < errorResult.length(); i++) {
                                    JSONObject errorObj = errorResult.getJSONObject(i);
                                    Iterator keys = errorObj.keys();

                                    while (keys.hasNext()) {
                                        // LOOP TO GET DYNAMIC KEY
                                        String currentDynamicKey = (String) keys.next();
                                        Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                        // GET THE VALUE OF DYNAMIC KEY
                                        String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                        Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                        Log.i("Error-DynamicValue", "" + currentDynamicValue);


                                        if (currentDynamicValue.equals("You must login first")) {
                                            //CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                            Toast.makeText(getActivity(), currentDynamicValue, Toast.LENGTH_SHORT).show();
                                            Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                            gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            ActivityCompat.finishAffinity(getActivity());
                                            getActivity().finish();
                                            startActivity(gotoLogin);
                                        } else {
                                            CustomUtils.hideDialog();
//                                        CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                                        }

                                    }
                                    Log.i("Error-JSONResponse", "" + errorResult.toString());
                                }

                                Log.e("jsonObject", errorResult + "");
                            }
                        } else {
                            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e.getMessage());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                CustomUtils.hideDialog();
            }
//            }

        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.i("Success", response.toString());

                if (response != null) {
                    if (Constants.volleyTAG.equals("getaddress")) {
                        // GETTING ADDRESS
                        arraylist = new ArrayList<HashMap<String, String>>();
                        try {
                            jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));

                            JSONArray user_add = jsonObject.getJSONArray("user_address");
                            Constants.status = response.getBoolean("status");

                            if (user_add.length() == 0) {
                                CustomUtils.toastMessage(getActivity(), "No saved address");
                                CustomUtils.hideDialog();
                            } else {
                                for (int i = 0; i < user_add.length(); i++) {
                                    jsonObject = user_add.getJSONObject(i);
                                    HashMap<String, String> map = new HashMap<String, String>();
                                    map.put("id", jsonObject.getString("id"));
                                    map.put("label", jsonObject.getString("label"));
                                    map.put("street_address", jsonObject.getString("street_address"));
                                    map.put("city", jsonObject.getString("city"));
                                    map.put("zip_code", jsonObject.getString("zip_code"));
                                    map.put("latitude", jsonObject.getString("latitude"));
                                    map.put("longtitude", jsonObject.getString("longtitude"));
                                    map.put("created_at", jsonObject.getString("created_at"));
                                    map.put("default_address", jsonObject.getString("default_address"));
                                    arraylist.add(map);
                                    if (Constants.status.toString().contentEquals("true")) {
                                        if (sharedPref.getAUTHToken() != null) {
                                            // PASS THE RESULTS INTO LISTVIEWADAPTER.JAVA
                                            AddressListAdapter adapter = new AddressListAdapter(getActivity(), arraylist);
                                            //SET THE ADAPTER TO THE LISTVIEW
                                            listview.setAdapter(adapter);
                                            // CLOSE THE PROGRESS DIALOG
                                            CustomUtils.hideDialog();
                                            UserModel.setUser_id(jsonObject.getString("user_id"));
                                            UserModel.setAddressLabel(jsonObject.getString("label"));
                                            userModel.setStreetAdd(jsonObject.getString("street_address"));
                                            userModel.setCity(jsonObject.getString("city"));
                                        }

                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (Constants.volleyTAG.equals("getuserinfo")) {
                        try {
                            jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                            userModel.setFirstname(jsonObject.getString("first_name"));
                            userModel.setLastname(jsonObject.getString("last_name"));
                            userModel.setGender(jsonObject.getString("gender"));
                            userModel.setMobile(jsonObject.getString("mobile_number"));
                            edtFirstname.setText(UserModel.getFirstname());
                            edtLastname.setText(UserModel.getLastname());
                            if (UserModel.getGender().equals("Male")) {
                                // Male
                                spnGender.setSelection(0);
                            } else {
                                // Female
                                spnGender.setSelection(1);
                            }
                            edtMobile.setText(UserModel.getMobile());

                            JSONObject userPhoto = jsonObject.getJSONObject("photo");
                            String imgURL = Constants.baseURL + userPhoto.getString("url");
                            Glide.with(getActivity())
                                    .load(imgURL)
                                    .dontAnimate()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.defaultimage)
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .fitCenter()
                                    .into(imageProfile);
                            Log.i("USERUPDATEPHOTO:", imgURL);
                            CustomUtils.hideDialog();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (Constants.volleyTAG.equals("getalcoholtypes")) {

                        ArrayList<AlcoholTypeModel> alcoholTypeList = new ArrayList<AlcoholTypeModel>();
                        try {
                            jsonObject = new JSONObject(response.toString());
                            jsonArr = jsonObject.getJSONArray("results");

                            for (int i = 0; i < jsonArr.length(); i++) {
                                jsonObj = new JSONObject();
                                jsonObj = (JSONObject) jsonArr.get(i);
                                Boolean isChecked = false;

                                if (jsonObj.getJSONArray("user_alcohol_types").length() > 0) {

                                    for (int j = 0; j < jsonObj.getJSONArray("user_alcohol_types").length(); j++) {
                                        String isUserPreferredAlcohol = jsonObj.getJSONArray("user_alcohol_types").getJSONObject(j).getString("user_id");
                                        if (isUserPreferredAlcohol.equals(UserModel.getUser_id())) {
                                            isChecked = true;
                                        }
                                    }
//                                    Log.i("AlcoholTypes-userID", userModel.getUser_id());
//                                    isChecked = true;
                                }

                                Log.i("isChecked?", isChecked + "");
                                AlcoholTypeModel alcoholTypeModel = new AlcoholTypeModel(jsonObj.getString("id"), jsonObj.getString("name"), isChecked);
                                alcoholTypeList.add(alcoholTypeModel);
                            }

                            adapter = new AlcoholTypesAdapter(getActivity(), alcoholTypeList);
                            lvAlcoholTypes.setAdapter(adapter);
                            CustomUtils.hideDialog();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (Constants.volleyTAG.equals("update")) {
                            CustomUtils.hideDialog();
                        }
                        CustomUtils.toastMessage(getActivity(), "Successfully updated");
                        CustomUtils.hideDialog();
                    }
                }
            }
        };
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("RequestCode", requestCode + "");

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();


            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setDataAndType(filePath, "image/*");
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("outputX", 150);
            intent.putExtra("outputY", 150);
            intent.putExtra("noFaceDetection", true);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CROP_ICON);


        } else if (requestCode == REQUEST_CROP_ICON) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                bitmap = extras.getParcelable("data");
                Log.i("BITMAPS", bitmap + " ");
                imageProfile.setImageBitmap(bitmap);
            }
        }
    }
}
