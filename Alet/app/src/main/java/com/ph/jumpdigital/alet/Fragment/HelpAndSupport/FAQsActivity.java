package com.ph.jumpdigital.alet.Fragment.HelpAndSupport;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.ph.jumpdigital.alet.R;

/**
 * Created by vidalbenjoe on 14/12/2015.
 */
public class FAQsActivity extends AppCompatActivity {
    public static String Content;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq_disclaimer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Report a Problem");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView content = (TextView) findViewById(R.id.contentTxtV);

        if (Content.contentEquals("faq")) {
            content.setText("FAQ!");
            getSupportActionBar().setTitle("FAQ");
        } else {
            content.setText("DISCLAIMER!");
            getSupportActionBar().setTitle("Disclaimer");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FAQsActivity.this.finish();
    }

}
