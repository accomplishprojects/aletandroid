package com.ph.jumpdigital.alet.Fragment.OrderPlacement;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Database.AletDBHelper;
import com.ph.jumpdigital.alet.Database.AletDataSource;
import com.ph.jumpdigital.alet.Fragment.ItemListing.CartViewFragment;
import com.ph.jumpdigital.alet.Fragment.LandingFragment;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.PaypalUtils;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.Utilities.UI.ExpandableHeightListView;
import com.ph.jumpdigital.alet.model.ProductModel;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by luigigo on 10/27/15.
 */
public class OrderDetailsFragment extends Fragment implements View.OnClickListener {
    public static Integer page = 0;
    public static JSONObject jsonObject, jsonObjectProducts, jsonObj, params;
    static String url;
    public static String strIndicator;
    private static JSONArray jsonArray;
    ArrayList<HashMap<String, String>> arraylist;

    ExpandableHeightListView listOfProducts;
    Button btnOrderDetails, btnCancel;
    UserModel userModel = new UserModel();
    public AletDataSource aletDataSource;
    public AletDBHelper aletDBHelper;
    OrderListAdapter orderAdapter;
    private TextView txtSubTotal, txtTax, txtShippingHandling, txtTotal, txtPromoDiscount,
            txtDeliveryAddress, deliveryInfoTxt, txtPaymentMethod, txtSpecialInstruction;
    Toolbar toolbar;
    DecimalFormat formatter;
    String TAG = "OrderDetails", strPromotionTypeID;
    Double dblPromoDiscount;
    Double dblTax;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.order_details, container, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Review Order Details");

        aletDBHelper = new AletDBHelper(getActivity());
        aletDataSource = new AletDataSource(getActivity());
        aletDataSource.open();
        formatter = new DecimalFormat("#,###.00");
        listOfProducts = (ExpandableHeightListView) mainView.findViewById(R.id.listOfProd);
        listOfProducts.setExpanded(true);
        txtDeliveryAddress = (TextView) mainView.findViewById(R.id.txtDeliveryAddress);
        deliveryInfoTxt = (TextView) mainView.findViewById(R.id.deliveryInfoTxt);
        txtPaymentMethod = (TextView) mainView.findViewById(R.id.txtPaymentMethod);
        txtSpecialInstruction = (TextView) mainView.findViewById(R.id.txtSpecialInstruction);
        txtSubTotal = (TextView) mainView.findViewById(R.id.txtSubTotal);
        txtTax = (TextView) mainView.findViewById(R.id.txtTax);
        txtShippingHandling = (TextView) mainView.findViewById(R.id.txtShippingHandling);
        txtPromoDiscount = (TextView) mainView.findViewById(R.id.txtPromoDiscount);
        txtTotal = (TextView) mainView.findViewById(R.id.txtTotal);

        btnOrderDetails = (Button) mainView.findViewById(R.id.btnOrderDetails);
        btnOrderDetails.setOnClickListener(this);
        btnCancel = (Button) mainView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
        Log.i(TAG, "Promocode is: " + ProductModel.getOrderPromoCode());

        if (ProductModel.getOrderDetails() != null) {
            if (strIndicator.equals("orderHistory")) {
                btnOrderDetails.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
                getOrderDetailsOrderHistory();
            } else {
                Log.i(TAG + " LOG", ProductModel.getOrderList());
                getOrderDetailsOrderPlacement();
            }
            getUserDetails();
        } else
            Log.e("Warning", "orderDetails is null");
        return mainView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOrderDetails:
                try {
                    jsonObj = new JSONObject();
                    params = new JSONObject();
                    jsonArray = new JSONArray(ProductModel.getOrderList());

                    Log.i("CheckoutType", Constants.checkoutType);

                    if (Constants.checkoutType.equals("with_new_address")) {
                        url = Constants.apiURL + Constants.checkoutNewAddress;
                        jsonObj.put("label", ProductModel.getAddressLabel());
                        jsonObj.put("street_address", ProductModel.getAddressStreet());
                        jsonObj.put("city", ProductModel.getAddressCity());
                        jsonObj.put("zip_code", "");
                        jsonObj.put("latitude", UserModel.getLatitude());
                        jsonObj.put("longtitude", UserModel.getLongitude());

                    } else {
                        url = Constants.apiURL + Constants.checkoutWithAddress;
                        jsonObj.put("address_id", ProductModel.getAddressId());
                        jsonObj.put("correlation_id", PaypalUtils.getPaypalMetaID());
                    }

                    if (ProductModel.getOrderPromoCode() != null) {
                        jsonObj.put("promo_code", ProductModel.getOrderPromoCode());
                    } else {
                        jsonObj.put("promo_code", "");
                    }

                    jsonObj.put("correlation_id", PaypalUtils.getPaypalMetaID());
                    jsonObj.put("payment_firstname", UserModel.getFirstname());
                    jsonObj.put("payment_lastname", UserModel.getLastname());
                    jsonObj.put("payment_company", "");
                    jsonObj.put("payment_address1", UserModel.getStreetAdd());
                    jsonObj.put("payment_address2", "");
                    jsonObj.put("payment_city", UserModel.getCity());
                    jsonObj.put("payment_postcode", UserModel.getZipCode());
                    jsonObj.put("payment_country", "");
                    jsonObj.put("payment_zone", "");
                    jsonObj.put("payment_menthod", "");
                    jsonObj.put("payment_code", "");
                    jsonObj.put("payment_method_id", "");

                    jsonObj.put("order_product", jsonArray);
                    Log.i(TAG + "ArrayOrderList", jsonArray.toString());
                    Log.i("CheckOut-JSONObject", jsonObj.toString());

                    checkout();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.btnCancel:
                CustomUtils.loadFragment(new LandingFragment(), true, getActivity());
                break;
        }

    }

    private void checkout() {
        Constants.volleyTAG = "checkout";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.putMethod, url,
                null, jsonObj,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        Log.i("Checkout", jsonObj.toString());
        CustomUtils.showDialog(getActivity());

    }

    private void getOrderDetailsOrderHistory() {
        double dblDiscountedAmount = 0;

        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            Log.d("OrderDetailsHistory", ProductModel.getOrderDetails().toString());
            jsonObject = new JSONObject(ProductModel.getOrderDetails());
            Log.d(TAG + "History", jsonObject.toString());
            jsonArray = new JSONArray();
            jsonArray = jsonObject.getJSONArray("order_product");
            Log.d("OrderProductList", jsonArray.toString());

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObjectProducts = jsonArray.getJSONObject(i);
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("quantity", jsonObjectProducts.getString("quantity"));
                map.put("product_total", jsonObjectProducts.getString("total"));
                map.put("title", jsonObjectProducts.getString("name"));
                map.put("price", jsonObjectProducts.getString("price"));

                dblDiscountedAmount += Double.parseDouble(jsonObjectProducts.getString("discounted_amount"));
                // Toast.makeText(getActivity(), "Discount: " + dblDiscountedAmount, Toast.LENGTH_SHORT).show();

                if (jsonObjectProducts.has("product")) {
                    map.put("store_name", jsonObjectProducts.getJSONObject("product").getJSONObject("store").getString("name"));
                    Log.i("ProdImage", jsonObjectProducts.getJSONObject("product").getJSONObject("image").getString("url"));
                    map.put("prod_imageurl", jsonObjectProducts.getJSONObject("product").getJSONObject("image").getString("url"));
                }

                arraylist.add(map);
            }
            txtDeliveryAddress.setText(jsonObject.getString("shipping_address1"));
            txtPaymentMethod.setText("PayPal");
            txtSubTotal.setText("₱ " + CustomUtils.decimalFormat(getActivity(), Double.parseDouble(jsonObject.getString("total"))));

            // Display Promo Discount
            if (jsonObject.has("promo_order")) {
                JSONArray jArrPromoOrder = new JSONArray();
                jArrPromoOrder = jsonObject.getJSONArray("promo_order");

                for (int i = 0; i < jArrPromoOrder.length(); i++) {
                    JSONObject jObj = new JSONObject();
                    jObj = jArrPromoOrder.getJSONObject(i);
                    String strPromotionTypeID = jObj.getJSONObject("promotion").getString("promotion_type_id");
                    dblPromoDiscount = Double.parseDouble(jObj.getJSONObject("promotion").getString("discount"));
                    // txtPromoDiscount.setText("₱ " + CustomUtils.decimalFormat(getActivity(), dblPromoDiscount));
                    Log.i(TAG, strPromotionTypeID + "PromotionTypeID");
                }
            }

            Double dblTax = Double.parseDouble(jsonObject.getString("tax"));
            Double dblGrandTotal = (Double.parseDouble(jsonObject.getString("total")) + dblTax);

            txtPromoDiscount.setText("₱ " + CustomUtils.decimalFormat(getActivity(), dblDiscountedAmount));
            txtTax.setText("₱ " + CustomUtils.decimalFormat(getActivity(), dblTax));
            txtTotal.setText("₱ " + CustomUtils.decimalFormat(getActivity(), dblGrandTotal - dblDiscountedAmount));

            orderAdapter = new OrderListAdapter(getActivity(), arraylist);
            listOfProducts.setAdapter(orderAdapter);
            CustomUtils.hideDialog();

        } catch (JSONException e) {
            e.printStackTrace();
            CustomUtils.toastMessage(getActivity(), e.getMessage());
            CustomUtils.hideDialog();
        }

    }

    private void getOrderDetailsOrderPlacement() {
        double dblDiscountedAmount = 0;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            Log.d("OrderDetailsPlacement", ProductModel.getOrderDetails());
            jsonObject = new JSONObject(ProductModel.getOrderDetails()).getJSONObject("results");
            Log.d(TAG, jsonObject.toString());
            jsonArray = new JSONArray();
            jsonArray = jsonObject.getJSONArray("order_product");
            Log.d("OrderProductList", jsonArray.toString());

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObjectProducts = jsonArray.getJSONObject(i);
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("quantity", jsonObjectProducts.getString("quantity"));
                map.put("product_total", jsonObjectProducts.getString("total"));
                map.put("title", jsonObjectProducts.getString("name"));
                map.put("price", jsonObjectProducts.getString("price"));
                map.put("store_name", jsonObjectProducts.getJSONObject("product").getJSONObject("store").getString("name"));
                // Log.i("ProdImage", jsonObjectProducts.getJSONObject("product").getJSONObject("image").getString("url"));
                map.put("prod_imageurl", jsonObjectProducts.getJSONObject("product").getJSONObject("image").getString("url"));

                dblDiscountedAmount += Double.parseDouble(jsonObjectProducts.getString("discounted_amount"));
                arraylist.add(map);
            }

            // Log.i("Address", ProductModel.getAddressId());
            if (ProductModel.getSpecialInstruction() == null) {
            } else
                Log.i("SpecialInstruction", ProductModel.getSpecialInstruction());

            txtDeliveryAddress.setText(ProductModel.getDeliveryAddress());
            txtSpecialInstruction.setText(ProductModel.getSpecialInstruction());
            txtPaymentMethod.setText("PayPal");
            txtSubTotal.setText("₱ " + CustomUtils.decimalFormat(getActivity(), Double.parseDouble(jsonObject.getString("total"))));

            if (jsonObject.has("promo_order")) {
                // Display Promo Discount
                if (ProductModel.getOrderPromoCode() != null) {
                    JSONArray jArrPromoOrder = new JSONArray();
                    jArrPromoOrder = jsonObject.getJSONArray("promo_order");

                    for (int i = 0; i < jArrPromoOrder.length(); i++) {
                        JSONObject jObj = new JSONObject();
                        jObj = jArrPromoOrder.getJSONObject(i);
                        strPromotionTypeID = jObj.getJSONObject("promotion").getString("promotion_type_id");
                        dblPromoDiscount = Double.parseDouble(jObj.getJSONObject("promotion").getString("discount"));
                        // txtPromoDiscount.setText("₱ " + CustomUtils.decimalFormat(getActivity(), dblPromoDiscount));
                        Log.i(TAG, strPromotionTypeID + "PromotionTypeID");
                    }

                }
            }

            Double dblTax = Double.parseDouble(jsonObject.getString("tax"));
            Double dblGrandTotal = (Double.parseDouble(jsonObject.getString("total")) + dblTax);

            txtPromoDiscount.setText("₱ " + CustomUtils.decimalFormat(getActivity(), dblDiscountedAmount));
            txtTax.setText("₱ " + CustomUtils.decimalFormat(getActivity(), dblTax));
            txtTotal.setText("₱ " + CustomUtils.decimalFormat(getActivity(), dblGrandTotal - dblDiscountedAmount));

            orderAdapter = new OrderListAdapter(getActivity(), arraylist);
            listOfProducts.setAdapter(orderAdapter);
            CustomUtils.hideDialog();
            // getUserDetails();

        } catch (JSONException e) {
            e.printStackTrace();
            CustomUtils.toastMessage(getActivity(), e.getMessage());
            CustomUtils.hideDialog();
        }
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            Log.i("Response", responseBody);

                            jsonObject = new JSONObject(responseBody);
                            JSONArray errorResult = jsonObject.getJSONArray("error");
                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");

                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);
                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                    if (currentDynamicValue.equals("Promo code not valid")) {
                                        Log.i(TAG, "Promo code is invalid in add to cart");
                                    } else {
                                        Toast.makeText(getActivity(), currentDynamicValue, Toast.LENGTH_SHORT).show();
                                    }

                                    Log.i(TAG + "CheckoutErr", currentDynamicValue);
                                    if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                        CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                        gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ActivityCompat.finishAffinity(getActivity());
                                        startActivity(gotoLogin);
                                        getActivity().finish();

                                    } else {
                                        CustomUtils.hideDialog();
                                        CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                                    }
                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }
                        } else {
                            CustomUtils.toastMessage(getActivity(), "Please check your internet connection");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJson", "" + e);
                        CustomUtils.toastMessage(getActivity(), "Invalid response from server");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }


    void getUserDetails() {

        url = Constants.apiURL + Constants.showSpecificUser;
        Constants.volleyTAG = "getuserinfo";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());

    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                CustomUtils.hideDialog();
                if (Constants.volleyTAG.equals("getuserinfo")) {
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                        userModel.setFirstname(jsonObject.getString("first_name"));
                        userModel.setLastname(jsonObject.getString("last_name"));
                        userModel.setGender(jsonObject.getString("gender"));
                        userModel.setMobile(jsonObject.getString("mobile_number"));
                        CustomUtils.hideDialog();
                        deliveryInfoTxt.setText(UserModel.getFirstname() + " " + UserModel.getLastname() + ", " + UserModel.getMobile());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.i("Success Checkout", response.toString());
                    Toast.makeText(getActivity(), "Successful Transaction", Toast.LENGTH_SHORT).show();
                    CustomUtils.hideDialog();
                    try {
                        OrderConfirmed.strOrderID = response.getJSONObject("results").getString("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    aletDataSource.deleteAllCartByEmail(UserModel.getEmail());
                    CustomUtils.loadFragment(new OrderConfirmed(), true, getActivity());
                }

            }
        };
    }

}
