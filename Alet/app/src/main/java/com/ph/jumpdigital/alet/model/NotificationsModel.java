package com.ph.jumpdigital.alet.model;

import java.io.Serializable;

/**
 * Created by luigigo on 12/11/15.
 */
public class NotificationsModel implements Serializable {

    public static String ratingsRate, ratingsMessage, ratingsOrderId;

    public static String getRatingsRate() {
        return ratingsRate;
    }

    public static void setRatingsRate(String ratingsRate) {
        NotificationsModel.ratingsRate = ratingsRate;
    }

    public static String getRatingsMessage() {
        return ratingsMessage;
    }

    public static void setRatingsMessage(String ratingsMessage) {
        NotificationsModel.ratingsMessage = ratingsMessage;
    }

    public static String getRatingsOrderId() {
        return ratingsOrderId;
    }

    public static void setRatingsOrderId(String ratingsOrderId) {
        NotificationsModel.ratingsOrderId = ratingsOrderId;
    }
}
