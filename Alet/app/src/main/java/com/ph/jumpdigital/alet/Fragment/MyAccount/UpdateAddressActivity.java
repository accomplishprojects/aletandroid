package com.ph.jumpdigital.alet.Fragment.MyAccount;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.UserModel;
import com.ph.jumpdigital.alet.registration.GooglePlacesAutocompleteAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils.checkPlayServices;

/**
 * Created by vidalbenjoe on 28/09/2015.
 */
public class UpdateAddressActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {
    EditText homeEditText/*, zipCodeEditTxt*/;
    AutoCompleteTextView actStreetAdd;
    Spinner citySpinner;
    TextView cantfindCityTxt;
    Button updateAddressBt, cancelSubscribe, subscribeButton;
    ImageView imgShowMap;
    String url;
    UserModel userModel = new UserModel();
    String TAG = "UPDATE ADDRESS";
    String id, label, streetAdd, cityAdd, zipCode, longtitude, latitude;
    CheckBox chkSetDefaultAddress;
    Map<String, String> params;
    Dialog dialog;
    Toolbar toolbar;
    boolean canUpdate = true;

    MapFragment supportmapfragment;
    GoogleMap supportMap;
    Marker marker;
    LatLng _point;
    CameraPosition cameraPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_update_address);
        // enabling action bar app icon and behaving it as toggle button
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        citySpinner = (Spinner) findViewById(R.id.citySpinner);
        homeEditText = (EditText) findViewById(R.id.homeEditText);
        actStreetAdd = (AutoCompleteTextView) findViewById(R.id.actStreetAdd);
        actStreetAdd.setAdapter(new GooglePlacesAutocompleteAdapter(UpdateAddressActivity.this, R.layout.list_item));
        //zipCodeEditTxt = (EditText) findViewById(R.id.zipCodeEditTxt);
        cantfindCityTxt = (TextView) findViewById(R.id.cantfindCityTxt);
        cantfindCityTxt.setOnClickListener(this);
        updateAddressBt = (Button) findViewById(R.id.updateAddressBt);
        updateAddressBt.setOnClickListener(this);
        chkSetDefaultAddress = (CheckBox) findViewById(R.id.chkSetDefaultAddress);
        imgShowMap = (ImageView) findViewById(R.id.imgShowMap);
        imgShowMap.setOnClickListener(this);

        Intent i = getIntent();
        id = i.getStringExtra("id");
        label = i.getStringExtra("label");
        streetAdd = i.getStringExtra("street_address");
        cityAdd = i.getStringExtra("city");
        zipCode = i.getStringExtra("zip_code");
        longtitude = i.getStringExtra("longtitude");
        latitude = i.getStringExtra("latitude");

        UserModel.setLatitude(latitude);
        UserModel.setLongitude(longtitude);

        homeEditText.setText(label);
        actStreetAdd.setText(streetAdd);
        //citySpinner.setPrompt("Luigi");
        //zipCodeEditTxt.setText(zipCode);
        // cityAdd = citySpinner.getSelectedItem().toString();
        UserModel.setLatitude(latitude);
        UserModel.setLongitude(longtitude);

        Log.i(TAG, " Address ID: " + id + " City Address: " + cityAdd);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cantfindCityTxt:
                if (homeEditText.getText().toString().isEmpty() && actStreetAdd.getText().toString().isEmpty()) {
                    homeEditText.setError("Field can't be blank");
                    actStreetAdd.setError("Field can't be blank");
                } else if (homeEditText.getText().toString().isEmpty()) {
                    homeEditText.setError("Field can't be blank");
                } else if (actStreetAdd.getText().toString().isEmpty()) {
                    actStreetAdd.setError("Field can't be blank");
                } else {
                    showSubscribeDialog();
                }
                break;

            case R.id.updateAddressBt:
                if (canUpdate) {
                    if (chkSetDefaultAddress.isChecked()) {
                        setDefaultAddress();
                        CustomUtils.showDialog(UpdateAddressActivity.this);
                    } else {
                        //getLatLong();
                        UpdateUserAddress(UserModel.getLatitude(), UserModel.getLongitude());
                        CustomUtils.showDialog(UpdateAddressActivity.this);
                    }
                } else {
                    Toast.makeText(UpdateAddressActivity.this, "Sorry selected address is not applicable", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imgShowMap:
                imgShowMap.setEnabled(false);
                dialog = new Dialog(UpdateAddressActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_map);
                dialog.setCancelable(false);

                Button btnDismissMap = (Button) dialog.findViewById(R.id.btnDismissMap);
                btnDismissMap.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(UpdateAddressActivity.this, Locale.getDefault());
                        try {
                            if (_point != null) {
                                addresses = geocoder.getFromLocation(_point.latitude, _point.longitude, 1);

                                if (addresses.size() > 0) {
                                    String city = addresses.get(0).getAddressLine(1);
                                    //CustomUtils.toastMessage(UpdateAddressActivity.this, "LongLat:" + city + ": " + _point.latitude + ", " + _point.longitude);
                                    Log.i("MarkerPos: ", "LAT: " + marker.getPosition().latitude + " LNG: " + marker.getPosition().longitude);

                                    if (city.contains("Quezon City")) {
                                        canUpdate = true;
                                        citySpinner.setSelection(0);
                                        Toast.makeText(UpdateAddressActivity.this, "Address located", Toast.LENGTH_SHORT).show();

                                    } else if (city.contains("Makati")) {
                                        canUpdate = true;
                                        citySpinner.setSelection(1);
                                        Toast.makeText(UpdateAddressActivity.this, "Address located", Toast.LENGTH_SHORT).show();

                                    } else if (city.contains("Pasig")) {
                                        canUpdate = true;
                                        citySpinner.setSelection(2);
                                        Toast.makeText(UpdateAddressActivity.this, "Address located", Toast.LENGTH_SHORT).show();

                                    } else {
                                        canUpdate = false;
                                        Toast.makeText(UpdateAddressActivity.this, "Sorry selected address is not applicable", Toast.LENGTH_SHORT).show();
                                    }

                                    UserModel.setLatitude(String.valueOf(_point.latitude));
                                    UserModel.setLongitude(String.valueOf(_point.longitude));
                                }
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                            Toast.makeText(UpdateAddressActivity.this, "Can't get location. Please check your internet connection", Toast.LENGTH_SHORT).show();
                            CustomUtils.hideDialog();
                        }

                        dialog.dismiss();
                        imgShowMap.setEnabled(true);
                        android.app.Fragment fragment = (getFragmentManager().findFragmentById(R.id.map));
                        android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.remove(fragment);
                        ft.commit();
                    }
                });

                if (checkPlayServices(UpdateAddressActivity.this)) {
                    setUpMap();
                } else {
                    CustomUtils.toastMessage(UpdateAddressActivity.this, "Google Services not Installed");
                }
                dialog.show();

                //    new LoadMoreDataTask().execute();
                break;

        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void setUpMap() {
        supportmapfragment = (MapFragment) this.getFragmentManager().findFragmentById(R.id.map);

        supportMap = supportmapfragment.getMap();
        supportmapfragment.getMapAsync(this);
        supportMap.getUiSettings().setMapToolbarEnabled(false);
        supportMap.getUiSettings().setZoomControlsEnabled(false);

        GoogleMapOptions options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_SATELLITE)
                .compassEnabled(false)
                .rotateGesturesEnabled(false)
                .tiltGesturesEnabled(true);

        if (UserModel.getLatitude() != null && UserModel.getLongitude() != null) {
            marker = supportMap.addMarker(new MarkerOptions().position(
                    new LatLng(Double.parseDouble(UserModel.getLatitude()), Double.parseDouble(UserModel.getLongitude()))));

            // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(UserModel.getLatitude()), Double.parseDouble(UserModel.getLongitude())))      // Sets the center of the map to Mountain View
                    .zoom(11)                   // Sets the zoom
                    .bearing(360)                // Sets the orientation of the camera to east
//                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
        } else {
            // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
            cameraPosition = new CameraPosition.Builder()
                    .target(Constants.METRO_MANILA_LOC)      // Sets the center of the map to Mountain View
                    .zoom(11)                   // Sets the zoom
                    .bearing(360)                // Sets the orientation of the camera to east
//                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
        }

        supportMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        supportMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                _point = point;
                supportMap.clear();
                marker = supportMap.addMarker(new MarkerOptions().position(
                        new LatLng(point.latitude, point.longitude)));
            }
        });

    }

    private void setDefaultAddress() {
        params = new HashMap<String, String>();
        params.put("address_id", id);

        url = Constants.apiURL + Constants.setDefaultAddress;
        Constants.volleyTAG = "setdefaultaddress";

        VolleyRequest request = new VolleyRequest(UpdateAddressActivity.this,
                Constants.postMethod, url,
                (HashMap<String, String>) params, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                UpdateAddressActivity.this, true, Constants.volleyTAG);

        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);

    }


    public void showSubscribeDialog() {

        dialog = new Dialog(UpdateAddressActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_subscribe);
        dialog.setCancelable(true);
//        dialog.getWindow().setBackgroundDrawable(
//                new ColorDrawable(
//                        android.graphics.Color.TRANSPARENT));

        cancelSubscribe = (Button) dialog.findViewById(R.id.cancelSubscribe);
        cancelSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        subscribeButton = (Button) dialog.findViewById(R.id.subscribeButton);
        subscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubscribeToNewsLetter();
            }
        });

        dialog.show();
    }

    private void getLatLong() {
        String strDesc = null;
        streetAdd = actStreetAdd.getText().toString();

        try {
            strDesc = URLEncoder.encode(streetAdd, "utf8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = Constants.GOOGLE_PLACES_BASE_URL + Constants.GOOGLE_GEOCODING + strDesc + Constants.GOOGLE_GEOCODING_API_KEY;
        Constants.volleyTAG = "getLatLng";
        VolleyRequest request = new VolleyRequest(UpdateAddressActivity.this,
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                UpdateAddressActivity.this, false, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        //CustomUtils.showDialog(UpdateAddressActivity.this);

    }

    public void UpdateUserAddress(String lat, String lng) {
        params = new HashMap<String, String>();
        params.put("label", homeEditText.getText().toString());
        params.put("street_address", actStreetAdd.getText().toString());
        params.put("city", citySpinner.getSelectedItem().toString());
        //params.put("zip_code", zipCodeEditTxt.getText().toString());

        params.put("latitude", lat);
        params.put("longtitude", lng);


        url = Constants.apiURL + Constants.updateAddress + id;
        Constants.volleyTAG = "updatedaddress";

        VolleyRequest request = new VolleyRequest(UpdateAddressActivity.this,
                Constants.putMethod, url,
                (HashMap<String, String>) params, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                UpdateAddressActivity.this, true, Constants.volleyTAG);

        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
//        Toast.makeText(UpdateAddressActivity.this, "LABEL:" + homeEditText.getText().toString() + "\n" +
//                "STREETADD:" + actStreetAdd.getText().toString() + "\n" +
//                "CITY:" + citySpinner.getSelectedItem().toString() + "\n" +
//                //"zip_code" + zipCodeEditTxt.getText().toString() + "\n" +
//                "TOKEN:" + UserModel.getAuthToken(), Toast.LENGTH_SHORT).show();

    }

    public void SubscribeToNewsLetter() {

        url = Constants.apiURL + Constants.subscribeNewsLetter;
        Log.i("URL", url);
        Constants.volleyTAG = "subscribenewsletter";
        VolleyRequest request = new VolleyRequest(UpdateAddressActivity.this,
                Constants.postMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                UpdateAddressActivity.this, true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(this);
        dialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        UpdateAddressActivity.this.finish();
        super.onBackPressed();

    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {

                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");
                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);

                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // loop to get the dynamic key
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                    // get the value of the dynamic key
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    // do something here with the value...
                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);

                                    if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                        CustomUtils.toastMessage(UpdateAddressActivity.this, currentDynamicValue);
                                        Intent gotoLogin = new Intent(UpdateAddressActivity.this, LoginActivity.class);
                                        gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ActivityCompat.finishAffinity(UpdateAddressActivity.this);
                                        startActivity(gotoLogin);
                                        UpdateAddressActivity.this.finish();

                                    } else {
                                        // if (rowCount > 0) {
                                        CustomUtils.hideDialog();
                                        // CustomUtils.loadFragment(new CartViewFragment(), true, UpdateAddressActivity.this);
                                        //} else {
                                        CustomUtils.toastMessage(UpdateAddressActivity.this, currentDynamicValue);
                                        //}
                                    }
                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

                            Log.e("jsonObject", errorResult + "");
                        } else {
                            Toast.makeText(UpdateAddressActivity.this, "Please check your internet connection",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

//               Log.i("UpdateAddressActivity", response.toString());
//               Toast.makeText(UpdateAddressActivity.this, "Updated Address Successfully",
//                        Toast.LENGTH_LONG).show();

                if (Constants.volleyTAG.equals("getLatLng")) {
                    //Parsing JSONResponse
                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        JSONArray jsonArray = jsonObject.getJSONArray("results");

                        Log.i("GetLatLngResponse", jsonArray.toString());
                        if (jsonObject.getString("status").equals("OK")) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonLocation = jsonArray.getJSONObject(i).getJSONObject("geometry").getJSONObject("location");
                                latitude = jsonLocation.getString("lat");
                                longtitude = jsonLocation.getString("lng");
                                Log.i("LAT", latitude + " index" + i + " LNG " + longtitude);
                                //CustomUtils.hideDialog();
                            }
                            //Log.i("getLatLng", id);
                            UpdateUserAddress(latitude, longtitude);
                        } else {
                            Toast.makeText(UpdateAddressActivity.this, "Invalid Address", Toast.LENGTH_SHORT).show();
                            CustomUtils.hideDialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (Constants.volleyTAG.equals("setdefaultaddress")) {
                    //getLatLong();
                    UpdateUserAddress(UserModel.getLatitude(), UserModel.getLongitude());
                    Log.i("SetDefaultAddress", response.toString());

                } else {
                    if (response != null) {
                        if (Constants.volleyTAG.equals("subscribenewsletter")) {
                            Log.i(TAG + "SubscribedNewsLetter", response.toString());
                            Toast.makeText(UpdateAddressActivity.this, "You've successfully subcribed", Toast.LENGTH_SHORT).show();
                            CustomUtils.hideDialog();
                            UpdateAddressActivity.this.finish();
                        } else {
                            Log.i("UpdateAddress", response.toString());
                            Toast.makeText(UpdateAddressActivity.this, "Address successfully updated", Toast.LENGTH_SHORT).show();
                            CustomUtils.hideDialog();
                            UpdateAddressActivity.this.finish();
                        }
                    }
                }
            }
        };
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }


}
