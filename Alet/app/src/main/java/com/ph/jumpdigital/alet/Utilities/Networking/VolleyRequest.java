package com.ph.jumpdigital.alet.Utilities.Networking;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ph.jumpdigital.alet.Utilities.SharedPref;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by luigigo on 10/18/15.
 */
public class VolleyRequest {
    Context context;
    SharedPref sharedPref;

    public JsonObjectRequest jsonObjectRequest;
    UserModel userModel = new UserModel();

    //    SharedPref sharedPref = new SharedPref();
    public VolleyRequest(Context context, int method, String url, HashMap<String, String> params, JSONObject jsonParams, Response.Listener<JSONObject> reponseListener,
                         Response.ErrorListener errorListener, Activity activity, final Boolean requiredToken, final String volleyTAG) {
        this.context = context;
        sharedPref = new SharedPref(context);
        if (params != null) {
            Log.i("VolleyHashMap", params.toString());
        } else {
            if (params != null)
                Log.i("VolleyJSON", jsonParams.toString());
        }

//        HttpsTrustManager.allowAllSSL();

        if (volleyTAG.equals("getaddress")
                || volleyTAG.equals("getuserinfo")
                || volleyTAG.equals("getviewcart")
                || volleyTAG.equals("getorderhistory")
//                || volleyTAG.equals("subscribenewsletter")
                || volleyTAG.equals("getLatLng")
                || volleyTAG.equals("getalcoholtypes")
                || volleyTAG.equals("getcartitems")
                || volleyTAG.equals("checkpaypallogin")
                || volleyTAG.equals("viewtypes")
                || volleyTAG.equals("issuetype")) {
            //THIS IS FOR METHOD GET
            jsonObjectRequest = new JsonObjectRequest(method,
                    url, reponseListener
                    , errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    headers.put("Accept", "application/json");
                    if (requiredToken.equals(true)) {
                        headers.put("x-auth-token", sharedPref.getAUTHToken());
                        Log.i("requiresToken ", "This request uses authentication token");
                    }
                    return headers;
                }
            };
        } else if (volleyTAG.equals("subscribenewsletter")) {
            //THIS IS FOR METHOD PUT AND POST WITHOUT ANY PARAMS
            jsonObjectRequest = new JsonObjectRequest(method,
                    url, (String) null, reponseListener
                    , errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    //      headers.put("Accept", "application/json");
                    if (requiredToken.equals(true)) {
                        headers.put("x-auth-token", sharedPref.getAUTHToken());
                        Log.i("requiresToken ", "This request uses authentication token");
                    }
                    return headers;
                }
            };
        } else if (volleyTAG.equals("addnewaddress")
                || (volleyTAG.equals("usersignout"))
                || volleyTAG.equals("checkout")
                || volleyTAG.equals("preferred_alcohol")
                || volleyTAG.equals("jsonformat")
                || volleyTAG.equals("deleteCartItems")
                || volleyTAG.equals("validatepromocode")
                ) {

            //THIS IS FOR METHOD PUT AND POST USING JSONOBJECT FORMAT
            jsonObjectRequest = new JsonObjectRequest(method,
                    url, jsonParams, reponseListener
                    , errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    if (!volleyTAG.equals("deleteCartItems"))
                        headers.put("Accept", "application/json");
                    if (requiredToken.equals(true)) {
                        headers.put("x-auth-token", sharedPref.getAUTHToken());
                        Log.i("requiresToken ", "This request uses authentication token");
                    }
                    return headers;
                }
            };
        } else {
            //THIS IS FOR METHOD PUT AND POST USING HASHMAP FORMAT
            jsonObjectRequest = new JsonObjectRequest(method,
                    url, new JSONObject(params), reponseListener
                    , errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Accept", "application/json");

                    if (requiredToken.equals(true)) {
                        headers.put("x-auth-token", sharedPref.getAUTHToken());
                        Log.i("requiresToken ", "This request uses authentication token");
                    }
                    return headers;
                }
            };
        }

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // ADDING REQUEST TO REQUEST QUEUE
    }
}