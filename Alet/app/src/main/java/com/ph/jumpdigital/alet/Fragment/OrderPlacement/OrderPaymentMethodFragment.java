package com.ph.jumpdigital.alet.Fragment.OrderPlacement;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.ProductModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by luigigo on 10/27/15.
 */
public class OrderPaymentMethodFragment extends Fragment implements View.OnClickListener {

    static String strPaymentMethod = null;
    Button btnPaymentMethod;
    ProductModel productModel = new ProductModel();
    ListView ls;
    JSONObject jsonObject;
    ArrayList<HashMap<String, String>> arraylist;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.order_payment_method, container, false);

        btnPaymentMethod = (Button) mainView.findViewById(R.id.btnPaymentMethod);
        btnPaymentMethod.setOnClickListener(this);
//        rbPaymentPaypal = (RadioButton) mainView.findViewById(R.id.rbPaymentPaypal);
//        rbPaymentDelivery = (RadioButton) mainView.findViewById(R.id.rbPaymentDelivery);

        ls = (ListView) mainView.findViewById(R.id.listView);
        ls.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                for (int i = 0; i < parent.getCount(); i++) {

                    View v = parent.getChildAt(i);
                    RadioButton radio = (RadioButton) v.findViewById(R.id.rbPayMethod);
                    radio.setChecked(false);

                }

                RadioButton radio = (RadioButton) view.findViewById(R.id.rbPayMethod);
                radio.setChecked(true);

                CustomUtils.toastMessage(getActivity(), radio.getText().toString());
                strPaymentMethod = radio.getText().toString();

            }
        });

        getPaymentMethod();

        return mainView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPaymentMethod:
//                if (rbPaymentPaypal.isChecked()) {
//                    productModel.setPaymentMethod("Paypal Account");
//                    CustomUtils.loadFragment(new OrderSpecialInstructFragment(), true, getActivity());
//
//                } else if (rbPaymentDelivery.isChecked()) {
//                    productModel.setPaymentMethod("Cash on Delivery");
//                    CustomUtils.loadFragment(new OrderSpecialInstructFragment(), true, getActivity());
//
//                } else {
//                    CustomUtils.toastMessage(getActivity(), "Please select mode of payment");
//                }
                if (strPaymentMethod == null) {
                    CustomUtils.toastMessage(getActivity(), "Select above");
                } else {
                    ProductModel.setPaymentMethod(strPaymentMethod);
                    strPaymentMethod = null;
                    CustomUtils.loadFragment(new OrderSpecialInstructFragment(), true, getActivity());

                }

                break;
        }

    }

    private void getPaymentMethod() {
        String url = Constants.apiURL + Constants.payment_method;
        Constants.volleyTAG = "getaddress";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");

                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);
                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                    Toast.makeText(getActivity(), currentDynamicValue,
                                            Toast.LENGTH_LONG).show();

                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

                            Log.e("jsonObject", errorResult + "");
                        } else {
                            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.i("Success", response.toString());

                arraylist = new ArrayList<HashMap<String, String>>();
                try {
                    jsonObject = new JSONObject(response.toString());
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObj = new JSONObject();
                        jsonObj = jsonArray.getJSONObject(i);

                        String name = jsonObj.getJSONObject("payment_list").getString("list");

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("name", name);
                        arraylist.add(map);
                    }

                    PaymentMethodAdapter adapter = new PaymentMethodAdapter(getActivity(), arraylist);
                    ls.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
    }

}
