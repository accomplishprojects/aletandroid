package com.ph.jumpdigital.alet.Fragment.OrderPlacement;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Fragment.ItemListing.CartViewFragment;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.PaypalUtils;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.ProductModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;

/**
 * Created by luigigo on 9/29/15.
 */
public class OrderType extends Fragment implements View.OnClickListener {



    public static Integer page = 0;
    public static JSONObject jsonObject;
    static String url;
    Button btnOrderType;
    Toolbar toolbar;
    ProductModel productModel = new ProductModel();
    RadioButton rbDefaultAddress, rbSelectSavedAddress, rbAddNewAddress;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.order_type, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Select Order Type");

        btnOrderType = (Button) mainView.findViewById(R.id.btnOrderType);
        btnOrderType.setOnClickListener(this);
        rbDefaultAddress = (RadioButton) mainView.findViewById(R.id.rbDeliverDefaultAddress);
        rbSelectSavedAddress = (RadioButton) mainView.findViewById(R.id.rbSelectSavedAddress);
        rbAddNewAddress = (RadioButton) mainView.findViewById(R.id.rbAddNewAddress);

//        orderList = this.getArguments().getString("order_results");
//        productModel.setOrderList(orderList);
//        Log.i("orderList", productModel.getOrderList());

//        Log.i("ORDERLIST", productModel.getOrderList());

        return mainView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOrderType:
                if (rbDefaultAddress.isChecked()) {
                    Constants.checkoutType ="with_address";
                    url = Constants.apiURL + Constants.userDefaultAddress;
                    Constants.volleyTAG = "getaddress";
                    VolleyRequest request = new VolleyRequest(getActivity(),
                            Constants.getMethod, url,
                            null, null,
                            createRequestSuccessListener(),
                            createRequestErrorListener(),
                            getActivity(), true, Constants.volleyTAG);
                    AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
                    CustomUtils.showDialog(getActivity());

                } else if (rbSelectSavedAddress.isChecked()) {
//                    ProfileInfo pf = new ProfileInfo();
//                    ProfileInfo.page = 4;
//                    AddressListAdapter.indicator = "paymentMethod";
                    Constants.checkoutType ="with_address";
                    CustomUtils.loadFragment(new OrderSavedAddress(), true, getActivity());

                } else if (rbAddNewAddress.isChecked()) {
                    Constants.checkoutType ="with_new_address";
                    OrderNewAddressFragment.classIndicator = "fromOrderTypeFragment";
                    CustomUtils.loadFragment(new OrderNewAddressFragment(), true, getActivity());
                }
                break;
        }
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");

                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);
                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);

                                    if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                        CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                        gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ActivityCompat.finishAffinity(getActivity());
                                        startActivity(gotoLogin);
                                        getActivity().finish();

                                    } else {
                                        // if (rowCount > 0) {
                                        CustomUtils.hideDialog();
                                        CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                                        //} else {
                                        //    CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        //}
                                    }

                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

                            Log.e("jsonObject", errorResult + "");
                        } else {
                            CustomUtils.toastMessage(getActivity(), "Please check your internet connection");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    Log.i("Success-DefaultAddress ", response.toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    jsonObject = new JSONObject(response.getJSONObject("results").toString());

                    String addressId = jsonObject.getString("id");
                    String streetAddress = jsonObject.getString("street_address");
                    String city = jsonObject.getString("city");
                    String zipCode = jsonObject.getString("zip_code");
                    String deliveryAddress = streetAddress + ", " + city + " ";

                    ProductModel.setAddressId(addressId);
                    ProductModel.setDeliveryAddress(deliveryAddress);

                    Log.i("addressId", ProductModel.getAddressId());
                    CustomUtils.loadFragment(new OrderSpecialInstructFragment(), true, getActivity());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                CustomUtils.hideDialog();

            }
        };
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                Log.i("resultcode", String.valueOf(resultCode));
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                Log.i("confirm", String.valueOf(confirm));
                if (confirm != null) {
                    try {
                        Log.i("TOSTRING 4", confirm.toJSONObject().toString(4));
                        Log.i("TOSTRING 4 GETPAYMENT", confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        Toast.makeText(
                                getActivity(),
                                "PaymentConfirmation info received from PayPal", Toast.LENGTH_LONG)
                                .show();
                    } catch (JSONException e) {
                        Log.i(Constants.TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(Constants.TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        Constants.TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == Constants.REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        PaypalUtils.sendAuthorizationToServer(auth);
                        Toast.makeText(
                                getActivity(),
                                "Future Payment code received from PayPal", Toast.LENGTH_LONG)
                                .show();

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == Constants.REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        PaypalUtils.sendAuthorizationToServer(auth);
                        Toast.makeText(
                                getActivity(),
                                "Profile Sharing code received from PayPal", Toast.LENGTH_LONG)
                                .show();

                    } catch (JSONException e) {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

}
