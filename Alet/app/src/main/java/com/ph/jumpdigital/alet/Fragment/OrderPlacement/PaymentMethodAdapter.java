package com.ph.jumpdigital.alet.Fragment.OrderPlacement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.ph.jumpdigital.alet.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vidalbenjoe on 07/10/2015.
 */
public class PaymentMethodAdapter extends BaseAdapter {

    public JSONObject jsonObject;
    ArrayList<HashMap<String, String>> _arraylist;
    Context context;
    LayoutInflater inflater;
    int selectedIndex = -1;

    public PaymentMethodAdapter(Context context,
                               ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        this._arraylist = new ArrayList<HashMap<String,String>>();
        this._arraylist.addAll(arraylist);
    }


    @Override
    public int getCount() {
        return _arraylist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_paymethod, viewGroup, false);

           holder.rbPaymentMethod = (RadioButton)view.findViewById(R.id.rbPayMethod);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.rbPaymentMethod.setText(_arraylist.get(position).get("name"));
        if(selectedIndex == position){
            holder.rbPaymentMethod.setChecked(true);
        }
        else{
            holder.rbPaymentMethod.setChecked(false);
        }
        return view;

    }


    public class ViewHolder {
        RadioButton rbPaymentMethod;

    }

    public void setSelectedIndex(int index){
        selectedIndex = index;
    }


}

