package com.ph.jumpdigital.alet.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Fragment.ItemListing.ProductListFragment;
import com.ph.jumpdigital.alet.Fragment.MyAccount.ProfileInfo;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.SharedPref;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by vidalbenjoe on 29/09/2015.
 */
public class LandingFragment extends Fragment {
    ImageView recommendedImg;
    Toolbar toolbar;
    public static JSONObject jsonObject;

    LinearLayout recommendedHolderLinear;
    HorizontalScrollView horizontalScrollViewHolder;

    String TAG = "LandingFragment";
    int screenSize;
    Boolean isFeatured;
    ImageView storeImg;
    TextView storetitle;
    String imageURL, strProdName;

    JsonObjectRequest jsonObjectRequest;
    SharedPref sharedPref;

    JSONArray typesResults, productsArray;
    JSONObject resultsObj, imageObj;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.landing_page_layout, container, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Welcome to Alét");
        storeImg = (ImageView) mainView.findViewById(R.id.storeImg);
        storetitle = (TextView) mainView.findViewById(R.id.storetitle);
        horizontalScrollViewHolder = (HorizontalScrollView) mainView.findViewById(R.id.horizontalScrollViewHolder);
        recommendedHolderLinear = (LinearLayout) mainView.findViewById(R.id.recommendedHolderLinear);

        sharedPref = new SharedPref(getActivity());
        CustomUtils.showDialog(getActivity());
        Constants.volleyTAG = "viewtypes";
        if (Constants.volleyTAG == "viewtypes") {
            String url = Constants.apiURL + Constants.recommended_alcohol_types;
            Log.i("URLLAND:", url);
            VolleyRequest request = new VolleyRequest(getActivity(),
                    Constants.getMethod, url,
                    null, null,
                    createRequestSuccessListener(),
                    createRequestErrorListener(),
                    getActivity(), true, Constants.volleyTAG);
            AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        }

        getFeaturedStore(Constants.getMethod, Constants.apiURL + Constants.getAllStores, true);

        screenSize = mainView.getResources().getConfiguration().densityDpi;
        return mainView;
    }


    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.i("Success-Recommended", response.toString());
                CustomUtils.hideDialog();
                if (Constants.volleyTAG == "viewtypes") {
                    if (response != null) {
                        try {
                            Constants.status = response.getBoolean("status");
                            typesResults = response.optJSONArray("results");
                            Log.i("Success-typesResults", String.valueOf(typesResults));
                            //Shuffle the content of JSONArray
                            CustomUtils.shuffleJsonArray(typesResults);
                            if (typesResults.length() > 3) {
                                for (int i = 0; i < 3; i++) {
                                    displayRecommendedTypes(resultsObj, typesResults, i);
                                }
                            } else {
                                if (typesResults.length() != 0) {
                                    for (int i = 0; i < typesResults.length(); i++) {
                                        displayRecommendedTypes(resultsObj, typesResults, i);
                                    }
                                } else {
                                    CustomUtils.loadFragment(new ProfileInfo(), false, getActivity());
                                    ProfileInfo.page = 2;

                                    Toast.makeText(getActivity(), "Preference not available ", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.i("LandingException", e.getMessage());
                        }
                    } else {
                        Toast.makeText(getActivity(), "No Result", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyErrorReturn(error);
            }
        };
    }

    public void displayRecommendedTypes(JSONObject resultsObject, JSONArray resultsTypeArr, int loop) throws JSONException {

        resultsObject = resultsTypeArr.optJSONObject(loop);
        JSONObject product_type = resultsObject.optJSONObject("product_type");
        strProdName = product_type.optString("name");
        productsArray = product_type.optJSONArray("products");
        Log.i("strProdName", strProdName);
        Log.i("productsArray", String.valueOf(productsArray));
        recommendedImg = new ImageView(getActivity());
        recommendedImg.setId(loop);
        recommendedImg.setBackgroundResource(R.drawable.border_lines);
        recommendedImg.setScaleType(ImageView.ScaleType.CENTER_CROP);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        params.setMargins(5, 20, 5, 0);
        recommendedImg.setLayoutParams(params);
        recommendedHolderLinear.setOrientation(LinearLayout.HORIZONTAL);//
        recommendedHolderLinear.addView(recommendedImg);
        recommendedImg.requestLayout();
        Log.i("ScreenSie:", "" + screenSize);
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                Log.i("SCREEN: ", "Large Screen");
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                Log.i("SCREEN: ", "Normal screen");
                recommendedImg.getLayoutParams().height = 200;
                recommendedImg.getLayoutParams().width = 200;
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                Log.i("SCREEN: ", "Small screen");
                break;
            default:
                Log.i("SCREEN: ", "Screen size is neither large, normal or small");
                if (screenSize <= 320) {
                    recommendedImg.getLayoutParams().height = 200;
                    recommendedImg.getLayoutParams().width = 200;
                } else {
                    recommendedImg.getLayoutParams().height = 295;
                    recommendedImg.getLayoutParams().width = 295;
                }
        }

        imageObj = product_type.getJSONObject("image");
        imageURL = imageObj.getString("url");

        JSONObject newImageObj = product_type.optJSONObject("image");
        final String newImage = newImageObj.optString("url");
        String imgReco = Constants.baseURL + newImage;

        Log.i("imgReco", imgReco);
        Glide.with(getActivity())
                .load(imgReco)
                .placeholder(R.drawable.defaultimage)
                .dontAnimate()
                .into(recommendedImg);
        /** initializing another variable to avoid random conflict of
         * data when sending to next activity */

//                                        final String strstore = resultsObj.getString("name");

        final JSONArray productArray = product_type.optJSONArray("products");
        Log.i("productArrayJ: ", String.valueOf(productArray));

        recommendedImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductListFragment prodFrag = new ProductListFragment();
                CustomUtils.loadFragment(prodFrag, true, getActivity());
                Log.i("PRODARLAND:", String.valueOf(productArray));
                Log.i("imagTapped:", String.valueOf(imageObj));
                Bundle bundle = new Bundle();
                bundle.putString("product_list", String.valueOf(productArray));
//                                                bundle.putString("object_name", strstore);
//                                              bundle.putString("object_header_image", newImage);
                prodFrag.setArguments(bundle);

            }
        });
    }

    public void getFeaturedStore(int method, String url, final Boolean requiredToken) {
        jsonObjectRequest = new JsonObjectRequest(method,
                url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray featuredResults = response.getJSONArray("results");
                    Log.i("featuredReturn:", "" + featuredResults.toString(4));
                    for (int i = 0; i < featuredResults.length(); i++) {
                        JSONObject featuredResultyObj = featuredResults.getJSONObject(i);
                        final String storeName = featuredResultyObj.getString("name");
                        JSONObject imageObj = featuredResultyObj.getJSONObject("image");
                        final String imageURL = imageObj.getString("url");
                        isFeatured = featuredResultyObj.getBoolean("featured");

                        if (isFeatured) {
                            storetitle.setText(storeName);
                            String imgBanner = Constants.baseURL + imageURL;
                            Glide.with(getActivity())
                                    .load(imgBanner)
                                    .placeholder(R.drawable.defaultimage)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .dontAnimate()
                                    .into(storeImg);

                            final JSONArray productsArrays = featuredResultyObj.optJSONArray("products");

                            storeImg.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ProductListFragment prodFrag = new ProductListFragment();
                                    //put product_list to model
                                    CustomUtils.loadFragment(prodFrag, true, getActivity());
                                    Bundle bundle = new Bundle();
                                    Log.i("storeFe-LandingPage", "" + productsArrays);
                                    bundle.putString("product_list", String.valueOf(productsArrays));
                                    bundle.putString("object_name", storeName);
                                    bundle.putString("object_header_image", imageURL);
                                    Log.i("ImageSToreURL:", imageURL);
                                    prodFrag.setArguments(bundle);
                                }
                            });
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorReturn(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                if (requiredToken.equals(true)) {
                    headers.put("x-auth-token", sharedPref.getAUTHToken());
                    Log.i("requiresToken ", "This request uses authentication token");
                }
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, Constants.volleyTAG);
    }


    public void VolleyErrorReturn(VolleyError error) {
        if (error != null) {
            try {
                if (error.networkResponse != null) {
                    String responseBody = new String(error.networkResponse.data, "UTF-8");
                    jsonObject = new JSONObject(responseBody);
                    Log.i("JSONResponse", jsonObject.toString());
                    JSONArray errorResult = jsonObject.getJSONArray("error");

                    String errorCode = jsonObject.getString("error_code");
                    String errorDesc = jsonObject.getString("error_description");

                    Log.i("errorCode:", errorCode);
                    Log.i("errorDesc:", errorDesc);
                    //get errror by index
                    for (int i = 0; i < errorResult.length(); i++) {
                        JSONObject errorObj = errorResult.getJSONObject(i);
                        Iterator keys = errorObj.keys();

                        while (keys.hasNext()) {
                            // LOOP TO GET DYNAMIC KEY
                            String currentDynamicKey = (String) keys.next();
                            Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                            // GET THE VALUE OF DYNAMIC KEY
                            String currentDynamicValue = errorObj.getString(currentDynamicKey);
                            Log.i("Error-DynamicKey", "" + currentDynamicKey);
                            Log.i("Error-DynamicValue", "" + currentDynamicValue);

                            if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                ActivityCompat.finishAffinity(getActivity());
                                getActivity().finish();
                                startActivity(gotoLogin);
                            } else if (currentDynamicValue.toString().contentEquals("No result")) {
                                CustomUtils.hideDialog();
                                CustomUtils.loadFragment(new ProfileInfo(), false, getActivity());
                                ProfileInfo.page = 2;
                                Toast.makeText(getActivity(), "Please select preferred alcohol types", Toast.LENGTH_SHORT).show();
                            } else {
                                CustomUtils.hideDialog();
                            }
                        }
                        Log.i("Error-JSONResponse", "" + errorResult.toString());
                    }

                    Log.e("jsonObject", errorResult + "");
                    //}
                } else {
                    Toast.makeText(getActivity(), "Can't sync data from server.\nPlease check your internet connection", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("errorJSON", "" + e);
                Toast.makeText(getActivity(), "Invalid Response from server", Toast.LENGTH_SHORT).show();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                Log.e("errorEncoding", "" + e);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        CustomUtils.hideDialog();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO your code to hide item here
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        getActivity().getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.action_search).setVisible(false).setEnabled(false);
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
        item.setEnabled(false);
        toolbar.hideOverflowMenu();
        return;
    }

}
