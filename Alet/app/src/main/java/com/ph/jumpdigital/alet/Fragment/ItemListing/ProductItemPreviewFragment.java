package com.ph.jumpdigital.alet.Fragment.ItemListing;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.LayerDrawable;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Database.AletDBHelper;
import com.ph.jumpdigital.alet.Database.AletDataSource;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.CustomHurlStack;
import com.ph.jumpdigital.alet.Utilities.Networking.OkHttpHurlStack;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import it.carlom.stikkyheader.core.StikkyHeaderBuilder;
import it.carlom.stikkyheader.core.animator.AnimatorBuilder;
import it.carlom.stikkyheader.core.animator.HeaderStikkyAnimator;

public class ProductItemPreviewFragment extends Fragment {
    public JSONObject jsonObject;
    ScrollView scrollviewdesc;
    ImageView mHeader_image;
    Cursor tempAvailableProd;
    String prodImageURL;
    private AletDBHelper aletdbHelper;
    private AletDataSource aletDBSource;
    int rowCount;
    LayerDrawable icon;
    Cursor cursor = null;
    Button previewaddtocart;
    String TAG = "ItemPreviewFragment";
    MenuItem cartItem;
    String productid, storeID, storeName, prodTitle, prodPrice, prodStockStat, prodDesc, prodSku;
    String strOrderProdId, strOrderId, strProdId, strStoreId, strProdStoreName, strProdName, strProdPrice, strProdTotal, strProdDesc,
            strProdSKU, strProdImage, strProdStockStatus;
    int intProdQuantity;
    int prodStockQuantity, prodTempStockAvail;
    TextView stockstatusTxt;
    public int existingItemCount;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        aletdbHelper = new AletDBHelper(getActivity());
        aletDBSource = new AletDataSource(getActivity());/**/
        aletDBSource.open();

        cursor = AletDataSource.cartOrderCount(UserModel.getEmail());
        if (cursor != null) {
            try {
                rowCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0))));
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        StikkyHeaderBuilder.stickTo(scrollviewdesc)
                .setHeader(R.id.headerpreview, (ViewGroup) getView())
                .minHeightHeaderDim(R.dimen.min_height_header)
                .animator(new ParallaxStikkyAnimator())
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.product_item_preview_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        scrollviewdesc = (ScrollView) view.findViewById(R.id.scrollviewdesc);
        TextView brandTitle = (TextView) view.findViewById(R.id.brandTitle);
        TextView storeNamePreview = (TextView) view.findViewById(R.id.storeNamePreview);
        stockstatusTxt = (TextView) view.findViewById(R.id.stockstatusTxt);
        TextView itempreviewPrice = (TextView) view.findViewById(R.id.itempreviewPrice);
        TextView proddesccontent = (TextView) view.findViewById(R.id.proddesccontent);
        previewaddtocart = (Button) view.findViewById(R.id.previewaddtocartbt);

        productid = this.getArguments().getString("prod_id");
        storeID = this.getArguments().getString("store_id");
        storeName = this.getArguments().getString("storename");
        prodTitle = this.getArguments().getString("prod_name");
        prodStockStat = this.getArguments().getString("stock_status");
        prodPrice = this.getArguments().getString("prod_price");
        prodDesc = this.getArguments().getString("description");
        prodSku = this.getArguments().getString("prod_sku");
        prodStockQuantity = this.getArguments().getInt("prod_stockquantity");
        prodTempStockAvail = this.getArguments().getInt("prod_temp_stock");

        prodImageURL = this.getArguments().getString("prod_imageurl");

        Log.i("pricePreview:", prodPrice);
        Log.i("prodQuantity:", String.valueOf(prodStockQuantity));

        storeNamePreview.setText(storeName);
        brandTitle.setText(prodTitle);

        try {
            tempAvailableProd = AletDataSource.getTempProductAvailable(productid);
            int tempQuantity = Integer.parseInt(tempAvailableProd.getString(tempAvailableProd.getColumnIndex(tempAvailableProd.getColumnName(0))));
            Log.i("tempQuan:", "" + tempQuantity);
            if (tempQuantity > 0) {
                stockstatusTxt.setText(prodStockQuantity + " In Stock");
                stockstatusTxt.setTextColor(getResources().getColor(R.color.md_green_600));
            } else {
                stockstatusTxt.setText("Out of Stock");
                stockstatusTxt.setTextColor(getResources().getColor(R.color.md_red_600));
            }
            if (tempQuantity == 0) {
                previewaddtocart.setEnabled(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.i("prodStockQuantityw:", "" + prodStockQuantity);
            if (prodStockQuantity > 0) {
                stockstatusTxt.setText(prodStockQuantity + " In Stock");
                stockstatusTxt.setTextColor(getResources().getColor(R.color.md_green_600));
            } else {
                stockstatusTxt.setText("Out of Stock");
                stockstatusTxt.setTextColor(getResources().getColor(R.color.md_red_600));
            }
            if (prodStockQuantity == 0) {
                previewaddtocart.setEnabled(false);
            }
        }

        itempreviewPrice.setText("₱ " + CustomUtils.decimalFormat(getActivity(), Double.parseDouble(prodPrice)));
        proddesccontent.setText(Html.fromHtml(prodDesc));
        previewaddtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkItemInCart();
            }
        });
    }

    public void checkItemInCart() {

        Log.i("PRODDDUCT_ID:", productid);
        int checkItem = AletDataSource.checkIteminCart(Integer.parseInt(productid));
        Log.i("checkItemInCart:", "" + checkItem);
        if (checkItem < 1) {
            //not exist
            tempAvailableProd = AletDataSource.getTempProductAvailable(productid);
            if (tempAvailableProd != null) {
                try {
                    Log.i("prodStockQuantity", String.valueOf(prodStockQuantity));
                    int decreaseProdQuan = prodStockQuantity;
                    decreaseProdQuan--;
                    Log.i("decreaseProdQuan", String.valueOf(decreaseProdQuan));
                    aletDBSource.addcartDB(productid, productid, productid, storeID, storeName, prodTitle, 1, prodPrice, prodPrice, prodDesc, prodSku, prodImageURL, prodStockStat, UserModel.getEmail(), prodStockQuantity, decreaseProdQuan);
                    existingItemCount = 1;
                    Log.i("quantityCountunTAP", String.valueOf(existingItemCount));
                    CustomUtils.setBadgeCount(getActivity(), icon, existingItemCount);
                    updateCartCount();
                } catch (Exception e) {
                    e.printStackTrace();
                    int decreaseProdQuan = prodStockQuantity;
                    decreaseProdQuan--;
                    aletDBSource.addcartDB(productid, productid, productid, storeID, storeName, prodTitle, 1, prodPrice, prodPrice, prodDesc, prodSku, prodImageURL, prodStockStat, UserModel.getEmail(), prodStockQuantity, decreaseProdQuan);
                    existingItemCount = 1;
                    Log.i("quantityCountTAPcatc", String.valueOf(existingItemCount));
                    CustomUtils.setBadgeCount(getActivity(), icon, existingItemCount);
                    updateCartCount();
                }
            }
        } else {
            Log.i("HASCONTENT:", "" + prodStockQuantity);
            //if product exist in cart
            Cursor cartRowCount = AletDataSource.cartOrderCount(UserModel.getEmail());
            int quantityCount = 0;
            try {
                quantityCount = Integer.parseInt(cartRowCount.getString(cartRowCount.getColumnIndex(cartRowCount.getColumnName(0))));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.i("PRODSOTCKQ:", "" + prodStockQuantity);
            Log.i("quantiyCount:", "" + quantityCount);
            tempAvailableProd = AletDataSource.getCurrentOrderQuantity(productid, UserModel.getEmail());
            int tempQuantity = Integer.parseInt(tempAvailableProd.getString(tempAvailableProd.getColumnIndex(tempAvailableProd.getColumnName(0))));
//            Log.i("tempQuantity:", "" + tempQuantity);


            if (tempQuantity < prodStockQuantity) {
                Log.i("TRUE", "" + "prodStockQuantity is grater than quantityCount:" + quantityCount + " < " + prodStockQuantity + " ProducID: " + productid);
                Log.i("quantiyCoun2t:", "" + quantityCount);
                Cursor tempProdCount = AletDataSource.getTempProductAvailable(productid);
                int tempProdStock;
                try {
                    tempProdStock = Integer.parseInt(tempProdCount.getString(tempProdCount.getColumnIndex(tempProdCount.getColumnName(0))));
                    Log.i("tempProdStock:", "" + tempProdStock);
                    int newProdStock = tempProdStock - 1;
                    Log.i("newProdStock:", "" + newProdStock);
                    aletDBSource.addcartDB(productid, productid, productid, storeID, storeName, prodTitle, 1, prodPrice, prodPrice, prodDesc, prodSku, prodImageURL, prodStockStat, UserModel.getEmail(), prodStockQuantity, newProdStock);
                    updateCartCount();
                } catch (Exception e) {
                    Log.i("exceptionERROR:", "");
                    e.printStackTrace();
                    existingItemCount = 1;
                    updateCartCount();
                }
            } else {
                Log.i("FALSE", "" + "prodStockQuantity is less than quantity count:" + quantityCount + " > " + prodStockQuantity + " ProducID: " + productid);
                previewaddtocart.setEnabled(false);
                stockstatusTxt.setText("Out of Stock");
                stockstatusTxt.setTextColor(getResources().getColor(R.color.md_red_600));
                CustomUtils.toastMessage(getActivity(), "Sorry, you already added all " + prodTitle + " to cart");
            }
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
//        popBackStack();
    }

    private class ParallaxStikkyAnimator extends HeaderStikkyAnimator {
        @Override
        public AnimatorBuilder getAnimatorBuilder() {
            mHeader_image = (ImageView) getHeader().findViewById(R.id.item_preview_image);


//          mHeader_image.setDefaultImageResId(R.drawable.defaultimage);
            String imgURL = Constants.baseURL + prodImageURL;
            Glide.with(getActivity())
                    .load(imgURL)
                    .dontAnimate()
                    .placeholder(R.drawable.defaultimage)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .fitCenter()
                    .into(mHeader_image);

//            mHeader_image.buildDrawingCache();
//            Bitmap bitmap = mHeader_image.getDrawingCache();
//
//            scaleCropToFit(bitmap, 200, 90);

            /**
             //mImageLoader = AppController.getInstance().getImageLoader();
             Set the URL of the image that should be loaded into this view, and
             specify the ImageLoader that will be used to make the request.
             */
            return AnimatorBuilder.create().applyVerticalParallax(mHeader_image);
        }
    }

    public static Bitmap scaleCropToFit(Bitmap original, int targetWidth, int targetHeight) {
        //Need to scale the image, keeping the aspect ration first
        int width = original.getWidth();
        int height = original.getHeight();

        float widthScale = (float) targetWidth / (float) width;
        float heightScale = (float) targetHeight / (float) height;
        float scaledWidth;
        float scaledHeight;

        int startY = 0;
        int startX = 0;

        if (widthScale > heightScale) {
            scaledWidth = targetWidth;
            scaledHeight = height * widthScale;
            //crop height by...
            startY = (int) ((scaledHeight - targetHeight) / 2);
        } else {
            scaledHeight = targetHeight;
            scaledWidth = width * heightScale;
            //crop width by..
            startX = (int) ((scaledWidth - targetWidth) / 2);
        }

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(original, (int) scaledWidth, (int) scaledHeight, true);

        Bitmap resizedBitmap = Bitmap.createBitmap(scaledBitmap, startX, startY, targetWidth, targetHeight);
        return resizedBitmap;
    }

    private void deleteAllCartItems() {
        HttpStack httpStack;
        if (Build.VERSION.SDK_INT > 19) {
            httpStack = new CustomHurlStack();
        } else if (Build.VERSION.SDK_INT >= 9 && Build.VERSION.SDK_INT <= 19) {
            httpStack = new OkHttpHurlStack();
        } else {
            httpStack = new HttpClientStack(AndroidHttpClient.newInstance("Android"));
        }
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), httpStack);
        String url = Constants.apiURL + Constants.deleteAllCartItems;
        Log.i(TAG, url);
        Constants.volleyTAG = "deleteAllCartItems";

        JsonObjectRequest deleteRequest = new JsonObjectRequest(
                com.android.volley.Request.Method.DELETE,
                url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("OkHttp-ItemDeleted: ", response.toString());

                aletDBSource.deleteAllCartByEmail(UserModel.getEmail());
                // Toast.makeText(getActivity(), "All cart items deleted", Toast.LENGTH_SHORT).show();
                CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                CustomUtils.hideDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("ErrorMessage: ", error.getMessage());
                try {
                    if (error.getMessage() != null) {
                        String responseBody = String.valueOf(error.getMessage());
                        JSONObject jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");

                        String errorCode = jsonObject.getString("error_code");
                        String errorDesc = jsonObject.getString("error_description");

                        Log.i("errorCode:", errorCode);
                        Log.i("errorDesc:", errorDesc);

                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();

                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                // do something here with the value...
                                // Toast.makeText(getActivity(), currentDynamicValue, Toast.LENGTH_SHORT).show();
                                CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                CustomUtils.hideDialog();

                            }
                            Log.i("Error-JSONResponse", "" + errorResult.toString());
                        }

                        Log.e("jsonObject", errorResult + "");
                    } else {
                        Toast.makeText(getActivity(), "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                        CustomUtils.hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                    Toast.makeText(getActivity(), "Please check your internet connection",
                            Toast.LENGTH_SHORT).show();
                }
                CustomUtils.hideDialog();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("x-auth-token", UserModel.getAuthToken());
                return headers;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                String json;
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    try {
                        json = new String(volleyError.networkResponse.data,
                                HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
                    } catch (UnsupportedEncodingException e) {
                        return new VolleyError(e.getMessage());
                    }
                    return new VolleyError(json);
                }
                return volleyError;
            }
        };

        requestQueue.add(deleteRequest);
    }


    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
//                                //You have an array
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");

                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);
                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);

                                    if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                        CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                        gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ActivityCompat.finishAffinity(getActivity());
                                        startActivity(gotoLogin);
                                        getActivity().finish();

                                    } else {
                                        // if (rowCount > 0) {
                                        CustomUtils.hideDialog();
                                        CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                                        //} else {
                                        //    CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        //}
                                    }
                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }
                            Log.e("jsonObject-PPF", errorResult + "");
                            //  }
                        } else {
                            Toast.makeText(getActivity(), "Please check your internet connection",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                        Toast.makeText(getActivity(), "Invalid Response from server", Toast.LENGTH_SHORT).show();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.i("Success-order", response.toString());

                if (Constants.volleyTAG.equals("getcartitems")) {
                    try {
                        jsonObject = new JSONObject(response.toString()).getJSONObject("results");
                        JSONArray jsonArr = new JSONArray();
                        jsonArr = jsonObject.getJSONArray("order_product");

                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject jsonObj = jsonArr.getJSONObject(i);

                            if (jsonObj.has("product")) {
                                JSONObject jsonObjProduct = jsonObj.getJSONObject("product");

                                // JSONObject -- jsonObj
                                strOrderProdId = jsonObj.getString("id");
                                strOrderId = jsonObj.getString("order_id");
                                intProdQuantity = Integer.parseInt(jsonObj.getString("quantity"));
                                strProdTotal = jsonObj.getString("total");
                                strProdStoreName = jsonObj.getString("name");

                                // JSONObject -- jsonObjProduct
                                strProdId = jsonObjProduct.getString("id");
                                strStoreId = jsonObjProduct.getString("store_id");
                                strProdName = jsonObjProduct.getString("name");
                                strProdPrice = jsonObjProduct.getString("price");
                                strProdDesc = jsonObjProduct.getString("description");
                                strProdSKU = jsonObjProduct.getString("sku");
                                strProdImage = jsonObjProduct.getJSONObject("image").getString("url");
                                strProdStockStatus = jsonObjProduct.getString("in_stock");

//                                Log.i("ProductID-S",
//                                        strOrderProdId + " "
//                                                + strOrderId + " "
//                                                + strProdId + " "
//                                                + strStoreId + " "
//                                                + strProdName + " "
//                                                + intProdQuantity + " "
//                                                + strProdPrice + " "
//                                                + strProdTotal + " "
//                                                + strProdDesc + " "
//                                                + strProdSKU + " "
//                                                + strProdImage + " "
//                                                + strProdStockStatus);


                                aletDBSource.syncCartDB(strOrderProdId, strOrderId, strProdId, strStoreId, strProdStoreName, strProdName, intProdQuantity, strProdPrice, strProdTotal, strProdDesc, strProdSKU, strProdImage, strProdStockStatus, UserModel.getEmail());
                                CustomUtils.hideDialog();
                            } else {
                                Log.i(TAG, "Product key is not available");
                                deleteAllCartItems();
                            }
                        }
                        CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    if (response != null) {

                        try {
                            jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                            Constants.status = response.getBoolean("status");

                            Log.d("JSON-ORDER:", jsonObject.toString());
                            if (Constants.status.toString().contentEquals("true")) {
                                CustomUtils.hideDialog();
                                // ShoppingCartFragment shoppingCartFrag = new ShoppingCartFragment();
                                // CustomUtils.loadFragment(shoppingCartFrag, true, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                    else {
//                        Toast.makeText(context.getApplicationContext(), "Successfully Updated",
//                                Toast.LENGTH_LONG).show();
//                        CustomUtils.hideDialog();
//                    }
                    }
                }
            }
        };
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.cart_menu, menu); // removed to not double the menu items
        cartItem = menu.findItem(R.id.showcart);
        Cursor cartRowCount = AletDataSource.cartOrderCount(UserModel.getEmail());
        icon = (LayerDrawable) cartItem.getIcon();

        int checkItem = AletDataSource.checkIteminCart(Integer.parseInt(productid));
        if (checkItem < 1) {
            try {
                int quantityCount = Integer.parseInt(cartRowCount.getString(cartRowCount.getColumnIndex(cartRowCount.getColumnName(0))));
                Log.i("quantityCount: ", String.valueOf(quantityCount));
                CustomUtils.setBadgeCount(getActivity(), icon, quantityCount);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                int quantityCount = Integer.parseInt(cartRowCount.getString(cartRowCount.getColumnIndex(cartRowCount.getColumnName(0))));
                Log.i("quantityCount: ", String.valueOf(quantityCount));
                CustomUtils.setBadgeCount(getActivity(), icon, quantityCount);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            CustomUtils.setBadgeCount(getActivity(), icon, 1);
        }
//        cartItem.setIcon(getResources().getDrawable(R.drawable.iconcart));
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.showcart:
                getCartItems();

//                if (rowCount > 0) {
//                    CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
//                } else {
//                    Toast.makeText(getActivity(), "No Item in Cart", Toast.LENGTH_LONG).show();
//                }
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    public void getCartItems() {
        String url = Constants.apiURL + Constants.viewCart;
        Constants.volleyTAG = "getcartitems";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());
    }

    public void updateCartCount() {
        Cursor cartRowCount = AletDataSource.cartOrderCount(UserModel.getEmail());
        icon = (LayerDrawable) cartItem.getIcon();
        try {
            int quantityCount = Integer.parseInt(cartRowCount.getString(cartRowCount.getColumnIndex(cartRowCount.getColumnName(0))));
            Log.i("quantityCount: ", String.valueOf(quantityCount));
            CustomUtils.setBadgeCount(getActivity(), icon, quantityCount);
        } catch (Exception e) {
            Log.i("CountexistingItem: ", String.valueOf(existingItemCount));
            CustomUtils.setBadgeCount(getActivity(), icon, 1);
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        aletDBSource.close();
    }

}
