package com.ph.jumpdigital.alet.model;

import java.io.Serializable;

/**
 * Created by luigigo on 11/27/15.
 */
public class RegistrationModel implements Serializable {

    public static String email, password;
    public static String firstname;
    public static String lastname;
    public static String gender;
    public static String mobile;
    public static String facebookId;
    public static String birthday;
    public static String addressLabel, streetAdd, city, zipCode;
    public static String latitude, longitude;
    public static Boolean registering;
    public static String month, day, years;

    public static String getLastname() {
        return lastname;
    }

    public static void setLastname(String lastname) {
        RegistrationModel.lastname = lastname;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        RegistrationModel.email = email;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        RegistrationModel.password = password;
    }

    public static String getGender() {
        return gender;
    }

    public static void setGender(String gender) {
        RegistrationModel.gender = gender;
    }

    public static String getMobile() {
        return mobile;
    }

    public static void setMobile(String mobile) {
        RegistrationModel.mobile = mobile;
    }

    public static String getFacebookId() {
        return facebookId;
    }

    public static void setFacebookId(String facebookId) {
        RegistrationModel.facebookId = facebookId;
    }

    public static String getBirthday() {
        return birthday;
    }

    public static void setBirthday(String birthday) {
        RegistrationModel.birthday = birthday;
    }

    public static String getAddressLabel() {
        return addressLabel;
    }

    public static void setAddressLabel(String addressLabel) {
        RegistrationModel.addressLabel = addressLabel;
    }

    public static String getStreetAdd() {
        return streetAdd;
    }

    public static void setStreetAdd(String streetAdd) {
        RegistrationModel.streetAdd = streetAdd;
    }

    public static String getCity() {
        return city;
    }

    public static void setCity(String city) {
        RegistrationModel.city = city;
    }

    public static String getZipCode() {
        return zipCode;
    }

    public static void setZipCode(String zipCode) {
        RegistrationModel.zipCode = zipCode;
    }

    public static String getLatitude() {
        return latitude;
    }

    public static void setLatitude(String latitude) {
        RegistrationModel.latitude = latitude;
    }

    public static String getLongitude() {
        return longitude;
    }

    public static void setLongitude(String longitude) {
        RegistrationModel.longitude = longitude;
    }

    public static Boolean getRegistering() {
        return registering;
    }

    public static void setRegistering(Boolean registering) {
        RegistrationModel.registering = registering;
    }

    public static String getMonth() {
        return month;
    }

    public static void setMonth(String month) {
        RegistrationModel.month = month;
    }

    public static String getDay() {
        return day;
    }

    public static void setDay(String day) {
        RegistrationModel.day = day;
    }

    public static String getYears() {
        return years;
    }

    public static void setYears(String years) {
        RegistrationModel.years = years;
    }

    public static String getFirstname() {
        return firstname;
    }

    public static void setFirstname(String firstname) {
        RegistrationModel.firstname = firstname;
    }
}
