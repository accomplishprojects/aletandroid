package com.ph.jumpdigital.alet.registration;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.StartScreenActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.PaypalUtils;
import com.ph.jumpdigital.alet.Utilities.SharedPref;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.Utilities.libraries.TimeDatePicker.SlideDateTimeListener;
import com.ph.jumpdigital.alet.Utilities.libraries.TimeDatePicker.SlideDateTimePicker;
import com.ph.jumpdigital.alet.model.RegistrationModel;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils.checkPlayServices;

/**
 * Created by luigigo on 9/5/15.
 */

public class RegisterUser extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, OnMapReadyCallback {

    public static Integer page = 0;
    String[] title;
    int[] layouts;
    int[] buttons;
    String thisIsVia;
    LinearLayout layout_date;
    Button btnNext;
    ImageView imgShowMap;
    CheckBox chkAgeValidation, chkTermsOfUse;
    EditText edtEmail, edtPassword, edtFirstname, edtLastname,
            edtMobile;
    Spinner spnGender, spnCity;
    AutoCompleteTextView edtStreetAdd;
    TextView txtDay, txtMonth, txtYear;

    int years, month, day;
    String email, password, firstname, lastname, gender, mobile, streetAdd, city, zipcode;
    String mapTapIndicator = "untap";
    InputFilter filter = null;
    Intent i;
    private SimpleDateFormat dayFormatter, monthFormatter, yearFormatter;
    private SlideDateTimeListener listener;
    private UserModel userModel = new UserModel();
    static ArrayList<HashMap<String, String>> resultList1 = null;

    // Google Places API TAGS
    static HttpURLConnection conn = null;
    static StringBuilder jsonResponse = null;
    static String GOOGLE_PLACES_TAG = "GooglePlaces";
    int via;
    private Button btnLoginToPaypal;
    boolean canRegister = false;
    SharedPref sharedPref;
    int currentYear, allowedYear;
    MapFragment supportmapfragment;
    GoogleMap supportMap;
    Marker marker;
    LatLng _point;
    CameraPosition cameraPosition;

    private static PayPalConfiguration config = new PayPalConfiguration()

            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)

            .clientId(Constants.CONFIG_CLIENT_ID)

            // Minimally, you will need to set three merchant information properties.
            // These should be the same values that you provided to PayPal when you registered your app.
            .merchantName("Alet")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.alet.com.ph/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.alet.com.ph/legal"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new SharedPref(this);

        i = getIntent();
        via = i.getIntExtra("via", 0);
        Log.i("via", "" + via);
        if (via == 0) {
            page = 1;
            thisIsVia = "facebook";
        }else{
            thisIsVia = "alet";
        }
        currentYear = Calendar.getInstance().get(Calendar.YEAR);
        allowedYear = currentYear - 21;

        logUserModel();
        initVars();
    }

    private void logUserModel() {
        String TAG = "logUserModel";
        Log.i(TAG, "Email: " + String.valueOf(UserModel.getEmail() == null));
        Log.i(TAG, "Password: " + String.valueOf(UserModel.getPassword() == null));
        Log.i(TAG, "Firstname: " + String.valueOf(UserModel.getFirstname() == null));
        Log.i(TAG, "Lastname: " + String.valueOf(UserModel.getLastname() == null));
        Log.i(TAG, "Gender: " + String.valueOf(UserModel.getGender() == null));
        Log.i(TAG, "Mobile: " + String.valueOf(UserModel.getMobile() == null));
        Log.i(TAG, "Birthday: " + String.valueOf(UserModel.getBirthday() == null));
        Log.i(TAG, "Street Address: " + String.valueOf(UserModel.getStreetAdd() == null));
        Log.i(TAG, "City: " + String.valueOf(UserModel.getCity() == null));
        Log.i(TAG, "Zip Code: " + String.valueOf(UserModel.getZipCode() == null));
        Log.i(TAG, "Latitude: " + String.valueOf(UserModel.getLatitude() == null));
        Log.i(TAG, "Longitude: " + String.valueOf(UserModel.getLongitude() == null));

    }

    private void initVars() {
        title = getResources().getStringArray(R.array.regitration_titles);
        layouts = new int[]{
                R.layout.register_alet,
                R.layout.register_fillingprofile,
                R.layout.register_agevalidation,
                R.layout.register_termsofuse,
                R.layout.register_deliveryaddress,
                R.layout.register_paymentmethod,
                R.layout.register_successful
        };
        buttons = new int[]{
                R.id.btnNext1,
                R.id.btnNext2,
                R.id.btnNext3,
                R.id.btnNext4,
                R.id.btnNext5,
                R.id.btnSkip,
                R.id.btnNext6
        };

        setUpPage();
        clearFields();

        Log.v("getFirstname: 1", "" + RegistrationModel.getFirstname());
        Log.v("getLaststname: 1", "" + RegistrationModel.getLastname());
        Log.v("getGender: 1", "" + RegistrationModel.getGender());


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnNext1:
                email = edtEmail.getText().toString();
                password = edtPassword.getText().toString();

                if (email.isEmpty() && password.isEmpty()) {
                    edtEmail.setError("Fields can't be left blank");
                    edtPassword.setError("Fields can't be left blank");
                } else if (email.isEmpty()) {
                    edtEmail.setError("Invalid Email");
                } else if (password.isEmpty()) {
                    edtPassword.setError("Invalid Password");
                } else if (CustomUtils.isValidEmail(email) == false) {
                    edtEmail.setError("Please input a valid email");
                } else if (password.length() < 8) {
                    CustomUtils.AlphaNumFilter(edtPassword, filter);
                    edtPassword.setError("Minimum of 8 Alphanumeric Character");
                } else {
                    page++;
                    setUpPage();
                    userModel.setEmail(email);
                    userModel.setPassword(password);

                }

                break;

            case R.id.btnNext2:
//                Log.i("Email-ButtonClick :", email);
                if (via == 0) {
                    userModel.setEmail(email);
                }
                firstname = edtFirstname.getText().toString();
                lastname = edtLastname.getText().toString();

                gender = spnGender.toString();
                mobile = edtMobile.getText().toString();

                if (firstname.isEmpty() && lastname.isEmpty() && mobile.isEmpty()) {
                    edtFirstname.setError("Field can't be left blank");
                    edtLastname.setError("Field can't be left blank");
                    edtMobile.setError("Field can't be left blank");
                } else if (firstname.isEmpty()) {
                    edtFirstname.setError("Field can't be left blank");
                } else if (lastname.isEmpty()) {
                    edtLastname.setError("Field can't be left blank");
                } else if (mobile.isEmpty()) {
                    edtMobile.setError("Fields can't be left blank");
                } else if (mobile.length() < 11) {
                    edtMobile.setError("Mobile number must be 11 digits");
                } else {
                    page = 1;
                    page++;
                    setUpPage();

                    userModel.setFirstname(edtFirstname.getText().toString());
                    userModel.setLastname(edtLastname.getText().toString());
                    userModel.setGender(spnGender.getSelectedItem().toString());
                    userModel.setMobile(edtMobile.getText().toString());
                }

                break;

            case R.id.btnNext3:
                if (txtDay.getText().equals("") && txtMonth.getText().equals("") && txtYear.getText().equals("")) {
                    Toast.makeText(RegisterUser.this, "Invalid Date", Toast.LENGTH_SHORT).show();
                } else if (years > allowedYear) {
                    Toast.makeText(RegisterUser.this, "Must be 21 years old and above", Toast.LENGTH_LONG).show();
                } else if (chkAgeValidation.isChecked() == false) {
                    Toast.makeText(RegisterUser.this, "Please confirm your age above", Toast.LENGTH_LONG).show();

                } else {
                    page++;
                    setUpPage();

                }

                break;
            case R.id.btnNext4:
                if (chkTermsOfUse.isChecked() == false) {
                    Toast.makeText(RegisterUser.this, "Please agree in terms specified above", Toast.LENGTH_LONG).show();
                } else {
                    page++;
                    setUpPage();
                }

                break;
            case R.id.btnNext5:
                if (canRegister) {
                    streetAdd = edtStreetAdd.getText().toString();
                    city = spnCity.getSelectedItem().toString();
//                zipcode = edtZipCode.getText().toString();
                    Log.i("Luigi", streetAdd);

                    if (streetAdd.isEmpty() && city.isEmpty()) {
                        edtStreetAdd.setError("Fields can't be left blank");
                        //edtCity.setError("Fields can't be left blank");
                    } else if (streetAdd.isEmpty()) {
                        edtStreetAdd.setError("Field can't be left blank");
                    } else if (city.isEmpty()) {
                        //edtCity.setError("Field can't be left blank");
                    } else {
                        userModel.setStreetAdd(streetAdd);
                        userModel.setCity(city);
                        // userModel.setZipCode(zipcode);
//                    page++;
//                    setUpPage();
                        //getLatLong();

                        if (UserModel.getLatitude() == null && UserModel.getLongitude() == null) {
                            Toast.makeText(RegisterUser.this, "Please pick a location first", Toast.LENGTH_SHORT).show();

                        } else {
                            register();
                            //Login User in background
                        }

                    }
                    Log.i("Page - ", String.valueOf(page));
                } else {
                    Toast.makeText(RegisterUser.this, "Sorry selected address is not applicable", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnSkip:
                page++;
                setUpPage();

                Log.i("Page - ", String.valueOf(page));
                break;
            case R.id.btnNext6:
                sharedPref.setAUTHToken("");
                page = 0;
                i = new Intent(RegisterUser.this, StartScreenActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                RegisterUser.this.finish();

                break;

            case R.id.layout_date1:
                setUpDateTimeDialog();
                new SlideDateTimePicker.Builder(RegisterUser.this.getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(new Date())
                        //.setMinDate(new Date())
                        //.setMaxDate(maxDate)
                        //.setIs24HourTime(true)
                        .setTheme(SlideDateTimePicker.HOLO_LIGHT)
                        .setIndicatorColor(getResources().getColor(R.color.theme_color))
                        .build()
                        .show();
                break;

            case R.id.imgShowMap:
                imgShowMap.setEnabled(false);
                final Dialog dialog = new Dialog(RegisterUser.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_map);
                dialog.setCancelable(false);

                Button btnDismissMap = (Button) dialog.findViewById(R.id.btnDismissMap);
                btnDismissMap.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(RegisterUser.this, Locale.getDefault());
                        try {
                            if (_point != null) {
                                addresses = geocoder.getFromLocation(_point.latitude, _point.longitude, 1);
                                String city = addresses.get(0).getAddressLine(1);
                                // CustomUtils.toastMessage(RegisterUser.this, "LongLat:" + city + ": " + _point.latitude + ", " + _point.longitude);
                                Log.i("MarkerPos: ", "LAT: " + marker.getPosition().latitude + " LNG: " + marker.getPosition().longitude);

                                if (city.contains("Quezon City")) {
                                    canRegister = true;
                                    spnCity.setSelection(0);
                                    Toast.makeText(RegisterUser.this, "Address located", Toast.LENGTH_SHORT).show();

                                } else if (city.contains("Makati")) {
                                    canRegister = true;
                                    spnCity.setSelection(1);
                                    Toast.makeText(RegisterUser.this, "Address located", Toast.LENGTH_SHORT).show();

                                } else if (city.contains("Pasig")) {
                                    canRegister = true;
                                    spnCity.setSelection(2);
                                    Toast.makeText(RegisterUser.this, "Address located", Toast.LENGTH_SHORT).show();

                                } else {
                                    canRegister = false;
                                    Toast.makeText(RegisterUser.this, "Sorry selected address is not applicable", Toast.LENGTH_SHORT).show();
                                }


                                UserModel.setLatitude(String.valueOf(_point.latitude));
                                UserModel.setLongitude(String.valueOf(_point.longitude));

                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            Toast.makeText(RegisterUser.this, "Can't get location. Please check your internet connection", Toast.LENGTH_SHORT).show();

                        }
                        imgShowMap.setEnabled(true);
                        dialog.dismiss();
                        onDestroyView();
                    }
                });
                if (checkPlayServices(RegisterUser.this)) {
                    setUpMap();
                } else {
                    CustomUtils.toastMessage(RegisterUser.this, "Google Services not Installed");
                }
                dialog.show();
                break;

            default:

                break;
        }

    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        //getLatLong();
    }

    private void getLatLong() {
        String strDesc = null;
        streetAdd = edtStreetAdd.getText().toString();

        try {
            strDesc = URLEncoder.encode(streetAdd, "utf8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = Constants.GOOGLE_PLACES_BASE_URL + Constants.GOOGLE_GEOCODING + strDesc + Constants.GOOGLE_GEOCODING_API_KEY;
        Log.i("URL: ", url);
        Constants.volleyTAG = "getLatLng";
        VolleyRequest request = new VolleyRequest(RegisterUser.this,
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                RegisterUser.this, false, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(this);

    }

    private void register() {
        // Setting up parameters for request(POST)
        Map<String, String> params = new HashMap<String, String>();
        if (via == 0) {
            params.put("via", "facebook");
        } else {
            params.put("via", "alet");
        }
        Log.i("Email-register :", UserModel.getEmail());

        params.put("email", UserModel.getEmail());
        if (via == 1) {
            params.put("password", UserModel.getPassword());
            params.put("password_confirmation", UserModel.getPassword());
        }

        params.put("first_name", UserModel.getFirstname());
        params.put("last_name", UserModel.getLastname());
        params.put("birthday", UserModel.getBirthday());
        params.put("gender", UserModel.getGender());
        params.put("facebook_id", UserModel.getFacebookId());
        params.put("mobile_number", UserModel.getMobile());
        params.put("user_role_id", "1");
        params.put("is_deleted", "0");
        params.put("label", "Home");
        params.put("street_address", UserModel.getStreetAdd().toString());
        params.put("city", UserModel.getCity());
        params.put("latitude", UserModel.getLatitude());
        params.put("longtitude", UserModel.getLongitude());
        params.put("default_address", "1");


        String url = Constants.apiURL + Constants.userRegister;

        Constants.volleyTAG = "register";
        VolleyRequest request = new VolleyRequest(RegisterUser.this,
                Constants.postMethod, url,
                (HashMap<String, String>) params, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                RegisterUser.this, false, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(this);
        Log.i("volleyTagReg:", Constants.volleyTAG);
    }

    public static ArrayList<HashMap<String, String>> autocomplete(String input) {

        try {
            jsonResponse = new StringBuilder();
            StringBuilder sb = new StringBuilder(Constants.GOOGLE_PLACES_BASE_URL + Constants.GOOGLE_PLACES_AUTOCOMPLETE);
            sb.append(Constants.GOOGLE_PLACES_API_KEY);
            sb.append("&components=country:ph");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());


            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResponse.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(GOOGLE_PLACES_TAG, "Error processing Places API URL", e);
            return resultList1;
        } catch (IOException e) {
            Log.e(GOOGLE_PLACES_TAG, "Error connecting to Places API", e);
            return resultList1;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Log.i("GOOGLE-PLACES_RESULT", jsonResponse.toString());
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResponse.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList1 = new ArrayList<HashMap<String, String>>();
            for (int i = 0; i < predsJsonArray.length(); i++) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("desc", predsJsonArray.getJSONObject(i).getString("description"));
                hm.put("place_id", predsJsonArray.getJSONObject(i).getString("place_id"));
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println(predsJsonArray.getJSONObject(i).getString("place_id"));
                System.out.println("============================================================");
                resultList1.add(hm);
            }
        } catch (JSONException e) {
            Log.e(GOOGLE_PLACES_TAG, "Cannot process JSON results", e);
        }
        return resultList1;


    }

    private void setUpPage() {
        setContentView(layouts[page]);
        getSupportActionBar().setTitle(title[page]);

        btnNext = (Button) findViewById(buttons[page]);
        btnNext.setOnClickListener(this);

        if (page == 0) {
            edtEmail = (EditText) findViewById(R.id.edtEmail);
            edtPassword = (EditText) findViewById(R.id.edtPassword);

            Log.i("registering?", UserModel.getRegistering().toString());
            if (UserModel.getRegistering() == true) {
                edtEmail.setText(UserModel.getEmail());
                edtPassword.setText(UserModel.getPassword());
            }

        } else if (page == 1) {
            if (via == 0) {
                try {
                    email = UserModel.getEmail();
                    Log.i("Email-Setup :", email);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            edtFirstname = (EditText) findViewById(R.id.edtFirstname);
            edtLastname = (EditText) findViewById(R.id.edtLastname);
            spnGender = (Spinner) findViewById(R.id.edtGender);
            edtMobile = (EditText) findViewById(R.id.edtMobile);

            edtFirstname.setText(UserModel.getFirstname());
            edtLastname.setText(UserModel.getLastname());


            // if (userModel.getRegistering() == true) {
            edtFirstname.setText(RegistrationModel.getFirstname());
            edtLastname.setText(RegistrationModel.getLastname());
            if (RegistrationModel.getGender() != null) {
                if (RegistrationModel.getGender().toString().contentEquals("Male") ||
                        RegistrationModel.getGender().toString().contentEquals("male")) {
                    spnGender.setSelection(0);
                } else {
                    spnGender.setSelection(1);
                }
                // }
                edtMobile.setText(UserModel.getMobile());
            }

        } else if (page == 2) {
            layout_date = (LinearLayout) findViewById(R.id.layout_date1);
            txtDay = (TextView) findViewById(R.id.txtDay);
            txtMonth = (TextView) findViewById(R.id.txtMonth);
            txtYear = (TextView) findViewById(R.id.txtYear);
            chkAgeValidation = (CheckBox) findViewById(R.id.chkAgeValidation);

            txtMonth.setText(UserModel.getMonth());
            txtDay.setText(UserModel.getDay());
            txtYear.setText(UserModel.getYears());

            layout_date.setOnClickListener(this);

        } else if (page == 3) {
            chkTermsOfUse = (CheckBox) findViewById(R.id.chkTermsOfUse);

        } else if (page == 4) {
            imgShowMap = (ImageView) findViewById(R.id.imgShowMap);
            imgShowMap.setOnClickListener(this);

            edtStreetAdd = (AutoCompleteTextView) findViewById(R.id.edtStreetAdd);
            edtStreetAdd.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
            edtStreetAdd.setOnItemClickListener(this);
            spnCity = (Spinner) findViewById(R.id.spnCity);
            // edtZipCode = (EditText) findViewById(R.id.edtZipCode);

            if (UserModel.getRegistering() == true) {
                edtStreetAdd.setText(UserModel.getStreetAdd());
                //edtCity.setText(userModel.getCity());
                // edtZipCode.setText(userModel.getZipCode());
            }

        } else if (page == 5) {
            btnLoginToPaypal = (Button) findViewById(R.id.btnLoginToPaypal);
            btnLoginToPaypal.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View v) {
                    onFuturePaymentPressed(v);
                }
            });

        }

    }

    private void setUpDateTimeDialog() {

        dayFormatter = new SimpleDateFormat("dd");
        monthFormatter = new SimpleDateFormat("MM");
        yearFormatter = new SimpleDateFormat("yyyy");


        listener = new SlideDateTimeListener() {
            @Override
            public void onDateTimeSet(Date date) {

                month = Integer.parseInt(monthFormatter.format(date));
                day = Integer.parseInt(dayFormatter.format(date));
                years = Integer.parseInt(yearFormatter.format(date));

                UserModel.setMonth(month + "");
                UserModel.setDay(day + "");
                UserModel.setYears(years + "");

                txtMonth.setText(monthFormatter.format(date));
                txtDay.setText(dayFormatter.format(date));
                txtYear.setText(yearFormatter.format(date));

                userModel.setBirthday(years + "-" + month + "-" + day);

            }

            // Optional cancel listener
            @Override
            public void onDateTimeCancel() {
                Toast.makeText(RegisterUser.this,
                        "Canceled", Toast.LENGTH_SHORT).show();
            }
        };

    }

    @Override
    public void onBackPressed() {
        Log.i("OnBackPressed: ", String.valueOf(page));
        if (page == 1 && via == 0) {
            clearFields();
            RegisterUser.this.finish();

        } else if (page == 0 && via != 0) {
            page = 0;
            clearFields();
            RegisterUser.this.finish();

        } else if (page == 5) {

        } else {
            page--;
            setUpPage();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void setUpMap() {

        supportmapfragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        supportmapfragment.getMapAsync(this);
        supportMap = supportmapfragment.getMap();
        supportMap.getUiSettings().setMapToolbarEnabled(false);
        supportMap.getUiSettings().setZoomControlsEnabled(false);

        GoogleMapOptions options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_SATELLITE)
                .compassEnabled(false)
                .rotateGesturesEnabled(false)
                .tiltGesturesEnabled(true);

        if (UserModel.getLatitude() != null && UserModel.getLongitude() != null) {
            marker = supportMap.addMarker(new MarkerOptions().position(
                    new LatLng(Double.parseDouble(UserModel.getLatitude()), Double.parseDouble(UserModel.getLongitude()))));

            // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(UserModel.getLatitude()), Double.parseDouble(UserModel.getLongitude())))      // Sets the center of the map to Mountain View
                    .zoom(11)                   // Sets the zoom
                    .bearing(360)                // Sets the orientation of the camera to east
//                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
        } else {
            // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
            cameraPosition = new CameraPosition.Builder()
                    .target(Constants.METRO_MANILA_LOC)      // Sets the center of the map to Mountain View
                    .zoom(11)                   // Sets the zoom
                    .bearing(360)                // Sets the orientation of the camera to east
//                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
        }

        supportMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        supportMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                _point = point;
                supportMap.clear();
                marker = supportMap.addMarker(new MarkerOptions().position(
                        new LatLng(point.latitude, point.longitude)));
            }
        });

    }

    public void onDestroyView() {
        android.app.Fragment fragment = (getFragmentManager().findFragmentById(R.id.map));
        android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.remove(fragment);
        ft.commit();
    }

    private void clearFields() {
        // userModel.setRegistering(false);
        userModel.setEmail(null);
        userModel.setPassword(null);

        userModel.setFirstname(null);
        userModel.setLastname(null);
        userModel.setGender(null);
        userModel.setMobile(null);

        UserModel.setMonth(null);
        UserModel.setDay(null);
        UserModel.setYears(null);

        userModel.setStreetAdd(null);
        userModel.setCity(null);
        userModel.setZipCode(null);
        UserModel.setLatitude(null);
        UserModel.setLongitude(null);

    }

    public void onFuturePaymentPressed(View pressed) {
//        String metadataIdfuture = PayPalConfiguration.getClientMetadataId(this);
//        Log.i("metadataIdfuture", String.valueOf(metadataIdfuture));
        Intent intent = new Intent(RegisterUser.this, PayPalFuturePaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startActivityForResult(intent, Constants.REQUEST_CODE_FUTURE_PAYMENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                Log.i("resultcode", String.valueOf(resultCode));
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);

                Log.i("confirm", String.valueOf(confirm));
                if (confirm != null) {
                    try {
                        Log.i("TOSTRING 4", confirm.toJSONObject().toString(4));
                        Log.i("TOSTRING 4 GETPAYMENT", confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        Toast.makeText(
                                RegisterUser.this,
                                "PaymentConfirmation info received from PayPal", Toast.LENGTH_LONG)
                                .show();
                    } catch (JSONException e) {
                        Log.i(Constants.TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(Constants.TAG, "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        Constants.TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == Constants.REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        String authorization_code = auth.getAuthorizationCode();

                        sendAuthorizationToServer(auth);
                        Log.i("sendAuth:", authorization_code);
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));


                        JSONObject clientObj = auth.toJSONObject().getJSONObject("client");
                        String paypalenvironment = clientObj.getString("environment");
                        String paypalsdkversion = clientObj.getString("paypal_sdk_version");
                        String paypalplatform = clientObj.getString("platform");
                        String paypalprodname = clientObj.getString("product_name");


                        JSONObject paypalRespObj = auth.toJSONObject().getJSONObject("response");
                        String paypalcode = paypalRespObj.getString("code");
                        String paypaltype = auth.toJSONObject().getString("response_type");

                        PaypalUtils.setPaypalEnvironment(paypalenvironment);
                        PaypalUtils.setPaypalSDKVersion(paypalsdkversion);
                        PaypalUtils.setPaypalPlatform(paypalplatform);
                        PaypalUtils.setPaypalProdName(paypalprodname);

                        PaypalUtils.setPaypalRespCode(paypalcode);
                        PaypalUtils.setPaypalRespType(paypaltype);

                        String metadataIdfuture = PayPalConfiguration.getClientMetadataId(RegisterUser.this);
                        Log.i("metadataIdfuture", String.valueOf(metadataIdfuture));

                        HashMap<String, String> params = new HashMap<String, String>();

                        params.put("payment_list_id", "1");
                        params.put("environment", paypalenvironment);
                        params.put("paypal_sdk_version", paypalsdkversion);
                        params.put("platform", paypalplatform);
                        params.put("product_name", paypalprodname);
                        params.put("client_metadata_id", metadataIdfuture);

                        params.put("response_code", paypalcode);
                        params.put("response_type", paypaltype);

                        String url = Constants.apiURL + Constants.user_payment_paypal;

                        Constants.volleyTAG = "paypal";
                        VolleyRequest request = new VolleyRequest(RegisterUser.this,
                                Constants.postMethod, url,
                                params, null,
                                createRequestSuccessListener(),
                                createRequestErrorListener(),
                                RegisterUser.this, true, Constants.volleyTAG);
                        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
                        PaypalUtils.sendAuthorizationToServer(auth);

                        Toast.makeText(
                                RegisterUser.this,
                                "Future Payment code received from PayPal", Toast.LENGTH_LONG)
                                .show();

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == Constants.REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        PaypalUtils.sendAuthorizationToServer(auth);
                        Toast.makeText(
                                RegisterUser.this,
                                "Profile Sharing code received from PayPal", Toast.LENGTH_LONG)
                                .show();

                    } catch (JSONException e) {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

        Log.i("authToServer: ", String.valueOf(authorization));
        // TODO:
        // Send the authorization response to your server, where it can exchange the authorization code
        // for OAuth access and refresh tokens.
        //
        // Your server must then store these tokens, so that your server code can execute payments
        // for this user in the future.

    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            Log.i("RESPONSEBODY", responseBody);
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");
                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);

                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // loop to get the dynamic key
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                    // get the value of the dynamic key
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    // do something here with the value...
                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                    Toast.makeText(RegisterUser.this, currentDynamicValue, Toast.LENGTH_SHORT).show();
                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

                            Log.e("jsonObject", errorResult + "");
                        } else {
                            Toast.makeText(RegisterUser.this, "Please check your internet connection",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (Constants.volleyTAG == "login") {
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                        Boolean status = response.getBoolean("status");

                        UserModel.setUser_id(jsonObject.getString("user_id"));
                        UserModel.setAuthToken(jsonObject.getString("auth_token"));
                        userModel.setEmail(jsonObject.getString("email"));
                        UserModel.setStatus(response.getBoolean("status"));

                        sharedPref.setAUTHToken(jsonObject.getString("auth_token"));
                        if (status.toString().contentEquals("true")) {
                            CustomUtils.hideDialog();
                            clearFields();
                            page++;
                            setUpPage();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (Constants.volleyTAG == "paypal") {
                    page++;
                    setUpPage();
                } else if (Constants.volleyTAG.equals("getLatLng")) {
                    //Parsing JSONResponse
                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        JSONArray jsonArray = jsonObject.getJSONArray("results");
                        UserModel.setLatitude(null);
                        UserModel.setLongitude(null);

                        Log.i("SuccessgetLang", response.toString());
                        if (jsonObject.getString("status").equals("OK")) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONArray jsonArrayAddress = new JSONArray();
                                jsonArrayAddress = jsonArray.getJSONObject(i).getJSONArray("address_components");
                                for (int c = 0; c < jsonArrayAddress.length(); c++) {

                                    JSONArray jsonArrTypes = jsonArrayAddress.getJSONObject(c).getJSONArray("types");
                                    for (int d = 0; d < jsonArrTypes.length(); d++) {
                                        String strJsonArrTypes = jsonArrTypes.getString(d);
                                        Log.i("JsonArrTypes", strJsonArrTypes);
                                        if (strJsonArrTypes.equals("locality")) {
                                            Toast.makeText(RegisterUser.this, jsonArrayAddress.getJSONObject(c).getString("long_name"), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }

                                JSONObject jsonLocation = jsonArray.getJSONObject(i).getJSONObject("geometry").getJSONObject("location");
                                String strLat = jsonLocation.getString("lat");
                                String strLng = jsonLocation.getString("lng");
                                Log.i("LAT", strLat + " index" + i + " LNG " + strLng);

                                UserModel.setLatitude(strLat);
                                UserModel.setLongitude(strLng);
                                if (UserModel.getLatitude() == null && UserModel.getLongitude() == null) {
                                    Toast.makeText(RegisterUser.this, "Invalid Address", Toast.LENGTH_SHORT).show();
                                    CustomUtils.hideDialog();
                                } else {
                                    CustomUtils.hideDialog();
//                                    Log.i("GotoReg: ", UserModel.getEmail());
                                    register();
                                    //Login User in background
                                }
                            }
                        } else {
                            Toast.makeText(RegisterUser.this, "Invalid Address", Toast.LENGTH_SHORT).show();
                            CustomUtils.hideDialog();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (Constants.volleyTAG == "register") {
                    Toast.makeText(RegisterUser.this, "Registration successful", Toast.LENGTH_LONG).show();

//                    Log.i("logEmail: ", UserModel.getEmail());
//                    Log.i("logPassword: ", UserModel.getPassword());
//                    Log.i("logGCM: ", UserModel.getGCMToken());

                    UserModel.setLongitude(null);
                    UserModel.setLatitude(null);

                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("via", thisIsVia);
                    params.put("password", UserModel.getPassword());
                    params.put("email", UserModel.getEmail());
                    params.put("device_token", UserModel.getGCMToken());
                    if (params != null) {
                        String url = Constants.apiURL + Constants.userSignin;
                        Constants.volleyTAG = "login";
                        VolleyRequest request = new VolleyRequest(RegisterUser.this,
                                Constants.postMethod, url,
                                params, null,
                                createRequestSuccessListener(),
                                createRequestErrorListener(),
                                RegisterUser.this, true, Constants.volleyTAG);
                        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);

                        Log.i("Register-Params", params + "");
                    }
                }
            }
        };
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

}
