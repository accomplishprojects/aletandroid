
package com.ph.jumpdigital.alet.Fragment.ItemListing;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.LayerDrawable;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Database.AletDBHelper;
import com.ph.jumpdigital.alet.Database.AletDataSource;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.CustomHurlStack;
import com.ph.jumpdigital.alet.Utilities.Networking.OkHttpHurlStack;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.ProductModel;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import it.carlom.stikkyheader.core.animator.AnimatorBuilder;
import it.carlom.stikkyheader.core.animator.HeaderStikkyAnimator;
//Original

/**
 * Created by vidalbenjoe on 02/10/2015.
 */
public class ProductListFragment extends Fragment {
    ArrayList<HashMap<String, String>> arraylist, arrayListBatch;
    ProdItemListAdapter adapter;
    public ListView mListView;
    TextView brandTitle;
    JSONObject itemStoreObj = null;
    HashMap<String, String> map;

    String prodList;
    String header_image;
    String prodlistTitle;
    TextView noItemTxt;
    ImageView mHeader_image;
    JSONArray productArray;
    JSONObject jsonObject;
    Toolbar toolbar;
    static int rowCount;
    static Context context;
    String TAG = "ProductListFragment";
    AletDataSource dbsource;
    AletDBHelper dbhelper;
    static LayerDrawable icon;
    public int productTotalQuantity = 0;
    public int totalItemQuantity = 0;
    static Cursor cursor = null;
    int limit = 10;
    String strOrderProdId, strOrderId, strProdId, strStoreId, strProdStoreName, strProdName, strProdPrice, strProdTotal, strProdDesc,
            strProdSKU, strProdImage, strProdStockStatus;

    FrameLayout headerFrame, layout_container;


    public ProductListFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        StikkyHeaderBuilder.stickTo(mListView)
//                .setHeader(R.id.headerFrame, (ViewGroup) getView())
//                .minHeightHeaderDim(R.dimen.min_height_header)
//                .animator(new ParallaxStikkyAnimator())
//                .build();
        mListView.setAdapter(adapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        dbhelper = new AletDBHelper(getActivity());
        dbsource = new AletDataSource(getActivity());/**/
        dbsource.open();
        context = getActivity();
        cursor = AletDataSource.cartOrderCount(UserModel.getEmail());
        if (cursor != null) {
            try {
                rowCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0))));
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(prodlistTitle);
        arraylist = new ArrayList<HashMap<String, String>>();

        prodList = this.getArguments().getString("product_list");

        header_image = this.getArguments().getString("object_header_image");
        prodlistTitle = this.getArguments().getString("object_name");
        Log.i("fromFeaturedStore:", "" + prodlistTitle);

        if (prodList != null) {
            try {
                productArray = new JSONArray(prodList);
                //loop content of JSONArray
                for (int i = 0; i < productArray.length(); i++) {
                    JSONObject newOb = productArray.getJSONObject(i);
                    JSONObject imageObj = newOb.getJSONObject("image");
                    JSONArray prodQuantityObj = newOb.optJSONArray("product_quantity");
                    itemStoreObj = newOb.optJSONObject("store");

                    if (!newOb.optString("price").equals("null")
                            && !newOb.optString("name").equals("null")) {
//                  JSONObject storeObj = newOb.getJSONObject("store");
                        Log.d("newOB: ", newOb.toString(4));
                        map = new HashMap<String, String>();
                        map.put("prod_id", newOb.optString("id"));
                        map.put("store_id", newOb.optString("store_id"));
                        map.put("prod_name", newOb.optString("name"));
                        map.put("prod_price", newOb.optString("price"));
                        map.put("description", newOb.optString("description"));

                        map.put("prod_sku", newOb.optString("sku"));
                        if (itemStoreObj != null) {
                            map.put("store_name", itemStoreObj.optString("name"));
                        }
                        map.put("stock_status", newOb.optString("in_stock"));
                        map.put("prod_imageurl", imageObj.optString("url"));


                        if (prodQuantityObj != null) {
                            for (int k = 0; k < prodQuantityObj.length(); k++) {
                                map.put("prod_quantity", String.valueOf(prodQuantityObj));
                                Log.i("prod_quantity:", String.valueOf(prodQuantityObj));
                            }
                        }

                        arraylist.add(map);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(getActivity(), "No Result", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.product_main_layout, container, false);
        brandTitle = (TextView) mainView.findViewById(R.id.brandTitle);
        mListView = (ListView) mainView.findViewById(R.id.itemlistinglistview);
        noItemTxt = (TextView) mainView.findViewById(R.id.noItemTxt);
        layout_container = (FrameLayout) mainView.findViewById(R.id.layout_container);
        headerFrame = (FrameLayout) mainView.findViewById(R.id.headerFrame);
        headerFrame.setVisibility(View.GONE);
        Log.i("ProdList", prodList);

        if (prodList.isEmpty() || prodList.contentEquals("[]")) {
            layout_container.setVisibility(View.GONE);
            noItemTxt.setVisibility(View.VISIBLE);
        }

        arrayListBatch = new ArrayList<HashMap<String, String>>();
        if (limit < arraylist.size()) {
            int lastIndex = limit;
            int remainingIndex = (arraylist.size() - limit);
            //if (remainingIndex >= 10) {
            //limit += 10;
            Log.i("Greaterlimit:", String.valueOf(arraylist.size()));
            for (int i = 0; i < limit; i++) {
                hashMaps(i);
            }
//                                        } else {
//                                            limit += remainingIndex;
//                                            for (int i = 0; i < arraylist.size(); i++) {
//                                                HashMap<String, String> hm = new HashMap<String, String>();
//                                                hm.put("object_name", arraylist.get(i).get("object_name"));
//                                                hm.put("object_image", arraylist.get(i).get("object_image"));
//                                                hm.put("products_array", String.valueOf(productsArray));
//                                                arrayListBatch.add(hm);
//                                            }
//                                        }
        } else {
            int lastIndex = limit;
            //limit += 10;

            for (int i = 0; i < arraylist.size(); i++) {
                Log.i("Lesslimit:", String.valueOf(arraylist.size()));
                hashMaps(i);
            }
        }

        brandTitle.setText(prodlistTitle);
        adapter = new ProdItemListAdapter(getActivity(), arrayListBatch);
        return mainView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                int threshold = 1;
                int count = mListView.getCount();
                if (scrollState == SCROLL_STATE_IDLE) {
                    if (mListView.getLastVisiblePosition() >= count
                            - threshold) {
                        // Execute LoadMoreDataTask AsyncTask
                        new LoadMoreDataTask().execute();
                        Snackbar snackbar = Snackbar
                                .make(view, "Loading more data...", Snackbar.LENGTH_SHORT);
                        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
                        snackbarLayout.addView(new ProgressBar(getActivity()));
                        snackbar.show();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
            }
        });
    }


    private class ParallaxStikkyAnimator extends HeaderStikkyAnimator {
        @Override
        public AnimatorBuilder getAnimatorBuilder() {
            mHeader_image = (ImageView) getHeader().findViewById(R.id.header_image);
            // mHeader_image.setDefaultImageResId(R.drawable.defaultimage);
            String imgURL = Constants.baseURL + header_image;
//            mRequestQueue = Volley.newRequestQueue(context);
//            mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
//                private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
//
//                public void putBitmap(String url, Bitmap bitmap) {
//                    mCache.put(url, bitmap);
//                }
//
//                public Bitmap getBitmap(String url) {
//                    return mCache.get(url);
//                }
//            });
//
//            mHeader_image.setImageUrl(imgURL, mImageLoader);//Volley
//
////            imageLoader.DisplayImage(imgURL, mHeader_image);

            Glide.with(getActivity())
                    .load(imgURL)
                    .placeholder(R.drawable.defaultimage)
                    .fitCenter()
                    .override(1000, 700)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(mHeader_image);
            /**
             //mImageLoader = AppController.getInstance().getImageLoader();
             Set the URL of the image that should be loaded into this view, and
             specify the ImageLoader that will be used to make the request.
             */
            return AnimatorBuilder.create().applyVerticalParallax(mHeader_image);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.cart_menu, menu); // removed to not double the menu items
        MenuItem cartItem = menu.findItem(R.id.showcart);
        icon = (LayerDrawable) cartItem.getIcon();
        CustomUtils.setBadgeCount(getActivity(), icon, rowCount);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.showcart:
                getCartItems();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    public void getCartItems() {
        String url = Constants.apiURL + Constants.viewCart;
        Constants.volleyTAG = "getcartitems";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());
    }

    private void deleteAllCartItems() {
        HttpStack httpStack;
        if (Build.VERSION.SDK_INT > 19) {
            httpStack = new CustomHurlStack();
        } else if (Build.VERSION.SDK_INT >= 9 && Build.VERSION.SDK_INT <= 19) {
            httpStack = new OkHttpHurlStack();
        } else {
            httpStack = new HttpClientStack(AndroidHttpClient.newInstance("Android"));
        }
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), httpStack);
        String url = Constants.apiURL + Constants.deleteAllCartItems;
        Log.i(TAG, url);
        Constants.volleyTAG = "deleteAllCartItems";

        JsonObjectRequest deleteRequest = new JsonObjectRequest(
                com.android.volley.Request.Method.DELETE,
                url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("OkHttp-ItemDeleted: ", response.toString());

                dbsource.deleteAllCartByEmail(UserModel.getEmail());
                // Toast.makeText(getActivity(), "All cart items deleted", Toast.LENGTH_SHORT).show();
                CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                CustomUtils.hideDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("ErrorMessage: ", error.getMessage());
                try {
                    if (error.getMessage() != null) {
                        String responseBody = String.valueOf(error.getMessage());
                        JSONObject jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");

                        String errorCode = jsonObject.getString("error_code");
                        String errorDesc = jsonObject.getString("error_description");

                        Log.i("errorCode:", errorCode);
                        Log.i("errorDesc:", errorDesc);

                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();

                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                // do something here with the value...
                                // Toast.makeText(getActivity(), currentDynamicValue, Toast.LENGTH_SHORT).show();
                                CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                CustomUtils.hideDialog();

                            }
                            Log.i("Error-JSONResponse", "" + errorResult.toString());
                        }

                        Log.e("jsonObject", errorResult + "");
                    } else {
                        Toast.makeText(getActivity(), "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                        CustomUtils.hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                    Toast.makeText(getActivity(), "Please check your internet connection",
                            Toast.LENGTH_SHORT).show();
                }
                CustomUtils.hideDialog();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("x-auth-token", UserModel.getAuthToken());
                return headers;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                String json;
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    try {
                        json = new String(volleyError.networkResponse.data,
                                HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
                    } catch (UnsupportedEncodingException e) {
                        return new VolleyError(e.getMessage());
                    }
                    return new VolleyError(json);
                }
                return volleyError;
            }
        };

        requestQueue.add(deleteRequest);
    }

    // This class is for load more functionality of listview
    private class LoadMoreDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TITLES", String.valueOf(arraylist.size()));
            Log.i("TITLES-LIMIT", String.valueOf(limit));

            if (limit < arraylist.size()) {
                int lastIndex = limit;
                int remainingIndex = (arraylist.size() - limit);
                if (remainingIndex >= 5) {
                    limit += 5;
                    for (int i = lastIndex; i < limit; i++) {
                        hashMaps(i);
                    }
                } else {
                    limit += remainingIndex;
                    for (int i = lastIndex; i < arraylist.size(); i++) {
                        hashMaps(i);
                    }
                }
            } else {
                int lastIndex = limit;
                for (int i = lastIndex; i < arraylist.size(); i++) {
                    hashMaps(i);
                }
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            // Locate listview last item
            int position = mListView.getLastVisiblePosition();
            // Pass the results into ListViewAdapter.java
            adapter = new ProdItemListAdapter(getActivity(), arrayListBatch);
            // Binds the Adapter to the ListView
//            listview.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            // Show the latest retrived results on the top
            mListView.setSelectionFromTop(position, 0);
            // Close the progressdialog


        }
    }

    public void hashMaps(int loop) {
        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("prod_id", arraylist.get(loop).get("prod_id"));
        hm.put("store_id", arraylist.get(loop).get("store_id"));
        hm.put("prod_name", arraylist.get(loop).get("prod_name"));
        hm.put("prod_price", arraylist.get(loop).get("prod_price"));
        hm.put("description", arraylist.get(loop).get("description"));
        hm.put("prod_sku", arraylist.get(loop).get("prod_sku"));
        hm.put("store_name", arraylist.get(loop).get("store_name"));
        hm.put("stock_status", arraylist.get(loop).get("stock_status"));
        hm.put("prod_imageurl", arraylist.get(loop).get("prod_imageurl"));
        hm.put("prod_quantity", arraylist.get(loop).get("prod_quantity"));
        if (arraylist.get(loop).get("prod_quantity") != null) {
            try {
                JSONArray quantityArr = new JSONArray(arraylist.get(loop).get("prod_quantity"));
                Log.i("quantityArr:", String.valueOf(quantityArr));
                if (quantityArr != null) {
                    for (int k = 0; k < quantityArr.length(); k++) {
                        JSONObject quantityObjects = quantityArr.getJSONObject(k);
                        int itemquantity = quantityObjects.getInt("quantity");
                        totalItemQuantity += itemquantity;
                        hm.put("item_quantity", String.valueOf(totalItemQuantity));
                        Log.i("PRDQNTQT:", quantityObjects.getString("quantity"));
                        Log.i("quantityObjects:", String.valueOf(quantityObjects));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        arrayListBatch.add(hm);
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            jsonObject = new JSONObject(responseBody);
//                            Log.i("JSONResponse", jsonObject.toString());

//                                //You have an array
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");

                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                    if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                        CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                        gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ActivityCompat.finishAffinity(getActivity());
                                        startActivity(gotoLogin);
                                        getActivity().finish();

                                    } else {
                                        // if (rowCount > 0) {
                                        CustomUtils.hideDialog();
                                        CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                                        //} else {
                                        //    CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        //}
                                    }
                                }

//                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

//                            Log.e("jsonObject-PLF", errorResult + "");
                            //   }
                        } else {
                            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                        Toast.makeText(getActivity(), "Invalid Response from server", Toast.LENGTH_SHORT).show();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Success", response.toString());
                try {
                    jsonObject = new JSONObject(response.toString()).getJSONObject("results");
                    JSONArray jsonArr = new JSONArray();
                    jsonArr = jsonObject.getJSONArray("order_product");

                    for (int i = 0; i < jsonArr.length(); i++) {
                        int intProdQuantity;

                        JSONObject jsonObj = jsonArr.getJSONObject(i);
                        if (jsonObj.has("product")) {
                            JSONObject jsonObjProduct = jsonObj.getJSONObject("product");

                            // JSONObject -- jsonObj
                            strOrderProdId = jsonObj.getString("id");
                            strOrderId = jsonObj.getString("order_id");
                            intProdQuantity = Integer.parseInt(jsonObj.getString("quantity"));
                            strProdTotal = jsonObj.getString("total");
                            strProdStoreName = jsonObj.getString("name");

                            // JSONObject -- jsonObjProduct
                            strProdId = jsonObjProduct.getString("id");
                            strStoreId = jsonObjProduct.getString("store_id");
                            strProdName = jsonObjProduct.getString("name");
                            strProdPrice = jsonObjProduct.getString("price");
                            strProdDesc = jsonObjProduct.getString("description");
                            strProdSKU = jsonObjProduct.getString("sku");
                            strProdImage = jsonObjProduct.getJSONObject("image").getString("url");
                            strProdStockStatus = jsonObjProduct.getString("in_stock");

                            dbsource.syncCartDB(strOrderProdId, strOrderId, strProdId, strStoreId, strProdStoreName, strProdName, intProdQuantity, strProdPrice, strProdTotal, strProdDesc, strProdSKU, strProdImage, strProdStockStatus, UserModel.getEmail());
                            CustomUtils.hideDialog();
                        } else {
                            Log.i(TAG, "Product key is not available");
                            deleteAllCartItems();
                        }
                    }
                    CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                } catch (JSONException e) {

                    CustomUtils.toastMessage(getActivity(), "No Product Available");
                    CustomUtils.hideDialog();
                    e.printStackTrace();
                }
            }
        };
    }

    public static void updateCartCount() {
        cursor = AletDataSource.cartOrderCount(UserModel.getEmail());
        if (cursor != null) {
            try {
                rowCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0))));
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
                int rowTotalCount = AletDataSource.cartRowItemCount(UserModel.getEmail());
                CustomUtils.setBadgeCount(context, icon, rowTotalCount);
            }
        } else {
            int rowTotalCount = AletDataSource.cartRowItemCount(UserModel.getEmail());
            rowCount = 1;
            CustomUtils.setBadgeCount(context, icon, rowTotalCount);
        }
//        Log.i("rowCountProdList", String.valueOf(rowCount));
        CustomUtils.setBadgeCount(context, icon, rowCount);
    }

    public void onStart() {
        super.onStart();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(prodlistTitle);
        cursor = AletDataSource.cartOrderCount(UserModel.getEmail());
        if (cursor != null) {
            try {
                rowCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0))));
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dbsource.close();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rowCount = 0;
    }

    public class ProdItemListAdapter extends BaseAdapter {
        public JSONObject jsonObject;
        Context context;
        LayoutInflater inflater;
        ArrayList<HashMap<String, String>> data;
        private RequestQueue mRequestQueue;
        String imgURL;

        Button addtocartDialogBT, cancelAddtoBT;
        int itemCounter = 1;
        float priceTotal;

        String productid;
        String storeid;
        String storename;
        String prod_title;
        String stock_stat;
        String prodDesc;
        String prodSKU;
        String prodIMG;

        int prodStockQuantity;
        float prod_price;
        Dialog dialog;
        private AletDBHelper aletdbHelper;
        private AletDataSource aletDBSource;
        int tempProdAvailable, checkItem;
        Cursor tempAvailableProd;
        String tempProdStock;
        JSONArray quantityArray;
        ViewHolder holder;

        public ProdItemListAdapter(Context context,
                                   ArrayList<HashMap<String, String>> arraylist) {
            this.context = context;
            data = arraylist;

            aletdbHelper = new AletDBHelper(context);
            aletDBSource = new AletDataSource(context);/**/
            aletDBSource.open();

        }

        public class ViewHolder {
            TextView itemStore;
            TextView itemTitle;
            TextView itemPrice;
            TextView productQuantity;
            ImageView productItemThumb;
            FrameLayout gotoprodpreviewBT;
            FrameLayout gotoaddtocartBT;

            HashMap<String, String> resultp = new HashMap<String, String>();

        }


        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {


            if (view == null) {
                holder = new ViewHolder();
                inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.product_list_layout, viewGroup, false);
                holder.productItemThumb = (ImageView) view.findViewById(R.id.productItemThumb);
//                holder.productItemThumb.setDefaultImageResId(R.drawable.defaultimage);
                holder.itemStore = (TextView) view.findViewById(R.id.itemStore);
                holder.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
                holder.itemPrice = (TextView) view.findViewById(R.id.itemPrice);
                holder.productQuantity = (TextView) view.findViewById(R.id.productQuantity);

                holder.gotoprodpreviewBT = (FrameLayout) view.findViewById(R.id.gotoprodpreviewBT);
                holder.gotoaddtocartBT = (FrameLayout) view.findViewById(R.id.gotoaddtocartBT);
                view.setTag(holder);

            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.resultp = data.get(position);
            if (holder.resultp.get("store_name") != null) {
                holder.itemStore.setText(holder.resultp.get("store_name"));
            } else {
                holder.itemStore.setText(ProductModel.getStoreName());
            }
            holder.itemTitle.setText(holder.resultp.get("prod_name"));
            holder.itemPrice.setText("₱ " + CustomUtils.decimalFormat(getActivity(), Double.parseDouble(holder.resultp.get("prod_price"))));

            try {
                productTotalQuantity = 0;
                if (holder.resultp.get("prod_quantity") != null) {
                    JSONArray quantityArray = new JSONArray(holder.resultp.get("prod_quantity"));
                    if (quantityArray != null) {
                        for (int i = 0; i < quantityArray.length(); i++) {
                            JSONObject quantityObj = quantityArray.getJSONObject(i);
                            int quantity = quantityObj.getInt("quantity");
                            productTotalQuantity += quantity;
                            prodStockQuantity = productTotalQuantity;
                            if (prodStockQuantity <= 0) {
                                holder.productQuantity.setText("Qty:  " + 0);
                            } else {
                                holder.productQuantity.setText("Qty:  " + productTotalQuantity);
                            }
                            Log.i("prodquan_settext:", String.valueOf(productTotalQuantity));
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            imgURL = Constants.baseURL + holder.resultp.get("prod_imageurl");

            Glide.with(getActivity())
                    .load(imgURL)
                    .placeholder(R.drawable.defaultimage)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.productItemThumb);

            holder.productItemThumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.resultp = data.get(position);
                    ProductItemPreviewFragment prodPreview = new ProductItemPreviewFragment();
                    CustomUtils.loadFragment(prodPreview, true, context);
                    Bundle bundle = new Bundle();
                    bundle.putString("prod_id", holder.resultp.get("prod_id"));
                    bundle.putString("name", holder.resultp.get("name"));
                    bundle.putString("store_id", holder.resultp.get("store_id"));
                    bundle.putString("storename", holder.resultp.get("store_name"));
                    bundle.putString("prod_name", holder.resultp.get("prod_name"));
                    bundle.putString("prod_price", holder.resultp.get("prod_price"));
                    bundle.putString("stock_status", holder.resultp.get("stock_status"));
                    bundle.putString("description", holder.resultp.get("description"));
                    bundle.putString("prod_sku", holder.resultp.get("prod_sku"));
                    bundle.putString("prod_imageurl", holder.resultp.get("prod_imageurl"));
                    bundle.putInt("prod_temp_stock", tempProdAvailable);

                    if (holder.resultp.get("store_name") != null) {
                        bundle.putString("storename", holder.resultp.get("store_name"));
                    } else {
                        bundle.putString("storename", storename = ProductModel.getStoreName());
                    }

                    try {
                        productTotalQuantity = 0;
                        if (holder.resultp.get("prod_quantity") != null) {
                            JSONArray quantityArray = new JSONArray(holder.resultp.get("prod_quantity"));
                            if (quantityArray != null) {
                                for (int i = 0; i < quantityArray.length(); i++) {
                                    JSONObject quantityObj = quantityArray.getJSONObject(i);
                                    int quantity = quantityObj.getInt("quantity");
                                    productTotalQuantity += quantity;
                                    prodStockQuantity = productTotalQuantity;
                                    if (prodStockQuantity > 0) {
                                        bundle.putInt("prod_stockquantity", productTotalQuantity);
                                    } else {
                                        bundle.putInt("prod_stockquantity", 0);
                                    }
                                    Log.i("prod_stockquantityprev:", String.valueOf(productTotalQuantity));
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ProductModel.setName(holder.resultp.get("prod_name"));
                    prodPreview.setArguments(bundle);
                }
            });

            holder.gotoaddtocartBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.resultp = data.get(position);
                    productid = holder.resultp.get("prod_id");
                    storeid = holder.resultp.get("store_id");
                    storename = holder.resultp.get("store_name");
                    if (holder.resultp.get("store_name") != null) {
                        storename = holder.resultp.get("store_name");
                    } else {
                        storename = ProductModel.getStoreName();
                    }
                    prod_title = holder.resultp.get("prod_name");
                    prod_price = Float.parseFloat(holder.resultp.get("prod_price"));
                    stock_stat = holder.resultp.get("stock_status");
                    prodDesc = holder.resultp.get("description");
                    prodSKU = holder.resultp.get("prod_sku");
                    prodIMG = holder.resultp.get("prod_imageurl");
//                    Log.i("addtocartprodstorename:", storename);
                    try {
                        productTotalQuantity = 0;
                        if (holder.resultp.get("prod_quantity") != null) {
                            quantityArray = new JSONArray(holder.resultp.get("prod_quantity"));
                            for (int i = 0; i < quantityArray.length(); i++) {
                                JSONObject quantityObj = quantityArray.getJSONObject(i);
                                int quantity = quantityObj.getInt("quantity");

                                productTotalQuantity += quantity;

                                prodStockQuantity = productTotalQuantity;
                                Log.i("AddToCarQuan:", String.valueOf(prodStockQuantity));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    tempAvailableProd = AletDataSource.getTempProductAvailable(productid);
                    checkItem = AletDataSource.checkIteminCart(Integer.parseInt(productid));
                    showAddToCartDialog();
                }
            });

            holder.gotoprodpreviewBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.resultp = data.get(position);
                    ProductItemPreviewFragment prodPreview = new ProductItemPreviewFragment();
                    CustomUtils.loadFragment(prodPreview, true, context);
                    Bundle bundle = new Bundle();
                    bundle.putString("prod_id", holder.resultp.get("prod_id"));
                    bundle.putString("name", holder.resultp.get("name"));
                    bundle.putString("store_id", holder.resultp.get("store_id"));
                    bundle.putString("storename", holder.resultp.get("store_name"));
                    bundle.putString("prod_name", holder.resultp.get("prod_name"));
                    bundle.putString("prod_price", holder.resultp.get("prod_price"));
                    bundle.putString("stock_status", holder.resultp.get("stock_status"));
                    bundle.putString("description", holder.resultp.get("description"));
                    bundle.putString("prod_sku", holder.resultp.get("prod_sku"));
                    bundle.putString("prod_imageurl", holder.resultp.get("prod_imageurl"));
                    bundle.putInt("prod_temp_stock", tempProdAvailable);

                    if (holder.resultp.get("store_name") != null) {
                        bundle.putString("storename", holder.resultp.get("store_name"));
                    } else {
                        bundle.putString("storename", storename = ProductModel.getStoreName());
                    }

                    try {
                        productTotalQuantity = 0;
                        if (holder.resultp.get("prod_quantity") != null) {
                            JSONArray quantityArray = new JSONArray(holder.resultp.get("prod_quantity"));
                            if (quantityArray != null) {
                                for (int i = 0; i < quantityArray.length(); i++) {
                                    JSONObject quantityObj = quantityArray.getJSONObject(i);
                                    int quantity = quantityObj.getInt("quantity");
                                    productTotalQuantity += quantity;
                                    prodStockQuantity = productTotalQuantity;
                                    bundle.putInt("prod_stockquantity", productTotalQuantity);
                                    Log.i("prod_stockquantityprev:", String.valueOf(productTotalQuantity));
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ProductModel.setName(holder.resultp.get("prod_name"));
                    prodPreview.setArguments(bundle);
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // Get the position
                    holder.resultp = data.get(position);
//                    Log.d("resultp:", holder.resultp.toString());
//                  loadFragment(new ProductListFragment(), true);
                }
            });
            holder.gotoaddtocartBT.setTag(position);
            return view;
        }

        public void showAddToCartDialog() {
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_addtocart);
            dialog.setCancelable(true);
            itemCounter = 1;
            TextView itemTitleToAdd = (TextView) dialog.findViewById(R.id.itemTitleToAdd);
            final TextView itemCountTxt = (TextView) dialog.findViewById(R.id.itemCountTxt);
            final TextView itemquantityprice = (TextView) dialog.findViewById(R.id.itemquantityprice);
            LinearLayout incrementItemBt = (LinearLayout) dialog.findViewById(R.id.incrementItemBt);
            LinearLayout decrementItemBT = (LinearLayout) dialog.findViewById(R.id.decrementItemBt);
            addtocartDialogBT = (Button) dialog.findViewById(R.id.addtocartDialogBT);
            cancelAddtoBT = (Button) dialog.findViewById(R.id.cancelAddtoBT);
            if (checkItem < 1) {
                tempProdStock = String.valueOf(prodStockQuantity);
//                itemTitleToAdd.setText(tempProdStock);
                if (prodStockQuantity == 0) {
                    addtocartDialogBT.setEnabled(false);
                }
            } else {
                Cursor tempAvailableProd = AletDataSource.getTempProductAvailable(productid);
                if (tempAvailableProd != null) {
                    try {
                        int temporaryProdStock = Integer.parseInt(tempAvailableProd.getString(tempAvailableProd.getColumnIndex(tempAvailableProd.getColumnName(0))));
//                        Log.i("temporaryProdStock:", String.valueOf(temporaryProdStock));
                        tempProdStock = String.valueOf(temporaryProdStock);
//                        itemTitleToAdd.setText(tempProdStock);
                        if (temporaryProdStock <= 0) {
                            addtocartDialogBT.setEnabled(false);
                        } else if (temporaryProdStock == 0) {
                            CustomUtils.toastMessage(getActivity(), "Sorry, you already added all " + prod_title + " to cart");
                            addtocartDialogBT.setEnabled(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            itemTitleToAdd.setText(prod_title);
            itemquantityprice.setText("₱ " + CustomUtils.decimalFormat(getActivity(), prod_price));
            incrementItemBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /**
                     * if the product is not yet added in the cart
                     * add the default temporaryProdAvailable value to cart to db
                     * temporaryProdAvailable = originalProductstock - order quantity
                     */
                    if (checkItem < 1) {
                        /**
                         * Product not exist in cart
                         */
                        if (itemCounter < prodStockQuantity) {
                            itemCounter++;
                            priceTotal = (prod_price * itemCounter) - 1;
                            priceTotal++;
                            itemCountTxt.setText(String.valueOf(itemCounter));
                            itemquantityprice.setText("₱ " + CustomUtils.decimalFormat(getActivity(), priceTotal));

                            tempProdAvailable = prodStockQuantity - itemCounter;
//                            Log.d("tempProdAvailable: ", String.valueOf(tempProdAvailable));
//                            Log.d("priceCounter: ", String.valueOf(priceTotal));
//                            Log.d("prodStockQuantity: ", String.valueOf(prodStockQuantity));

                        } else if (itemCounter == prodStockQuantity) {
                            CustomUtils.toastMessage(getActivity(), "Sorry, we only have " + prodStockQuantity + " " + prod_title + " in stock");
                        }

                        if (prodStockQuantity == 0) {
                            CustomUtils.toastMessage(getActivity(), prod_title + " is not available");
                            addtocartDialogBT.setEnabled(false);
                        }
                        /**
                         * else if product is already listed in the database
                         * search for the id of the product and get the value of tempProdAvailable
                         * in the database to limit the item order counter
                         */
                    } else {
                        /**
                         * Product already Exist in cart
                         */
                        if (tempAvailableProd != null) {
                            try {
                                /**
                                 * SUM up the value of temp_available_prod from local DB
                                 *  -- to get the latest product stock availble
                                 *  available product stock = temporary_prod_stock availabe - order quantity
                                 */
                                int temporaryProdStock = Integer.parseInt(tempAvailableProd.getString(tempAvailableProd.getColumnIndex(tempAvailableProd.getColumnName(0))));
//                                Log.i("temporaryProdStock:", String.valueOf(temporaryProdStock));
                                if (itemCounter < temporaryProdStock) {

                                    itemCounter++;
                                    priceTotal = (prod_price * itemCounter) - 1;
                                    priceTotal++;

                                    itemCountTxt.setText(String.valueOf(itemCounter));
                                    itemquantityprice.setText("₱ " + CustomUtils.decimalFormat(getActivity(), priceTotal));

                                    tempProdAvailable = temporaryProdStock - itemCounter;
//                                    Log.d("tempProdStockExist: ", String.valueOf(temporaryProdStock));
//                                    Log.d("tempProdAvailExist: ", String.valueOf(tempProdAvailable));
//                                    Log.d("itemCounterExist: ", String.valueOf(itemCounter));
//                                    Log.d("priceCounter: ", String.valueOf(priceTotal));

                                } else if (itemCounter <= temporaryProdStock) {
                                    CustomUtils.toastMessage(getActivity(), "Sorry, we only have " + temporaryProdStock + " " + prod_title + " in stock");
                                } else if (temporaryProdStock == 0) {
                                    CustomUtils.toastMessage(getActivity(), "Sorry, you already added all " + prod_title + " to cart");
                                    addtocartDialogBT.setEnabled(false);
                                }
                                if (prodStockQuantity == 0) {
                                    CustomUtils.toastMessage(getActivity(), prod_title + " is not available");
                                    addtocartDialogBT.setEnabled(false);
                                }
                                cursor.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });

            decrementItemBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemCounter > 1) {
                        itemCounter--;
                        priceTotal = (prod_price * itemCounter) + 1;
                        priceTotal--;

                        itemCountTxt.setText(String.valueOf(itemCounter));
                        itemquantityprice.setText("₱ " + CustomUtils.decimalFormat(getActivity(), priceTotal));
//                        Log.i("Item priceTotal", String.valueOf(priceTotal));
                    }
                }
            });

            addtocartDialogBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stockQuantity = prodStockQuantity;


                    if (checkItem < 1) {
                        //product not exist checkItem is less than 1
                        int temporaryProdStock = 0;
                        if (itemCounter == 1) {
                            tempAvailableProd = AletDataSource.getTempProductAvailable(productid);

                            if (tempAvailableProd != null) {
                                try {
                                    temporaryProdStock = Integer.parseInt(tempAvailableProd.getString(tempAvailableProd.getColumnIndex(tempAvailableProd.getColumnName(0))));
                                    tempProdAvailable = temporaryProdStock - itemCounter;
                                    priceTotal = (prod_price * 1);
                                    rowCount = itemCounter;
                                    aletDBSource.addcartDB(productid, productid, productid, storeid, storename, prod_title, itemCounter, String.valueOf(prod_price), String.valueOf(priceTotal), prodDesc, prodSKU, prodIMG, stock_stat, UserModel.getEmail(), stockQuantity, tempProdAvailable);
                                    ProductListFragment.updateCartCount();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    rowCount = itemCounter;
                                    tempProdAvailable = prodStockQuantity - itemCounter;
                                    priceTotal = (prod_price * 1);
//                                    Log.i("priceTotalAdd", String.valueOf(priceTotal));
                                    aletDBSource.addcartDB(productid, productid, productid, storeid, storename, prod_title, itemCounter, String.valueOf(prod_price), String.valueOf(priceTotal), prodDesc, prodSKU, prodIMG, stock_stat, UserModel.getEmail(), stockQuantity, tempProdAvailable);
                                    ProductListFragment.updateCartCount();
                                }
                            } else {
                                rowCount = itemCounter;
                                priceTotal = (prod_price * 1);
//                                Log.i("tempProdAvailNull", String.valueOf(tempProdAvailable));
                                aletDBSource.addcartDB(productid, productid, productid, storeid, storename, prod_title, itemCounter, String.valueOf(prod_price), String.valueOf(priceTotal), prodDesc, prodSKU, prodIMG, stock_stat, UserModel.getEmail(), stockQuantity, tempProdAvailable);
                                ProductListFragment.updateCartCount();
                            }

                        } else {
                            //multiple order
                            rowCount = itemCounter;
                            aletDBSource.addcartDB(productid, productid, productid, storeid, storename, prod_title, itemCounter, String.valueOf(prod_price), String.valueOf(priceTotal), prodDesc, prodSKU, prodIMG, stock_stat, UserModel.getEmail(), stockQuantity, tempProdAvailable);
                            ProductListFragment.updateCartCount();
                        }
                    } else {
                        //item already exist
                        tempAvailableProd = AletDataSource.getTempProductAvailable(productid);
                        int temporaryProdStock = 0;
                        if (itemCounter == 1) {
                            try {
                                //product already added in the cart
                                temporaryProdStock = Integer.parseInt(tempAvailableProd.getString(tempAvailableProd.getColumnIndex(tempAvailableProd.getColumnName(0))));
                                tempProdAvailable = temporaryProdStock - itemCounter;
                                priceTotal = (prod_price * 1);
                                aletDBSource.addcartDB(productid, productid, productid, storeid, storename, prod_title, itemCounter, String.valueOf(prod_price), String.valueOf(priceTotal), prodDesc, prodSKU, prodIMG, stock_stat, UserModel.getEmail(), stockQuantity, tempProdAvailable);
                                ProductListFragment.updateCartCount();
                            } catch (Exception e) {

                            }
                        } else {
                            //itemCounter is greater than 1
//                            temporaryProdStock = Integer.parseInt(tempAvailableProd.getString(tempAvailableProd.getColumnIndex(tempAvailableProd.getColumnName(0))));
//                            tempProdAvailable = temporaryProdStock - itemCounter;
                            aletDBSource.addcartDB(productid, productid, productid, storeid, storename, prod_title, itemCounter, String.valueOf(prod_price), String.valueOf(priceTotal), prodDesc, prodSKU, prodIMG, stock_stat, UserModel.getEmail(), stockQuantity, tempProdAvailable);
                            ProductListFragment.updateCartCount();
                        }
                    }
                    dialog.dismiss();
                }
            });
            cancelAddtoBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemCounter = 1;
                    prodStockQuantity = prodStockQuantity;
                    dialog.dismiss();
                    dialog.cancel();
                }
            });

            dialog.show();

        }


        private Response.ErrorListener createRequestErrorListener() {
            return new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null) {
                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "UTF-8");
                                jsonObject = new JSONObject(responseBody);
                                Log.i("JSONResponse", jsonObject.toString());
                                JSONArray errorResult = jsonObject.getJSONArray("error");

                                String errorCode = jsonObject.getString("error_code");
                                String errorDesc = jsonObject.getString("error_description");
                                //get errror by index
                                for (int i = 0; i < errorResult.length(); i++) {
                                    JSONObject errorObj = errorResult.getJSONObject(i);
                                    Iterator keys = errorObj.keys();

                                    while (keys.hasNext()) {
                                        // LOOP TO GET DYNAMIC KEY
                                        String currentDynamicKey = (String) keys.next();
                                        Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                        // GET THE VALUE OF DYNAMIC KEY
                                        String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                        Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                        Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                        Toast.makeText(context.getApplicationContext(), currentDynamicValue,
                                                Toast.LENGTH_LONG).show();

                                    }
                                    Log.i("Error-JSONResponse", "" + errorResult.toString());
                                }

                                Log.e("jsonObject", errorResult + "");
                            } else {
                                Toast.makeText(context.getApplicationContext(), "Please check your internet connection",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("errorJSON", "" + e);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            Log.e("errorEncoding", "" + e);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    CustomUtils.hideDialog();
                }
            };
        }

        private Response.Listener<JSONObject> createRequestSuccessListener() {
            return new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.i("Success-order", response.toString());

                    if (response != null) {

                        try {
                            jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                            Constants.status = response.getBoolean("status");

                            Log.d("JSON-ORDER:", jsonObject.toString());
                            if (Constants.status.toString().contentEquals("true")) {
                                dialog.dismiss();
                                CustomUtils.hideDialog();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                    else {
//                        Toast.makeText(context.getApplicationContext(), "Successfully Updated",
//                                Toast.LENGTH_LONG).show();
//                        CustomUtils.hideDialog();
//                    }
                    }
                }
            };
        }

    }


}