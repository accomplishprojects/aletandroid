package com.ph.jumpdigital.alet.registration;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by luigigo on 11/3/15.
 */
public class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
    private ArrayList<String> resultList;
    private ArrayList<HashMap<String, String>> resultList1;

    public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public int getCount() {
        return resultList1.size();
    }

    @Override
    public String getItem(int index) {
        return resultList1.get(index).get("desc");
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    //resultList = RegisterUser.autocomplete(constraint.toString());
                    resultList1 = RegisterUser.autocomplete(constraint.toString());

                    Log.i("RESULTLIST", resultList1.toString());
                    // Assign the data to the FilterResults
                    filterResults.values = resultList1;
                    filterResults.count = resultList1.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }
}