package com.ph.jumpdigital.alet.Fragment.MyAccount;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.model.AlcoholTypeModel;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vidalbenjoe on 07/10/2015.
 */
public class AlcoholTypesAdapter extends BaseAdapter {

    public JSONObject jsonObject;
    ArrayList<AlcoholTypeModel> _arraylist;
    Context context;
    LayoutInflater inflater;

    public AlcoholTypesAdapter(Context context,
                               ArrayList<AlcoholTypeModel> arraylist) {
        this.context = context;
        this._arraylist = new ArrayList<AlcoholTypeModel>();
        this._arraylist.addAll(arraylist);
    }


    @Override
    public int getCount() {
        return _arraylist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_alcoholtypes, viewGroup, false);

            holder.checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    if (cb.isChecked())
                        cb.setTextColor(context.getResources().getColor(R.color.theme_color));
                    else
                        cb.setTextColor(Color.parseColor("#757575"));

                    AlcoholTypeModel alcoholTypeModel = (AlcoholTypeModel) cb.getTag();
                    alcoholTypeModel.setSelected(cb.isChecked());
                }
            });

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        AlcoholTypeModel alcoholTypeModel = _arraylist.get(position);
        holder.checkBox.setText(alcoholTypeModel.getName());
        holder.checkBox.setChecked(alcoholTypeModel.isSelected());
        if (_arraylist.get(position).isSelected())
            holder.checkBox.setTextColor(context.getResources().getColor(R.color.theme_color));
        else
            holder.checkBox.setTextColor(Color.parseColor("#757575"));
        holder.checkBox.setTag(alcoholTypeModel);
        Log.i("_arraylist", _arraylist.toString());

        return view;
    }


    public class ViewHolder {
        CheckBox checkBox;

    }


}

