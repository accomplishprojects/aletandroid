package com.ph.jumpdigital.alet.Database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by vidalbenjoe on 04/11/2015.
 */
public class AletDBHelper extends SQLiteOpenHelper {

    public SQLiteDatabase database;
    private Context context;
    private static final String DATABASE_NAME = "alet_database";
    private static final int DATABASE_VERSION = 25;
    public static final String TABLE_ORDER_PRODUCT = "order_product_tbl";

    private static String DATABASE_PATH = "";

    public static final String COLUMN_ENTRY_ID = "_id";
    public static final String COLUMN_ORDER_PROD_ID = "order_product_id";//id
    public static final String COLUMN_PROD_ID = "product_id";
    public static final String COLUMN_STORE_ID = "store_id";
    public static final String COLUMN_PROD_STORE_NAME = "storename";
    public static final String COLUMN_PROD_NAME = "orderprodname";
    public static final String COLUMN_PROD_QUANTITY = "quantity";
    public static final String COLUMN_PROD_PRICE = "price";
    public static final String COLUMN_PROD_TOTAL = "item_total";
    public static final String COLUMN_PROD_DESC = "description";
    public static final String COLUMN_PROD_SKU = "sku";

    public static final String COLUMN_PROD_IMAGE = "productimage";
    public static final String COLUMN_PROD_STOCK_STATUS = "stock_status";

    public static final String COLUMN_USER_EMAIL = "user_email";
    public static final String COLUMN_ORDER_ID = "order_id";

    public static final String PROD_STOCK_AVAIL = "prod_stock_available";

    public static final String PROD_TEMP_AVAIL_STOCK = "temp_available_prod";


    private static final String TEXT_TYPE = "TEXT";
    private static final String INTEGER_TYPE = "INTEGER";

    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + TABLE_ORDER_PRODUCT + " (" +
                    COLUMN_ENTRY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    COLUMN_ORDER_PROD_ID + INTEGER_TYPE +
                    COLUMN_PROD_ID + INTEGER_TYPE +
                    COLUMN_STORE_ID + INTEGER_TYPE +
                    COLUMN_PROD_STORE_NAME + TEXT_TYPE +
                    COLUMN_PROD_NAME + TEXT_TYPE +
                    COLUMN_PROD_QUANTITY + INTEGER_TYPE +
                    COLUMN_PROD_PRICE + TEXT_TYPE +
                    COLUMN_PROD_TOTAL + INTEGER_TYPE +
                    COLUMN_PROD_DESC + TEXT_TYPE +
                    COLUMN_PROD_SKU + TEXT_TYPE +
                    COLUMN_PROD_IMAGE + TEXT_TYPE +
                    COLUMN_PROD_STOCK_STATUS + TEXT_TYPE +
                    COLUMN_USER_EMAIL + TEXT_TYPE +
                    COLUMN_ORDER_ID + INTEGER_TYPE +
                    PROD_STOCK_AVAIL + INTEGER_TYPE +
                    PROD_TEMP_AVAIL_STOCK + INTEGER_TYPE +
                    " )";

    public static String query = SQL_CREATE_TABLE;

    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_ORDER_PRODUCT;

    public AletDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        if (android.os.Build.VERSION.SDK_INT >= 4.2) {
            DATABASE_PATH = context.getApplicationInfo().dataDir + "/databases/";
        } else {
            DATABASE_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            db.execSQL(SQL_DELETE_TABLE);
            db.execSQL(SQL_CREATE_TABLE);
//            db.beginTransaction();
//            db.getVersion();
        }
    }

    //This piece of code will create a database if it’s not yet created
    public void createDataBase() {
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), "Copying error");
                throw new Error("Error copying database!");
            }
        } else {
            Log.i(this.getClass().toString(), "Database already exists");
        }
    }

    //    Performing a database existence check
    private boolean checkDataBase() {
        SQLiteDatabase checkDb = null;
        try {
            String path = DATABASE_PATH + DATABASE_NAME;
            checkDb = SQLiteDatabase.openDatabase(path, null,
                    SQLiteDatabase.OPEN_READONLY);
        } catch (SQLException e) {
            Log.e(this.getClass().toString(), "Error while checking db");
        }
        //Android doesn’t like resource leaks, everything should
        // be closed
        if (checkDb != null) {
            checkDb.close();
        }
        return checkDb != null;
    }

    //    Method for copying the database
    private void copyDataBase() throws IOException {
        //Open a stream for reading from our ready-made database
        //The stream source is located in the assets
        InputStream externalDbStream = context.getAssets().open(DATABASE_NAME);

        //Path to the created empty database on your Android device
        String outFileName = DATABASE_PATH + DATABASE_NAME;

        //Now create a stream for writing the database byte by byte
        OutputStream localDbStream = new FileOutputStream(outFileName);

        //Copying the database
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = externalDbStream.read(buffer)) > 0) {
            localDbStream.write(buffer, 0, bytesRead);
        }
        //Don’t forget to close the streams
        localDbStream.close();
        externalDbStream.close();
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        String path = DATABASE_PATH + DATABASE_NAME;
        if (database == null) {
            createDataBase();
            database = SQLiteDatabase.openDatabase(path, null,
                    SQLiteDatabase.OPEN_READWRITE);
        }
        return database;
    }

    @Override
    public synchronized void close() {
        if (database != null) {
            database.close();
        }
        super.close();
    }

}
