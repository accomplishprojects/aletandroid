package com.ph.jumpdigital.alet.Fragment.OrderPlacement;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ph.jumpdigital.alet.Fragment.Inventory.SearchProductFragment;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;

/**
 * Created by luigigo on 11/21/15.
 */
public class OrderConfirmed extends Fragment implements View.OnClickListener {

    Button btnTrackOrder;
    Toolbar toolbar;
    TextView txtOrderId;
    public static String strOrderID;
    String TAG = "OrderConfirmed";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.order_confirmed, container, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Order Confirmed");
        btnTrackOrder = (Button) mainView.findViewById(R.id.btnTrackOrder);
        btnTrackOrder.setOnClickListener(this);
        txtOrderId = (TextView) mainView.findViewById(R.id.txtOrderId);
        txtOrderId.setText("Order #: " + strOrderID);
        return mainView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTrackOrder:
                toolbar.setTitle("Brands");
                SearchProductFragment.browseInvPos = 0;
                CustomUtils.loadFragment(new SearchProductFragment(), true, getActivity());
                break;
        }
    }
}
