package com.ph.jumpdigital.alet.Utilities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalOAuthScopes;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.ShippingAddress;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vidalbenjoe on 9/8/15.
 */
public class PaypalUtils {
    public static String paypalEnvironment;
    public static String paypalSDKVersion;

    public static String paypalPlatform;
    public static String paypalProdName;

    public static String paypalRespCode;
    public static String paypalRespType;
    public static String paypalMetaID;


    public static PayPalConfiguration config = new PayPalConfiguration()
            .environment(Constants.CONFIG_ENVIRONMENT)
            .clientId(Constants.CONFIG_CLIENT_ID)
            .rememberUser(true)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Alét")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    public static void onBuyPressed(View pressed, Activity activity) {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_AUTHORIZE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

        addAppProvidedShippingAddress(thingToBuy);

        enableShippingAddressRetrieval(thingToBuy, true);

        Intent intent = new Intent(activity, PayPalFuturePaymentActivity.class);
        String metadataId = PayPalConfiguration.getClientMetadataId(activity);
        Log.i("METADATAID:", metadataId);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //disable card scanning
        config.acceptCreditCards(false);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        intent.putExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION, thingToBuy);

        activity.startActivityForResult(intent, Constants.REQUEST_CODE_FUTURE_PAYMENT);

        getOauthScopes();
    }

    //
    public static PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal("1.75"), "USD", "Kamote",
                paymentIntent);
    }

    //    public static PayPalFuturePaymentActivity getThingsTo(String paymentIntent){
//        return new PayPalFuturePaymentActivity(new BigDecimal("1.75"), "USD", "Kamote",
//                paymentIntent);
//    }
    ///Multiple Items
    public static PayPalPayment getStuffToBuy(String paymentIntent) {
        //--- include an item list, payment amount details
        PayPalItem[] items =
                {
                        new PayPalItem("sample item #1", 2, new BigDecimal("87.50"), "USD",
                                "sku-12345678"),
                        new PayPalItem("free sample item #2", 1, new BigDecimal("0.00"),
                                "USD", "sku-zero-price"),
                        new PayPalItem("sample item #3 with a longer name", 6, new BigDecimal("37.99"),
                                "USD", "sku-33333")
                };

        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal("7.21");
        BigDecimal tax = new BigDecimal("4.67");
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment = new PayPalPayment(amount, "USD", "sample item", paymentIntent);
        payment.items(items).paymentDetails(paymentDetails);

        //--- set other optional fields like invoice_number, custom field, and soft_descriptor
        payment.custom("This is text that will be associated with the payment that the app can use.");

        return payment;
    }

    /*
    * Add app-provided shipping address to payment
    */
    public static void addAppProvidedShippingAddress(PayPalPayment paypalPayment) {
        ShippingAddress shippingAddress =
                new ShippingAddress().recipientName("Jump Digital").line1("Philippine Stock Exchange Tower")
                        .city("Pasig City").state("MNL").postalCode("78729").countryCode("PH");
        paypalPayment.providedShippingAddress(shippingAddress);
    }

    /*
     * Enable retrieval of shipping addresses from buyer's PayPal account
     */
    private static void enableShippingAddressRetrieval(PayPalPayment paypalPayment, boolean enable) {
        paypalPayment.enablePayPalShippingAddressesRetrieval(enable);
    }

    public static PayPalOAuthScopes getOauthScopes() {
        /* create the set of required scopes
         * Note: see https://developer.paypal.com/docs/integration/direct/identity/attributes/ for mapping between the
         * attributes you select for this app in the PayPal developer portal and the scopes required here.
         */
        Set<String> scopes = new HashSet<String>(
                Arrays.asList(PayPalOAuthScopes.PAYPAL_SCOPE_EMAIL, PayPalOAuthScopes.PAYPAL_SCOPE_ADDRESS));
        return new PayPalOAuthScopes(scopes);
    }

    public static void sendAuthorizationToServer(PayPalAuthorization authorization) {

        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }

    public static String getPaypalEnvironment() {
        return paypalEnvironment;
    }

    public static void setPaypalEnvironment(String paypalEnvironment) {
        PaypalUtils.paypalEnvironment = paypalEnvironment;
    }

    public static String getPaypalSDKVersion() {
        return paypalSDKVersion;
    }

    public static void setPaypalSDKVersion(String paypalSDKVersion) {
        PaypalUtils.paypalSDKVersion = paypalSDKVersion;
    }

    public static String getPaypalPlatform() {
        return paypalPlatform;
    }

    public static void setPaypalPlatform(String paypalPlatform) {
        PaypalUtils.paypalPlatform = paypalPlatform;
    }

    public static String getPaypalProdName() {
        return paypalProdName;
    }

    public static void setPaypalProdName(String paypalProdName) {
        PaypalUtils.paypalProdName = paypalProdName;
    }

    public static String getPaypalRespCode() {
        return paypalRespCode;
    }

    public static void setPaypalRespCode(String paypalRespCode) {
        PaypalUtils.paypalRespCode = paypalRespCode;
    }

    public static String getPaypalRespType() {
        return paypalRespType;
    }

    public static void setPaypalRespType(String paypalRespType) {
        PaypalUtils.paypalRespType = paypalRespType;
    }

    public static String getPaypalMetaID() {
        return paypalMetaID;
    }

    public static void setPaypalMetaID(String paypalMetaID) {
        PaypalUtils.paypalMetaID = paypalMetaID;
    }

}
