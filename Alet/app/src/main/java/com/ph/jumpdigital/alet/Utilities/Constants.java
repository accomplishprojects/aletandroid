package com.ph.jumpdigital.alet.Utilities;

import com.google.android.gms.maps.model.LatLng;
import com.paypal.android.sdk.payments.PayPalConfiguration;

/**
 * Created by vidalbenjoe on 9/8/15.
 */
public class Constants {
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     * <p/>
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     * <p/>
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    //All the constant value should put in here eq: URL, Tags , id
    public static final String TAG = "Alet";
    public static String volleyTAG = "";
    public static String checkoutType = "";
    public static Boolean status = null;
    public static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    // note that these credentials will differ between live & sandbox environments.
    public static final String CONFIG_CLIENT_ID = "AbK5mAx9cnmBf1cJKknzbpZVQwf3v7kwDylbUEUmEg6gUiH6BJqNA3wbShCjbqSNwHsuAjmgGGUdQGBo";
    public static final String CONFIG_SECRET_KEY = "EFmOe9biibHjg1OS6x1izh6bVRsZDDeEsQg8jsVD-hsPZNNuvkxcSVo9QcTPeo11kxVIqpnwx611Y_SE";
    public static final int REQUEST_CODE_PAYMENT = 1;
    public static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    public static final int REQUEST_CODE_PROFILE_SHARING = 3;

    //Alet API URLs
    public static final String baseURL = "http://alet.jumpdigital.ph";
    public static final String apiURL = baseURL + "/api/";
    public static final String showSpecificUser = "/user/";
    public static final String userRegister = "/users/sign_up";
    public static final String userSignin = "/users/sign_in";
    public static final String userSignout = "/users/sign_out";
    public static final String updateUserProfile = "/users/update";
    public static final String changePassword = "/users/change_password";
    public static final String addAddress = "/users/address";
    public static final String updateAddress = "/users/address/";
    public static final String displayAddress = "/users/addresses";
    public static final String userDefaultAddress = "/default_address";
    public static final String subscribeNewsLetter = "/users/news_letter";
    public static final String checkoutWithAddress = "/order_with_address";
    public static final String checkoutNewAddress = "/order_new_address";
    public static final String forgotPassword = "/users/reset_password";
    public static final String getAllProductBrand = "/brands";
    public static final String searchByBrandName = "/brands/";
    public static final String getAllProductTypes = "/types";
    public static final String searchByProdTypes = "/types/";
    public static final String getAllStores = "/stores";
    public static final String getStoreName = "/stores/";
    public static final String viewCart = "/orders";
    public static final String rating = "/rating";
    public static final String order_product = "/order_product/";
    public static final String deleteCartItems = "/order_product_delete";
    public static final String deleteAllCartItems = "/delete_all_cart";
    public static final String order_history = "/history/";
    public static final String preferred_alcohol_types = "/users/alcohol_type";
    public static final String recommended_alcohol_types = "/users/alcohol_types";
    public static final String payment_method = "/payment_method";
    public static final String user_payment_paypal = "/user_payment";
    public static final String validate_promo_code = "/promo";
    public static final String setDefaultAddress = "/users/default_address";

    public static final String issueType = "issue_type";
    public static final String reportProblem = "issue";

    public static final String check_paypal_login = "checkpaypal";
    public static final String remove_paypal_account = "paypal_destroy";

    public static final String addQuantity = "add_quantity";
    public static final String minusQuantity = "minus_quantity";

    //GCM
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String GCM_SENDER_ID = "264552332036";

    // Google Places API URLs
    public static String GOOGLE_PLACES_BASE_URL = "https://maps.googleapis.com/maps/api";
    public static String GOOGLE_PLACES_AUTOCOMPLETE = "/place/autocomplete/json";
    public static String GOOGLE_PLACES_API_KEY = "?key=AIzaSyAIlMo49ivaS3lVR8fgaTGv9Sv9vVckLcU";
    public static String GOOGLE_GEOCODING_API_KEY = "&key=AIzaSyCwXQ12UCpxVOedcgMfwSXmtiJUcCpHZOI";
    public static String GOOGLE_GEOCODING = "/geocode/json?address=";

    public static final LatLng METRO_MANILA_LOC = new LatLng(14.597413, 121.052477);

    //HTTP METHOD
    public static final int getMethod = com.android.volley.Request.Method.GET;
    public static final int postMethod = com.android.volley.Request.Method.POST;
    public static final int putMethod = com.android.volley.Request.Method.PUT;
    public static final int delMethod = com.android.volley.Request.Method.DELETE;

    // Request code to use when launching the resolution activity
    public static int PLACE_PICKER_REQUEST = 1;
    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    public static final int REQUEST_RESOLVE_ERROR = 1001;
    public static final String STATE_RESOLVING_ERROR = "resolving_error";
    public static final int GOOGLE_API_CLIENT_ID = 0;
    public static boolean mResolvingError = false;
    public static final String DIALOG_ERROR = "dialog_error";
}