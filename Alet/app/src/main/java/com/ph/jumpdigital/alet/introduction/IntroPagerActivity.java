package com.ph.jumpdigital.alet.introduction;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.StartScreenActivity;
import com.ph.jumpdigital.alet.Utilities.SharedPref;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.RegistrationModel;
import com.ph.jumpdigital.alet.model.UserModel;
import com.ph.jumpdigital.alet.registration.RegisterUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;


/**
 * Created by vidalbenjoe on 9/2/15.
 */
public class IntroPagerActivity extends AppCompatActivity implements View.OnClickListener {

    static final int NUM_PAGES = 5;
    public static String file = "Files";
    SharedPreferences sp;
    ViewPager pager;
    PagerAdapter pagerAdapter;
    LinearLayout circles;
    RelativeLayout btnRegAlet;
    RelativeLayout btnRegFacebookRel;
    UserModel userModel = new UserModel();
    RegistrationModel registrationModel;
    /*
        This is nasty but as the transparency of the fragments increases when swiping the underlying
        Activity becomes visible, so we changed the pager opacity on the last slide in
        setOnPageChangeListener below
     */
    boolean isOpaque = true;
    private Intent i;
    private CallbackManager callbackManager;
    private String fbEmail, fbFirstName, fbLastName, fbGender, facebookId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Window window = getWindow();
        //window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.intro_activity_layout);
        getSupportActionBar().hide();
        callbackManager = new CallbackManager.Factory().create();

        //Move to next View
//        pager.setCurrentItem(pager.getCurrentItem() + 1, true);

//        done.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                endTutorial();
//            }
//        });

        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.setPageTransformer(true, new CrossfadePageTransformer());
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //See note above for why this is needed
                if (position == NUM_PAGES - 2 && positionOffset > 0) {
                    if (isOpaque) {
                        pager.setBackgroundColor(Color.TRANSPARENT);
                        isOpaque = false;
                    }
                } else {
                    if (!isOpaque) {
//                        pager.setBackgroundColor(getResources().getColor(R.color.tutorial_background_opaque));
                        isOpaque = true;
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {
                Log.i("--position", "" + position);
                setIndicator(position);
                if (position == NUM_PAGES - 1) {
                    // referencing widgets inside new account layout
                    if (position == 4) {
                        endTutorial();
                    }
                } else if (position < NUM_PAGES - 1) {
                } else if (position == NUM_PAGES) {
                    //endTutorial();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //Unused
            }
        });

        buildCircles();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegAlet:
                RegisterUser.page = 0;
                i = new Intent(this, RegisterUser.class);
                i.putExtra("via", 1);
                clearRegistrationModel();
                startActivity(i);
                break;
        }
    }

    private void clearRegistrationModel() {
        registrationModel = new RegistrationModel();
        RegistrationModel.setFirstname(null);
        RegistrationModel.setLastname(null);
        RegistrationModel.setGender(null);
        RegistrationModel.setMobile(null);

        // clear all inputs in edit texts
        userModel.setEmail(null);
        userModel.setFirstname(null);
        userModel.setLastname(null);
        userModel.setGender(null);
        UserModel.setRegistering(true);
    }

    /*
        The last fragment is transparent to enable the swipe-to-finish behaviour seen on Google's apps
        So our viewpager circle indicator needs to show NUM_PAGES - 1
     */
    private void buildCircles() {
        circles = LinearLayout.class.cast(findViewById(R.id.circles));

        float scale = getResources().getDisplayMetrics().density;
        int padding = 0;

        int screenSize = circles.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                Log.i("SCREEN: ", "Large Screen");
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                Log.i("SCREEN: ", "Normal screen");
                padding = (int) (5 * scale + 0.5);
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                Log.i("SCREEN: ", "Small screen");
                break;
            default:
                padding = (int) (5 * scale + 0.5f);
        }

        for (int i = 0; i < NUM_PAGES; i++) {
            ImageView circle = new ImageView(this);
            circle.setImageResource(R.drawable.slider_other);
            circle.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            circle.setAdjustViewBounds(true);
            circle.setMaxHeight(50);
            circle.setMaxWidth(50);
            circle.setPadding(padding, 0, padding, 0);
            circles.addView(circle);
        }
        setIndicator(0);
    }

    private void setIndicator(int index) {
        if (index < NUM_PAGES) {
            for (int i = 0; i < NUM_PAGES; i++) {
                ImageView circle = (ImageView) circles.getChildAt(i);
                if (i == index) {
                    circle.setImageResource(R.drawable.slider_current);
                } else {
                    circle.setImageResource(R.drawable.slider_other);
                }
            }
        }
    }

    private void endTutorial() {

        btnRegAlet = (RelativeLayout) findViewById(R.id.btnRegAlet);
        btnRegAlet.setOnClickListener(this);

        btnRegFacebookRel = (RelativeLayout) findViewById(R.id.btnRegFBRelative);

        btnRegFacebookRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(IntroPagerActivity.this, Arrays.asList("public_profile,email, user_birthday, user_friends"));

            }
        });

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                CustomUtils.showDialog(IntroPagerActivity.this);
                getFbInfo();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {
                Log.i("FBError : ", e.getMessage().toString());
                Toast.makeText(IntroPagerActivity.this, "Facebook Error", Toast.LENGTH_SHORT).show();
            }
        });
        /*Intent mainDrawerIntent = new Intent(getApplicationContext(), MainDrawerActivity.class);
        startActivity(mainDrawerIntent);
        IntroPagerActivity.this.finish();
        overridePendingTransition(R.fade_in.fade_in, R.fade_in.fade_out);*/
    }

    @Override
    public void onBackPressed() {
        if (pager.getCurrentItem() == 0) {
            super.onBackPressed();
            Intent i = new Intent(this, StartScreenActivity.class);
            startActivity(i);
        } else {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    private void getFbInfo() {

        try {
            GraphRequest request = GraphRequest.newMeRequest(
                    AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                final JSONObject object,
                                GraphResponse response) {
                            //Toast.makeText(IntroPagerActivity.this, "Unable to fetch data from facebook!", Toast.LENGTH_SHORT).show();

                            // Application code
                            if (object == null) {
                                Toast.makeText(IntroPagerActivity.this, "Unable to fetch data from facebook!", Toast.LENGTH_SHORT).show();

                            } else {
                                try {
                                    facebookId = object.getString("id");
                                    fbFirstName = object.getString("first_name");
                                    fbLastName = object.getString("last_name");
                                    fbGender = object.getString("gender");

                                    UserModel.setFacebookId(facebookId);
                                    userModel.setFirstname(fbFirstName);
                                    userModel.setLastname(fbLastName);
                                    userModel.setGender(fbGender);

                                    registrationModel = new RegistrationModel();
                                    RegistrationModel.setFirstname(fbFirstName);
                                    RegistrationModel.setLastname(fbLastName);
                                    RegistrationModel.setGender(fbGender);

                                    UserModel.setRegistering(true);
                                    fbEmail = object.getString("email");
                                    userModel.setEmail(fbEmail);
                                    Log.i("getEmail", "" + UserModel.getEmail());

                                    i = new Intent(IntroPagerActivity.this, RegisterUser.class);
                                    startActivity(i);
                                    LoginManager.getInstance().logOut();
                                    CustomUtils.hideDialog();
                                } catch (Exception e) {
                                    if (e.getMessage().equals("No value for email")) {
                                        Toast.makeText(IntroPagerActivity.this, "Error sa fb", Toast.LENGTH_SHORT).show();

                                        AlertDialog.Builder al = new AlertDialog.Builder(IntroPagerActivity.this);
                                        al.setTitle("Facebook Error");
                                        al.setMessage("Ooppss! It seems that your email is not set as public.\nPlease try to register manually. Thank you!");
                                        al.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                try {
                                                    facebookId = object.getString("id");

                                                    fbFirstName = object.getString("first_name");
                                                    fbLastName = object.getString("last_name");
                                                    fbGender = object.getString("gender");

                                                    UserModel.setFacebookId(facebookId);
                                                    userModel.setFirstname(fbFirstName);
                                                    userModel.setLastname(fbLastName);
                                                    userModel.setGender(fbGender);

                                                    registrationModel = new RegistrationModel();
                                                    RegistrationModel.setFirstname(fbFirstName);
                                                    RegistrationModel.setLastname(fbLastName);
                                                    RegistrationModel.setGender(fbGender);

                                                    UserModel.setRegistering(true);
                                                    SharedPref sharedPref = new SharedPref(IntroPagerActivity.this);
                                                    sharedPref.setFBEmail(fbEmail);
                                                    Intent intent = new Intent(IntroPagerActivity.this, RegisterUser.class);
                                                    intent.putExtra("via", 0);
                                                    startActivity(intent);

                                                    LoginManager.getInstance().logOut();
                                                    CustomUtils.hideDialog();
                                                    dialogInterface.dismiss();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                        AlertDialog dialog = al.create();
                                        dialog.show();

                                        e.printStackTrace();
                                        CustomUtils.hideDialog();
                                    } else {
                                        Toast.makeText(IntroPagerActivity.this, "", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name, last_name, email,gender, birthday, picture.type(large)");
            request.setParameters(parameters);
            request.executeAsync();
            Log.i("FB-Parameters", "" + request);
        } catch (FacebookException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            IntroPanel tp = null;
            switch (position) {
                case 0:
                    tp = IntroPanel.newInstance(R.layout.fragment_intro_first);
                    break;
                case 1:
                    tp = IntroPanel.newInstance(R.layout.fragment_intro_second);
                    break;
                case 2:
                    tp = IntroPanel.newInstance(R.layout.fragment_intro_third);
                    break;
                case 3:
                    tp = IntroPanel.newInstance(R.layout.fragment_intro_fourth);
                    break;
                case 4:
                    tp = IntroPanel.newInstance(R.layout.register_newaccount);
                    break;
            }

            return tp;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public class CrossfadePageTransformer implements ViewPager.PageTransformer {

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void transformPage(View page, float position) {
            int pageWidth = page.getWidth();

            /**
             * Animating Object within the View
             */

//            View backgroundView = page.findViewById(R.id.background);
//            View text = page.findViewById(R.id.introcontent);
//
//            if (text instanceof TextView) {
//                introtextView = (TextView) text;
//                //Do your stuff
//            }
//            Typeface robotothin = Typeface.createFromAsset(getAssets(), "fonts/Roadbrush.ttf");
//            introtextView.setTypeface(robotothin);
//
//            if (position <= 1) {
//                page.setTranslationX(pageWidth * -position);
//            }
//
//            if (position <= -1.0f || position >= 1.0f) {
//            } else if (position == 0.0f) {
//            } else {
//                if (backgroundView != null) {
//                    backgroundView.setAlpha(1.0f - Math.abs(position));
//                }
//
//                //Text both translates in/out and fades in/out
//                if (text != null) {
//                    text.setTranslationX(pageWidth * position);
//                    text.setAlpha(1.0f - Math.abs(position));
//                }
//
//            }
        }
    }
}
