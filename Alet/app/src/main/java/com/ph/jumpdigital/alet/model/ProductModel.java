package com.ph.jumpdigital.alet.model;

import java.io.Serializable;

/**
 * Created by vidalbenjoe on 30/09/2015.
 */
public class ProductModel implements Serializable {

    public static String orderId;
    public String id;
    public String orderEmail;
    public String orderQuantity;
    public String orderProductId;
    public String orderProductName;
    public String orderStoreId;
    public static String prodOrderID;
    public static String name;
    public static String orderList;
    public static String orderListToAddCart;
    public static String orderDetails;

    public static String addressId;
    public static String addressLabel;
    public static String addressStreet;
    public static String addressCity;

    public static String storeName;
    public static String orderPromoCode;

    public static String getAddressLabel() {
        return addressLabel;
    }

    public static void setAddressLabel(String addressLabel) {
        ProductModel.addressLabel = addressLabel;
    }

    public static String getAddressStreet() {
        return addressStreet;
    }

    public static void setAddressStreet(String addressStreet) {
        ProductModel.addressStreet = addressStreet;
    }

    public static String getAddressCity() {
        return addressCity;
    }

    public static void setAddressCity(String addressCity) {
        ProductModel.addressCity = addressCity;
    }

    public static String productTitle;
    public static String productSnippet;
    public static String productPrice;
    public static String orderProduct;
    public static String deliveryAddress, deliveryInfo, paymentMethod, specialInstruction;
    public static int orderCount;


    public static String getStoreName() {
        return storeName;
    }

    public static void setStoreName(String storeName) {
        ProductModel.storeName = storeName;
    }

    public static String getOrderId() {
        return orderId;
    }

    public static void setOrderId(String orderId) {
        ProductModel.orderId = orderId;
    }

    public static String getOrderPromoCode() {
        return orderPromoCode;
    }

    public static void setOrderPromoCode(String orderPromoCode) {
        ProductModel.orderPromoCode = orderPromoCode;
    }


    public static String getProdOrderID() {
        return prodOrderID;
    }

    public static void setProdOrderID(String prodOrderID) {
        ProductModel.prodOrderID = prodOrderID;
    }

    public static int getOrderSubtotal() {
        return orderSubtotal;
    }

    public static void setOrderSubtotal(int orderSubtotal) {
        ProductModel.orderSubtotal = orderSubtotal;
    }

    public static int orderSubtotal;

    public static int getOrderCount() {
        return orderCount;
    }

    public static void setOrderCount(int orderCount) {
        ProductModel.orderCount = orderCount;
    }


    public static String getAddressId() {
        return addressId;
    }

    public static void setAddressId(String addressId) {
        ProductModel.addressId = addressId;
    }

    public static String getDeliveryAddress() {
        return deliveryAddress;
    }

    public static void setDeliveryAddress(String deliveryAddress) {
        ProductModel.deliveryAddress = deliveryAddress;
    }

    public static String getPaymentMethod() {
        return paymentMethod;
    }

    public static void setPaymentMethod(String paymentMethod) {
        ProductModel.paymentMethod = paymentMethod;
    }

    public static String getSpecialInstruction() {
        return specialInstruction;
    }

    public static void setSpecialInstruction(String specialInstruction) {
        ProductModel.specialInstruction = specialInstruction;
    }

    public static String getOrderList() {
        return orderList;
    }

    public static void setOrderList(String orderList) {
        ProductModel.orderList = orderList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        ProductModel.name = name;
    }

    public static String getOrderDetails() {
        return orderDetails;
    }

    public static void setOrderDetails(String orderDetails) {
        ProductModel.orderDetails = orderDetails;
    }


    public String getOrderEmail() {
        return orderEmail;
    }

    public void setOrderEmail(String orderEmail) {
        this.orderEmail = orderEmail;
    }

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getOrderProductId() {
        return orderProductId;
    }

    public void setOrderProductId(String orderProductId) {
        this.orderProductId = orderProductId;
    }

    public String getOrderProductName() {
        return orderProductName;
    }

    public void setOrderProductName(String orderProductName) {
        this.orderProductName = orderProductName;
    }

    public String getOrderStoreId() {
        return orderStoreId;
    }

    public void setOrderStoreId(String orderStoreId) {
        this.orderStoreId = orderStoreId;
    }

    public static String getOrderListToAddCart() {
        return orderListToAddCart;
    }

    public static void setOrderListToAddCart(String orderListToAddCart) {
        ProductModel.orderListToAddCart = orderListToAddCart;
    }


}
