package com.ph.jumpdigital.alet.Fragment.OrderPlacement;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Fragment.ItemListing.CartViewFragment;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.ProductModel;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by luigigo on 12/6/15.
 */
public class OrderSavedAddress extends Fragment implements View.OnClickListener {

    View mainView;
    ListView lvOrderSaveAddress;
    String url;
    JSONObject jsonObject;
    ArrayList<HashMap<String, String>> arraylist;
    UserModel userModel = new UserModel();
    OrderSavedAddressAdapter adapter;
    ProductModel productModel = new ProductModel();
    boolean isAddressSelected = false;
    Button btnOrderSaveAdd;
    Toolbar toolbar;
    public static Integer _position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.order_saved_address, container, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Saved Addresses");

        getAddress();
        CustomUtils.showDialog(getActivity());

        btnOrderSaveAdd = (Button) mainView.findViewById(R.id.btnOrderSavedAddress);
        btnOrderSaveAdd.setOnClickListener(this);
        lvOrderSaveAddress = (ListView) mainView.findViewById(R.id.lvOrderSaveAddress);
        lvOrderSaveAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // For single selection of radiobutton within listview
                adapter.setSelectedIndex(position);
                adapter.notifyDataSetChanged();

                String strAddressId, strStreetAddress, strCity, strZipCode, strLabel;
                strAddressId = adapter._arraylist.get(position).get("id");
                strLabel = adapter._arraylist.get(position).get("label");
                strStreetAddress = adapter._arraylist.get(position).get("street_address");
                strCity = adapter._arraylist.get(position).get("city");
                strZipCode = adapter._arraylist.get(position).get("zip_code");

                ProductModel.setAddressId(strAddressId);
                ProductModel.setDeliveryAddress(strStreetAddress + ", " + strCity + " ");
                isAddressSelected = true;
                Log.i("OSA-Details: ",
                        "LABEL: " + strLabel + "\n"
                                + "STREET ADD: " + strStreetAddress + "\n"
                                + "CITY: " + strCity);
                Log.i("Address ID", ProductModel.getAddressId());
                Log.i("Delivery Address", ProductModel.getDeliveryAddress());

            }
        });

        return mainView;
    }

    public void getAddress() {
        isAddressSelected = false;
        url = Constants.apiURL + Constants.displayAddress;
        Constants.volleyTAG = "getaddress";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");

                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);
                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);

                                    if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                        CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                        gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ActivityCompat.finishAffinity(getActivity());
                                        startActivity(gotoLogin);
                                        getActivity().finish();

                                    } else {
                                        // if (rowCount > 0) {
                                        CustomUtils.hideDialog();
                                        CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                                        //} else {
                                        //    CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        //}
                                    }

                                    Toast.makeText(getActivity(), currentDynamicValue,
                                            Toast.LENGTH_LONG).show();

                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

                            Log.e("jsonObject", errorResult + "");
                        } else {
                            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.i("Success-OrderSaveAdd", response.toString());
                CustomUtils.hideDialog();

                if (response != null) {
                    if (Constants.volleyTAG.equals("getaddress")) {
                        // GETTING ADDRESS
                        arraylist = new ArrayList<HashMap<String, String>>();
                        try {
                            jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));

                            JSONArray user_add = jsonObject.getJSONArray("user_address");
                            Constants.status = response.getBoolean("status");


                            if (user_add.length() == 0) {
                                CustomUtils.toastMessage(getActivity(), "No saved address");
                                CustomUtils.hideDialog();
                            } else {
                                for (int i = 0; i < user_add.length(); i++) {
                                    jsonObject = user_add.getJSONObject(i);

                                    Log.i("Volley-user_add", "" + user_add);

                                    HashMap<String, String> map = new HashMap<String, String>();
                                    map.put("id", jsonObject.getString("id"));
                                    map.put("label", jsonObject.getString("label"));
                                    map.put("street_address", jsonObject.getString("street_address"));
                                    map.put("city", jsonObject.getString("city"));
                                    map.put("zip_code", jsonObject.getString("zip_code"));
                                    map.put("latitude", jsonObject.getString("latitude"));
                                    map.put("longtitude", jsonObject.getString("longtitude"));
                                    map.put("created_at", jsonObject.getString("created_at"));
                                    map.put("default_address", jsonObject.getString("default_address"));
                                    arraylist.add(map);

                                    if (Constants.status.toString().contentEquals("true")) {
                                        if (UserModel.getAuthToken() != null) {

                                            // PASS THE RESULTS INTO LISTVIEWADAPTER.JAVA
                                            adapter = new OrderSavedAddressAdapter(getActivity(), arraylist);

                                            //SET THE ADAPTER TO THE LISTVIEW
                                            lvOrderSaveAddress.setAdapter(adapter);


                                            // CLOSE THE PROGRESS DIALOG
                                            CustomUtils.hideDialog();

                                            UserModel.setUser_id(jsonObject.getString("user_id"));
                                            UserModel.setAddressLabel(jsonObject.getString("label"));
                                            userModel.setStreetAdd(jsonObject.getString("street_address"));
                                            userModel.setCity(jsonObject.getString("city"));
                                            userModel.setZipCode(jsonObject.getString("created_at"));
                                        }

                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnOrderSavedAddress:
                if (isAddressSelected)
                    CustomUtils.loadFragment(new OrderSpecialInstructFragment(), true, getActivity());
                else
                    CustomUtils.toastMessage(getActivity(), "Select address above");
                break;
        }
    }


    public class OrderSavedAddressAdapter extends BaseAdapter {

        public JSONObject jsonObject;
        ArrayList<HashMap<String, String>> _arraylist;
        Context context;
        LayoutInflater inflater;
        int selectedIndex = -1;
        String strDefaultAddress;

        public OrderSavedAddressAdapter(Context context,
                                        ArrayList<HashMap<String, String>> arraylist) {
            this.context = context;
            this._arraylist = new ArrayList<HashMap<String, String>>();
            this._arraylist.addAll(arraylist);
        }


        @Override
        public int getCount() {
            return _arraylist.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            final ViewHolder holder;
            if (view == null) {
                holder = new ViewHolder();
                inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.list_item_order_saved_address, viewGroup, false);

                holder.rbOrderSaveAdd = (RadioButton) view.findViewById(R.id.rbOrderSaveAdd);
                holder.txtLabel = (TextView) view.findViewById(R.id.txtLabel);
                holder.txtStreetAdd = (TextView) view.findViewById(R.id.txtStreetAdd);
                holder.txtCity = (TextView) view.findViewById(R.id.txtCity);
                holder.relDefAddressIndicator = (RelativeLayout) view.findViewById(R.id.relDefAddressIndicator);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            if (selectedIndex == position) {
                holder.rbOrderSaveAdd.setChecked(true);
            } else {
                holder.rbOrderSaveAdd.setChecked(false);
            }
            strDefaultAddress = _arraylist.get(position).get("default_address");
            holder.txtLabel.setText(_arraylist.get(position).get("label"));
            holder.txtStreetAdd.setText(_arraylist.get(position).get("street_address"));
            holder.txtCity.setText(_arraylist.get(position).get("city"));
            if (strDefaultAddress.equals("1")) {
                holder.relDefAddressIndicator.setVisibility(View.VISIBLE);
            } else {
                holder.relDefAddressIndicator.setVisibility(View.INVISIBLE);
            }
            OrderSavedAddress._position = position;
            return view;

        }

        public class ViewHolder {
            RadioButton rbOrderSaveAdd;
            TextView txtLabel, txtStreetAdd, txtCity;
            RelativeLayout relDefAddressIndicator;

        }

        public void setSelectedIndex(int index) {
            selectedIndex = index;
        }

    }
}