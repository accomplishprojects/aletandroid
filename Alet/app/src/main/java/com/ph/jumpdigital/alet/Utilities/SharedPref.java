package com.ph.jumpdigital.alet.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by patrick on 10/12/2015.
 */
public class SharedPref {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private final String PREF_NAME = "com.ph.jumpdigital.alet";

    // (make variables public to access from outside)
    public static final String KEY_GCM_TOKEN = "GCMToken";
    public static final String KEY_AUTH_TOKEN = "AUTHToken";
    public static final String KEY_FB_EMAIL = "FBEmail";
    public String KEY_INITIALIZATION = "Initalize";

    public SharedPref(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void removePref(){
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        pref.edit().clear().commit();
    }

    public void setInitialization(boolean init) {
        editor.putBoolean(KEY_INITIALIZATION, init);
        editor.commit();
    }

    public boolean getInitialization(boolean init) {
        return pref.getBoolean(KEY_INITIALIZATION, false);
    }

    public void setGCMToken(String token) {
//       pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
//       editor = pref.edit();
        editor.putString(KEY_GCM_TOKEN, token);
        editor.commit();
    }

    public String getAUTHToken() {
        return pref.getString(KEY_AUTH_TOKEN, null);
    }

    public void setAUTHToken(String token) {
//       pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
//       editor = pref.edit();
        editor.putString(KEY_AUTH_TOKEN, token);
        editor.commit();
    }

    public String getGCMToken() {
        return pref.getString(KEY_GCM_TOKEN, null);
    }

    public void setFBEmail(String email) {
        editor.putString(KEY_FB_EMAIL, email);
        editor.commit();
    }

    public String getFBEmail() {
        return pref.getString(KEY_FB_EMAIL, "0");
    }


}
