/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ph.jumpdigital.alet.Utilities.GCM;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.ph.jumpdigital.alet.Fragment.RatingActivity;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.Utilities.SharedPref;
import com.ph.jumpdigital.alet.model.NotificationsModel;

public class MyGcmListenerService extends GcmListenerService {

    SharedPref sharedPref;
    Intent intent;
    int num;
    String strMessage, strOrderId, strNotifId, strOrderStatus;
    PendingIntent pendingIntent;
    NotificationManager notificationManager;
    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.i(TAG, String.valueOf(data));
        strMessage = data.getString("message");
        strOrderId = data.getString("order_id");
        strNotifId = data.getString("notification_id");
        strOrderStatus = data.getString("order_status");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + strMessage + " Order ID: " + strOrderId + " Notif ID: " + strNotifId + " Status: " + strOrderStatus);
        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }
        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        sendNotification(strMessage);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {
        String strNotiftype = null;
        sharedPref = new SharedPref(getApplicationContext());
        num = (int) System.currentTimeMillis();

        if (strNotifId != null) {
            if (strNotifId.equals("1"))
                strNotiftype = "Announcement";
            else if (strNotifId.equals("2"))
                strNotiftype = "Promotions";
            else if (strNotifId.equals("3"))
                strNotiftype = "Orders";
            else if (strNotifId.equals("5")) {
                strNotiftype = "Preferences";
            }
        }

        if (sharedPref.getAUTHToken() != null) {
            if (strNotifId != null) {
                if (strOrderStatus != null && strOrderStatus.equals("Completed")) {

                    NotificationsModel notificationsModel = new NotificationsModel();
                    NotificationsModel.setRatingsOrderId(strOrderId);

                    intent = new Intent(this, RatingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    pendingIntent = PendingIntent.getActivity(this, num /* Request code */, intent,/*PendingIntent.FLAG_ONE_SHOT*/0);
                } else {
//                intent = new Intent(this, MainNavigationDrawer.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                pendingIntent = PendingIntent.getActivity(this, num /* Request code */, intent,/*PendingIntent.FLAG_ONE_SHOT*/0);
                }
            }
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(strNotiftype)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message));
//

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(num /* ID of notification */, notificationBuilder.build());
    }
}
