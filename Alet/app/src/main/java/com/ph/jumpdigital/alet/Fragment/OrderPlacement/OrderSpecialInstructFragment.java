package com.ph.jumpdigital.alet.Fragment.OrderPlacement;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Database.AletDBHelper;
import com.ph.jumpdigital.alet.Database.AletDataSource;
import com.ph.jumpdigital.alet.Fragment.ItemListing.CartViewFragment;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.ProductModel;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;

/**
 * Created by luigigo on 10/27/15.
 */
public class OrderSpecialInstructFragment extends Fragment implements View.OnClickListener {
    static String url;
    JSONObject jsonObject, params, jsonObj, jsonParams;
    JSONArray jsonArray, jsonArr;
    Button btnSpecialInstruction;
    EditText edtSpecialInstruction;
    String specialInstruction;
    ProductModel productModel = new ProductModel();
    AletDBHelper dbhelper;
    AletDataSource dbsource;
    Toolbar toolbar;
    String TAG = "SpecialInstruction";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.order_special_instruction, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Special Instructions");

        dbhelper = new AletDBHelper(getActivity());
        dbsource = new AletDataSource(getActivity());/**/
        dbsource.open();

        btnSpecialInstruction = (Button) mainView.findViewById(R.id.btnSpecialInstruction);
        btnSpecialInstruction.setOnClickListener(this);
        edtSpecialInstruction = (EditText) mainView.findViewById(R.id.edtSpecialInstruction);


        Log.i("orderListToAddCart", ProductModel.getOrderListToAddCart());
        // Log.i("addressId", productModel.getAddressId());

        return mainView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSpecialInstruction:
                addToCart();
                break;
        }
    }

    private void addToCart() {
        try {
            JSONArray jsonArr = new JSONArray(ProductModel.getOrderListToAddCart());
            params = new JSONObject();
            if (ProductModel.getOrderPromoCode() != null) {
                params.put("promo_code", ProductModel.getOrderPromoCode());
                // CustomUtils.toastMessage(getActivity(), "Has Promo Code");
            } else {
                params.put("promo_code", "");
                // CustomUtils.toastMessage(getActivity(), "No Promo Code");
            }
            params.put("add_cart", jsonArr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i(TAG, params.toString());
        Constants.volleyTAG = "jsonformat";
        url = Constants.apiURL + Constants.order_product;
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.postMethod, url,
                null, params,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());
    }


    private Response.ErrorListener createRequestErrorListener() {

        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("VolleyError", error.getLocalizedMessage() + error.getMessage());
                try {
                    if (error.networkResponse != null) {
                        CustomUtils.hideDialog();
                        String responseBody = new String(error.networkResponse.data, "UTF-8");
                        jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");

                        String errorCode = jsonObject.getString("error_code");
                        String errorDesc = jsonObject.getString("error_description");

                        Log.i("errorCode:", errorCode);
                        Log.i("errorDesc:", errorDesc);
                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();
                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                // do something here with the value...
                                Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                if (currentDynamicValue.equals("Promo code not valid")) {
                                    Log.i(TAG, "Promo code is invalid in add to cart");
                                } else {
                                    Toast.makeText(getActivity(), currentDynamicValue, Toast.LENGTH_SHORT).show();
                                }

                                if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                    CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                    Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                    gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    ActivityCompat.finishAffinity(getActivity());
                                    startActivity(gotoLogin);
                                    getActivity().finish();

                                } else if (currentDynamicValue.toString().contentEquals("Product not available")) {
                                    CustomUtils.hideDialog();
                                    dbsource.deleteAllCartByEmail(UserModel.getEmail());
                                    CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                                    Toast.makeText(getActivity(), "Product is deleted from server, Cart needs to refresh", Toast.LENGTH_LONG).show();
                                } else {
                                    CustomUtils.hideDialog();
                                    CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                                }
                            }
                            Log.i("Error-JSONResponse", "" + errorResult.toString());
                        }

                        Log.e("jsonObject", errorResult + "");
                    } else {
                        Toast.makeText(getActivity(), "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                    CustomUtils.toastMessage(getActivity(), "Invalid response from server");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.e("errorEncoding", "" + e);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Volley-SuccAddToCart", response.toString());
                ProductModel.setOrderDetails(response.toString());
                Log.i("ORDERDETAILS", ProductModel.getOrderDetails());

                try {
                    jsonObject = new JSONObject(response.toString()).getJSONObject("results");
                    jsonArray = new JSONArray();
                    jsonArray = jsonObject.getJSONArray("order_product");

                    Log.i("JSONARRAY", jsonArray.toString());
                    jsonArr = new JSONArray();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObj = new JSONObject();
                        jsonObj = jsonArray.getJSONObject(i);

                        //------This code is for updating order_product_id, prod_id & order_id of items in database-------//
                        String strOrderProdId, strOrderId, strProdId, strStoreId, strProdStoreName, strProdName, strProdPrice, strProdTotal, strProdDesc,
                                strProdSKU, strProdImage, strProdStockStatus;
                        int intProdQuantity;

                        strOrderProdId = jsonObj.getString("id");
                        strOrderId = jsonObj.getString("order_id");
                        productModel.setId(strOrderId);
                        strProdId = jsonObj.getJSONObject("product").getString("id");
                        strStoreId = jsonObj.getJSONObject("product").getString("store_id");
                        strProdStoreName = jsonObj.getJSONObject("product").getString("name");
                        strProdName = jsonObj.getJSONObject("product").getString("name");
                        intProdQuantity = Integer.parseInt(jsonObj.getString("quantity"));
                        strProdPrice = jsonObj.getJSONObject("product").getString("price");
                        strProdTotal = jsonObj.getString("total");
                        strProdDesc = jsonObj.getJSONObject("product").getString("description");
                        strProdSKU = jsonObj.getJSONObject("product").getString("sku");
                        strProdImage = jsonObj.getJSONObject("product").getJSONObject("image").getString("url");
                        strProdStockStatus = jsonObj.getJSONObject("product").getString("in_stock");

                        Log.i("ProductID-S",
                                strOrderProdId + " "
                                        + strOrderId + " "
                                        + strProdId + " "
                                        + strStoreId + " "
                                        + strProdName + " "
                                        + intProdQuantity + " "
                                        + strProdPrice + " "
                                        + strProdTotal + " "
                                        + strProdDesc + " "
                                        + strProdSKU + " "
                                        + strProdImage + " "
                                        + strProdStockStatus);

                        dbsource.syncCartDB(strOrderProdId, strOrderId, strProdId, strStoreId, strProdStoreName, strProdName, intProdQuantity, strProdPrice, strProdTotal, strProdDesc, strProdSKU, strProdImage, strProdStockStatus, UserModel.getEmail());
                        //------ End of updating order_product_id, prod_id & order_id of items in database -------//

                        String strQuantity = jsonObj.getString("quantity");
                        String strOrderProductId = jsonObj.getString("id");
                        jsonParams = new JSONObject();
                        jsonParams.put("quantity", strQuantity);
                        jsonParams.put("order_product_id", strOrderProductId);
                        jsonArr.put(jsonParams);
                    }

                    Log.i("JSONArr:Quan&OrdProdId ", jsonArr.toString());
                    ProductModel.setOrderList(jsonArr.toString());
                    specialInstruction = edtSpecialInstruction.getText().toString();
                    if (specialInstruction.isEmpty()) {
                        ProductModel.setSpecialInstruction(null);
                        OrderDetailsFragment.strIndicator = "orderPlacement";
                        CustomUtils.loadFragment(new OrderDetailsFragment(), true, getActivity());

                    } else {
                        OrderDetailsFragment.strIndicator = "orderPlacement";
                        ProductModel.setSpecialInstruction(specialInstruction);
                        CustomUtils.loadFragment(new OrderDetailsFragment(), true, getActivity());
                    }

                    Log.i("JSONArr: ", jsonArr.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
    }

}
