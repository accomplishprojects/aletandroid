package com.ph.jumpdigital.alet.User;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by luigigo on 10/8/15.
 */
public class ForgotPassword extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    RadioGroup rgForgotPassword;
    RadioButton rbSentToEmail;
    EditText edtEmail;
    Button btnSendMyPassword;
    String email, volleyTAG;
    HashMap<String, String> params;
    Intent i;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Forgot Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        rbSentToEmail = (RadioButton) findViewById(R.id.rbSentToEmail);
        rgForgotPassword = (RadioGroup) findViewById(R.id.rgForgotPassword);
        rgForgotPassword.setOnCheckedChangeListener(this);
        btnSendMyPassword = (Button) findViewById(R.id.btnSendMyPassword);
        btnSendMyPassword.setOnClickListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rbSentToEmail:
                if (edtEmail.isEnabled() == false) {
                    edtEmail.setEnabled(true);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSendMyPassword:
                if (rbSentToEmail.isChecked()) {
                    email = edtEmail.getText().toString();
                    if (CustomUtils.isValidEmail(email) == false || email.isEmpty()) {
                        edtEmail.setError("Invalid email address");
                    } else {
                        params = new HashMap<String, String>();
                        params.put("email", email);
                        String url = Constants.apiURL + Constants.forgotPassword;
                        Log.i("URL", url);
                        volleyTAG = "forgotpassword";
                        VolleyRequest request = new VolleyRequest(ForgotPassword.this,
                                Constants.postMethod, url,
                                params, null,
                                createRequestSuccessListener(),
                                createRequestErrorListener(),
                                ForgotPassword.this, false, volleyTAG);
                        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, volleyTAG);
                        CustomUtils.showDialog(this);
                    }
                }  else {
                    Toast.makeText(ForgotPassword.this, "Select on options above", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }


    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            JSONArray errorResult = jsonObject.getJSONArray("error");
                            // GET ERROR BY INDEX
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // loop to get the dynamic key
                                    String currentDynamicKey = (String) keys.next();
                                    // get the value of the dynamic key
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                    Toast.makeText(ForgotPassword.this, currentDynamicValue, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Toast.makeText(ForgotPassword.this, "Please check your internet connection",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    CustomUtils.hideDialog();
                }
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(ForgotPassword.this, "Your password was already sent to your email address",
                        Toast.LENGTH_LONG).show();
                if (response != null) {
                    CustomUtils.hideDialog();
                    i = new Intent(ForgotPassword.this, LoginActivity.class);
                    startActivity(i);
                    ForgotPassword.this.finish();
                }
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
