package com.ph.jumpdigital.alet.Drawer;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Fragment.HelpAndSupport.HelpSupportFragment;
import com.ph.jumpdigital.alet.Fragment.Inventory.SearchProductFragment;
import com.ph.jumpdigital.alet.Fragment.LandingFragment;
import com.ph.jumpdigital.alet.Fragment.MyAccount.AddressListAdapter;
import com.ph.jumpdigital.alet.Fragment.MyAccount.PaypalSettingFragment;
import com.ph.jumpdigital.alet.Fragment.MyAccount.ProfileInfo;
import com.ph.jumpdigital.alet.Fragment.OrderPlacement.OrderHistory.OrderHistoryFragment;
import com.ph.jumpdigital.alet.Fragment.OrderPlacement.OrderType;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.StartScreenActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.PaypalUtils;
import com.ph.jumpdigital.alet.Utilities.SharedPref;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.Utilities.UI.CustomTypefaceSpan;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by vidalbenjoe on 9/15/15.
 */
public class MainNavigationDrawer extends AppCompatActivity {
    public static JSONObject jsonObject;
    RelativeLayout mDrawerRelative, wholeLayout;
    LinearLayout frame;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBar actionBar;

    FrameLayout frame_content;
    NavigationView navigationViews;
    MenuItem mi;
    SharedPref sharedPref;
    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Constants.CONFIG_CLIENT_ID)

            // Minimally, you will need to set three merchant information properties.
            // These should be the same values that you provided to PayPal when you registered your app.
            .merchantName("Alet")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.alet.com.ph/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.alet.com.ph/legal"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);

        long heapsize = Runtime.getRuntime().totalMemory();
        System.out.println("heapsize is :: " + heapsize);

        sharedPref = new SharedPref(MainNavigationDrawer.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        Log.i("TOKEN:", UserModel.getAuthToken());

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        mDrawerRelative = (RelativeLayout) findViewById(R.id.sideMenu);
        drawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer_layout);
        frame = (LinearLayout) findViewById(R.id.linear_content);
        wholeLayout = (RelativeLayout) findViewById(R.id.wholeLayout);

        frame_content = (FrameLayout) findViewById(R.id.frame_content);

        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) frame.getLayoutParams();
        frame.setLayoutParams(params);

//        Log.i("CREATEDB", AletDBHelper.query);

        navigationViews = (NavigationView) findViewById(R.id.navigation_view);
        if (navigationViews != null) {
            setupNavigationDrawerContent(navigationViews);
        }

        setupNavigationDrawerContent(navigationViews);
        CustomUtils.loadFragment(new LandingFragment(), true, MainNavigationDrawer.this);

//        Intent intents = new Intent(this, RatingActivity.class);
//        startActivity(intents);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (navigationViews.getWidth() * slideOffset);
                int pageWidth = navigationViews.getWidth();
                /**
                 * Animate layout when drawer slide
                 */
//                if (slideOffset <= 1) {
//                    wholeLayout.setTranslationX(moveFactor * slideOffset);
//                }
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                    wholeLayout.setTranslationX(moveFactor);
//                } else {
//                    TranslateAnimation fade_in = new TranslateAnimation(slideOffset, moveFactor, 0.0f, 0.0f);
//                    fade_in.setDuration(0);
//                    fade_in.setFillAfter(true);
//                    wholeLayout.startAnimation(fade_in);
//                    slideOffset = moveFactor;
//                }
//                Log.i("DRAWER", "SLIDE!");
//                Log.i("DRAWER", String.valueOf(slideOffset));
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.LEFT);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        Menu m = navigationViews.getMenu();
        for (int i = 0; i < m.size(); i++) {
            mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();

            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
//                    Log.i("j Menu", String.valueOf(j));
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);

        }
//        Log.i("miMenu", String.valueOf(m));

    }

    private void setupNavigationDrawerContent(final NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
//                        popBackStack();
                        switch (menuItem.getItemId()) {
                            case R.id.item_navigation_drawer_home:
                                menuItem.setChecked(true);
                                CustomUtils.loadFragment(new LandingFragment(), true, MainNavigationDrawer.this);
                                drawerLayout.closeDrawer(GravityCompat.START);

                                return true;
                            case R.id.item_navigation_drawer_search_by_brand:
                                //Search By Brands
                                menuItem.setChecked(true);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                toolbar.setTitle("Brands");
                                SearchProductFragment.browseInvPos = 0;
                                CustomUtils.loadFragment(new SearchProductFragment(), true, MainNavigationDrawer.this);
                                return true;
                            case R.id.item_navigation_drawer_search_by_alcoholtype:
                                menuItem.setChecked(true);
                                toolbar.setTitle("Alcohol Types");
                                SearchProductFragment.browseInvPos = 1;
                                CustomUtils.loadFragment(new SearchProductFragment(), true, MainNavigationDrawer.this);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_navigation_drawer_search_by_store:
                                menuItem.setChecked(true);
                                toolbar.setTitle("Stores");
                                SearchProductFragment.browseInvPos = 2;
                                CustomUtils.loadFragment(new SearchProductFragment(), true, MainNavigationDrawer.this);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            /*For my Account*/
                            case R.id.item_navigation_drawer_profile_info:
                                toolbar.setTitle("Profile Info");
                                ProfileInfo.page = 0;
                                CustomUtils.loadFragment(new ProfileInfo(), true, MainNavigationDrawer.this);
                                menuItem.setChecked(true);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_navigation_drawer_saved_address:
                                toolbar.setTitle("Saved Address");
                                ProfileInfo.page = 4;
                                AddressListAdapter.indicator = "savedAddress";
                                CustomUtils.loadFragment(new ProfileInfo(), true, MainNavigationDrawer.this);
                                menuItem.setChecked(true);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_navigation_drawer_payment_settings:
                                toolbar.setTitle("Payment");
                                CustomUtils.loadFragment(new PaypalSettingFragment(), true, MainNavigationDrawer.this);
                                menuItem.setChecked(true);
//                                onFuturePaymentPressed(navigationView);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_navigation_drawer_change_password:
                                toolbar.setTitle("Change Password");
                                ProfileInfo.page = 3;
                                CustomUtils.loadFragment(new ProfileInfo(), true, MainNavigationDrawer.this);
                                menuItem.setChecked(true);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_navigation_drawer_preffered_alcohol:
                                toolbar.setTitle("Preferences");
                                ProfileInfo.page = 2;
                                //Toast.makeText(MainNavigationDrawer.this, String.valueOf(ProfileInfo.page), Toast.LENGTH_SHORT).show();
                                CustomUtils.loadFragment(new ProfileInfo(), true, MainNavigationDrawer.this);
                                menuItem.setChecked(true);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.item_navigation_drawer_order_history:
                                OrderType.page = 0;
                                toolbar.setTitle("History");
                                CustomUtils.loadFragment(new OrderHistoryFragment(), true, MainNavigationDrawer.this);
                                menuItem.setChecked(true);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                break;
                            case R.id.item_navigation_drawer_help_support:
                                toolbar.setTitle("Help/Support");
                                CustomUtils.loadFragment(new HelpSupportFragment(), true, MainNavigationDrawer.this);
                                menuItem.setChecked(true);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                break;
                            case R.id.item_navigation_drawer_sign_out:
                                Constants.volleyTAG = "usersignout";
                                String url = Constants.apiURL + Constants.userSignout;

                                VolleyRequest request = new VolleyRequest(MainNavigationDrawer.this,
                                        Constants.delMethod, url,
                                        null, null,
                                        createRequestSuccessListener(),
                                        createRequestErrorListener(),
                                        MainNavigationDrawer.this, false, Constants.volleyTAG);
                                AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
                                MainNavigationDrawer.this.finish();
                                UserModel.setAuthToken(null);
                                break;
                        }
                        return true;

                    }

                });
    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface boldFont = Typeface.createFromAsset(getAssets(), "fonts/TitilliumWeb-Bold.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        CharSequence menuTitle = mi.getTitle();
        if (mi.toString().contains("Browse Inventory") || mi.toString().contains("My Account") || mi.toString().contains("More")) {
            mNewTitle.setSpan(new CustomTypefaceSpan("", boldFont), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            mNewTitle.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF")), 0, menuTitle.length(), 0);
            mi.setTitle(mNewTitle);
        } else {
            mNewTitle.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF")), 0, menuTitle.length(), 0);
            mi.setTitle(mNewTitle);
        }
    }

    public void onFuturePaymentPressed(View pressed) {
        Intent intent = new Intent(MainNavigationDrawer.this, PayPalFuturePaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startActivityForResult(intent, Constants.REQUEST_CODE_FUTURE_PAYMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                Log.i("resultcode", String.valueOf(resultCode));
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                Log.i("confirm", String.valueOf(confirm));
                if (confirm != null) {
                    try {
                        Log.i("TOSTRING 4", confirm.toJSONObject().toString(4));
                        Log.i("TOSTRING 4 GETPAYMENT", confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        Toast.makeText(
                                getApplicationContext(),
                                "PaymentConfirmation info received from PayPal", Toast.LENGTH_LONG)
                                .show();
                    } catch (JSONException e) {
                        Log.i(Constants.TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(Constants.TAG, "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        Constants.TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == Constants.REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                String metadataIdfuture = PayPalConfiguration.getClientMetadataId(this);
                Log.i("metadataIdfuture", String.valueOf(metadataIdfuture));
                if (auth != null) {
                    try {
                        String authorization_code = auth.getAuthorizationCode();

                        sendAuthorizationToServer(auth);
                        Log.i("sendAuth:", authorization_code);
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        JSONObject clientObj = auth.toJSONObject().getJSONObject("client");

                        String paypalenvironment = clientObj.getString("environment");
                        String paypalsdkversion = clientObj.getString("paypal_sdk_version");
                        String paypalplatform = clientObj.getString("platform");
                        String paypalprodname = clientObj.getString("product_name");

                        JSONObject paypalRespObj = auth.toJSONObject().getJSONObject("response");
                        String paypalcode = paypalRespObj.getString("code");
                        String paypaltype = auth.toJSONObject().getString("response_type");

                        PaypalUtils.setPaypalEnvironment(paypalenvironment);
                        PaypalUtils.setPaypalSDKVersion(paypalsdkversion);
                        PaypalUtils.setPaypalPlatform(paypalplatform);
                        PaypalUtils.setPaypalProdName(paypalprodname);

                        PaypalUtils.setPaypalRespCode(paypalcode);
                        PaypalUtils.setPaypalRespType(paypaltype);

                        HashMap<String, String> params = new HashMap<String, String>();

                        params.put("payment_list_id", "1");
                        params.put("environment", paypalenvironment);
                        params.put("paypal_sdk_version", paypalsdkversion);
                        params.put("platform", paypalplatform);
                        params.put("product_name", paypalprodname);

                        params.put("response_code", paypalcode);
                        params.put("response_type", paypaltype);

                        String url = Constants.apiURL + Constants.user_payment_paypal;

                        Constants.volleyTAG = "paypal";
                        VolleyRequest request = new VolleyRequest(MainNavigationDrawer.this,
                                Constants.postMethod, url,
                                params, null,
                                createRequestSuccessListener(),
                                createRequestErrorListener(),
                                MainNavigationDrawer.this, true, Constants.volleyTAG);
                        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);

                        String checkpaypalLoginURL = Constants.apiURL + Constants.check_paypal_login;

                        Constants.volleyTAG = "checkpaypallogin";
                        VolleyRequest paypalLogin = new VolleyRequest(MainNavigationDrawer.this,
                                Constants.getMethod, checkpaypalLoginURL,
                                params, null,
                                createRequestSuccessListener(),
                                createRequestErrorListener(),
                                MainNavigationDrawer.this, true, Constants.volleyTAG);
                        AppController.getInstance().addToRequestQueue(paypalLogin.jsonObjectRequest, Constants.volleyTAG);

                        PaypalUtils.sendAuthorizationToServer(auth);
                        Toast.makeText(
                                getApplicationContext(),
                                "Future Payment code received from PayPal", Toast.LENGTH_LONG)
                                .show();

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == Constants.REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        PaypalUtils.sendAuthorizationToServer(auth);
                        Toast.makeText(
                                getApplicationContext(),
                                "Profile Sharing code received from PayPal", Toast.LENGTH_LONG)
                                .show();

                    } catch (JSONException e) {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {
        Log.i("authToServer: ", String.valueOf(authorization));
        // TODO:
        // Send the authorization response to your server, where it can exchange the authorization code
        // for OAuth access and refresh tokens.
        //
        // Your server must then store these tokens, so that your server code can execute payments
        // for this user in the future.

    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");

                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);
                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                    Toast.makeText(getApplication(), currentDynamicValue,
                                            Toast.LENGTH_LONG).show();
                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

                            Log.e("jsonObject", errorResult + "");
                        } else {
//                            CustomUtils.toastMessage(getAc(), "Please check your internet connection");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.i("Success", response.toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                    Constants.status = response.getBoolean("status");
                    if (Constants.status.toString().contentEquals("true")) {
                        if (Constants.volleyTAG.toString().contentEquals("usersignout")) {
                            SharedPref sharedPref = new SharedPref(MainNavigationDrawer.this);
                            sharedPref.removePref();
                            Toast.makeText(MainNavigationDrawer.this, "Logout Succesfully", Toast.LENGTH_SHORT).show();
                            Intent logout = new Intent(MainNavigationDrawer.this, StartScreenActivity.class);
                            logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            ActivityCompat.finishAffinity(MainNavigationDrawer.this);
                            startActivity(logout);
                            MainNavigationDrawer.this.finish();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    public void onBackPressed() {

        FragmentManager fm = getFragmentManager();
        Log.i("EntryCount", String.valueOf(fm.getBackStackEntryCount()));
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();

        } else if (fm.getBackStackEntryCount() == 1) {
            new AlertDialog.Builder(this)
                    .setTitle("Logout?")
                    .setMessage("Are you sure you want to logout?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            MainNavigationDrawer.super.onBackPressed();
                            Constants.volleyTAG = "usersignout";
                            String url = Constants.apiURL + Constants.userSignout;

                            VolleyRequest request = new VolleyRequest(MainNavigationDrawer.this,
                                    Constants.delMethod, url,
                                    null, null,
                                    createRequestSuccessListener(),
                                    createRequestErrorListener(),
                                    MainNavigationDrawer.this, false, Constants.volleyTAG);
                            AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
                            MainNavigationDrawer.this.finish();
                            UserModel.setAuthToken(null);
                        }
                    }).create().show();
        }
    }


    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

}