package com.ph.jumpdigital.alet.model;

import java.io.Serializable;

/**
 * Created by luigigo on 11/6/15.
 */
public class AlcoholTypeModel implements Serializable {

    String name = null;
    String id = null;
    boolean selected = false;

    public AlcoholTypeModel(String id, String name, boolean selected) {
        super();
        this.id = id;
        this.name = name;
        this.selected = selected;
    }


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
