package com.ph.jumpdigital.alet.User;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Drawer.MainNavigationDrawer;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.SharedPref;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by vidalbenjoe on 9/2/15.
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    public static JSONObject jsonObject;
    public static Boolean status;
    static UserModel userModel = new UserModel();
    Boolean isInternetPresent = false;
    CustomUtils connectionDetector;
    EditText emailEditText, passwordEditText;
    String email, pass, url;

    String volleyTAG;
    HashMap<String, String> params;
    private Button btnLoginButton;
    private TextView txtForgotPassword;
    RelativeLayout btnLoginFacebook;
    private CallbackManager callbackManager;
    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_login_layout);
        connectionDetector = new CustomUtils(getApplicationContext());
        sharedPref = new SharedPref(this);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        btnLoginButton = (Button) findViewById(R.id.loginButton);
        btnLoginFacebook = (RelativeLayout) findViewById(R.id.btnLoginRelative);
        btnLoginFacebook.setOnClickListener(this);
        btnLoginButton.setOnClickListener(this);
        txtForgotPassword = (TextView) findViewById(R.id.txtForgotPassword);
        txtForgotPassword.setOnClickListener(this);

        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        emailEditText.setText("eyes@dispostable.com");
        passwordEditText.setText("password");

    }

    public void FBLogin() {
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile,email, user_birthday, user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        try {
                            GraphRequest request = GraphRequest.newMeRequest(
                                    loginResult.getAccessToken(),
                                    new GraphRequest.GraphJSONObjectCallback() {
                                        @Override
                                        public void onCompleted(
                                                JSONObject object,
                                                GraphResponse response) {
                                            if (object == null) {
                                                Toast.makeText(LoginActivity.this, "Unable to fetch data from facebook!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                try {
                                                    email = object.getString("email");
                                                    UserModel.setUser_id(loginResult.getAccessToken().getUserId());
                                                    params = new HashMap<String, String>();
                                                    params.put("via", "facebook");
                                                    params.put("email", email);
                                                    params.put("device_token", UserModel.getGCMToken());
                                                    emailEditText.setText(email);
                                                    url = Constants.apiURL + Constants.userSignin;
                                                    volleyTAG = "login";
                                                    VolleyRequest request = new VolleyRequest(LoginActivity.this,
                                                            Constants.postMethod, url,
                                                            params, null,
                                                            createRequestSuccessListener(),
                                                            createRequestErrorListener(),
                                                            LoginActivity.this, true, volleyTAG);
                                                    AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, volleyTAG);
                                                    CustomUtils.showDialog(LoginActivity.this);
                                                    LoginManager.getInstance().logOut();
                                                } catch (Exception e) {
                                                    if (e.getMessage().equals("No value for email")) {
                                                        Toast.makeText(LoginActivity.this, "Error sa fb", Toast.LENGTH_SHORT).show();

                                                        AlertDialog.Builder al = new AlertDialog.Builder(LoginActivity.this);
                                                        al.setTitle("Facebook Error");
                                                        al.setMessage("Ooppss! It seems that your email is not set as public.\nPlease try to register manually. Thank you!");
                                                        al.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                dialogInterface.dismiss();

                                                            }
                                                        });
                                                        AlertDialog dialog = al.create();
                                                        dialog.show();

                                                        e.printStackTrace();
                                                        CustomUtils.hideDialog();
                                                    } else {
                                                        Toast.makeText(LoginActivity.this, "", Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                            }
                                        }
                                    });

                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id,first_name, last_name, email,gender, birthday, picture.type(large)");
                            request.setParameters(parameters);
                            request.executeAsync();
                            Log.i("FB-Parameters", "" + request);
                        } catch (FacebookException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginButton:
                isInternetPresent = connectionDetector.isConnectingToInternet();

                if (isInternetPresent) {
                    if (v.getId() == R.id.loginButton) {
                        email = emailEditText.getText().toString();
                        pass = passwordEditText.getText().toString();
                        if (email.isEmpty() && pass.isEmpty()) {
                            emailEditText.setError("Fields can't be left blank");
                            passwordEditText.setError("Fields can't be left blank");
                        } else if (email.isEmpty()) {
                            emailEditText.setError("Fields can't be left blank");
                        } else if (pass.isEmpty()) {
                            passwordEditText.setError("Fields can't be left blank");
                        } else if (pass.isEmpty() || email.isEmpty()) {
                            emailEditText.setError("Fields can't be left blank");
                            passwordEditText.setError("Fields can't be left blank");
                        } else {
                            params = new HashMap<String, String>();
                            params.put("via", "alet");
                            params.put("password", pass);
                            params.put("email", email);
                            params.put("device_token", UserModel.getGCMToken());

                            if (params != null) {
                                url = Constants.apiURL + Constants.userSignin;
                                volleyTAG = "login";
                                VolleyRequest request = new VolleyRequest(LoginActivity.this,
                                        Constants.postMethod, url,
                                        params, null,
                                        createRequestSuccessListener(),
                                        createRequestErrorListener(),
                                        LoginActivity.this, true, volleyTAG);

                                if (request.jsonObjectRequest != null) {
                                    AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, volleyTAG);
                                }

                                CustomUtils.showDialog(this);
                            } else {
                                emailEditText.setError("Fields can't be left blank");
                                passwordEditText.setError("Fields can't be left blank");
                            }
                        }
//                        Toast.makeText(LoginActivity.this, "Devicetoken:" + UserModel.getGCMToken(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    // NO INTERNET CONNECTION
                    CustomUtils.toastMessage(LoginActivity.this, "Please check your internet connection");
                }

                break;

            case R.id.btnLoginRelative:
                if (sharedPref.getAUTHToken() != null) {
                    UserModel.setAuthToken(sharedPref.getAUTHToken());
                    userModel.setEmail(sharedPref.getFBEmail());
                    Intent mainActivity = new Intent(LoginActivity.this, MainNavigationDrawer.class);
                    startActivity(mainActivity);
                    LoginActivity.this.finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    FBLogin();
                }
                break;

            case R.id.txtForgotPassword:
                Intent i = new Intent(this, ForgotPassword.class);
                startActivity(i);
                break;

        }
    }


    private Response.ErrorListener createRequestErrorListener() {

        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("VolleyError", error.getLocalizedMessage() + error.getMessage());
                try {
                    if (error.networkResponse != null) {
                        CustomUtils.hideDialog();
                        String responseBody = new String(error.networkResponse.data, "UTF-8");
                        jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");
                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();
                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                Toast.makeText(LoginActivity.this, currentDynamicValue, Toast.LENGTH_SHORT).show();
                            }
                        }
                        Log.e("jsonObject", errorResult + "");
                    } else {
                        Toast.makeText(LoginActivity.this, "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.e("errorEncoding", "" + e);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("Volley-Success", response.toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                    status = response.getBoolean("status");
                    UserModel.setUser_id(jsonObject.getString("user_id"));
                    UserModel.setAuthToken(jsonObject.getString("auth_token"));
                    userModel.setEmail(jsonObject.getString("email"));
                    UserModel.setStatus(response.getBoolean("status"));
                    sharedPref.setFBEmail(UserModel.getEmail());
                    sharedPref.setAUTHToken(UserModel.getAuthToken());

                    if (status.toString().contentEquals("true")) {
                        Intent mainActivity = new Intent(LoginActivity.this, MainNavigationDrawer.class);
                        mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        ActivityCompat.finishAffinity(LoginActivity.this);
                        startActivity(mainActivity);
                        LoginActivity.this.finish();
//                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        CustomUtils.hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}


