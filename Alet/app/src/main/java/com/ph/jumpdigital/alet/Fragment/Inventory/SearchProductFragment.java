package com.ph.jumpdigital.alet.Fragment.Inventory;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.LayerDrawable;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Database.AletDBHelper;
import com.ph.jumpdigital.alet.Database.AletDataSource;
import com.ph.jumpdigital.alet.Drawer.MainNavigationDrawer;
import com.ph.jumpdigital.alet.Fragment.ItemListing.CartViewFragment;
import com.ph.jumpdigital.alet.Fragment.ItemListing.ProductListFragment;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.CustomHurlStack;
import com.ph.jumpdigital.alet.Utilities.Networking.OkHttpHurlStack;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.SharedPref;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.ProductModel;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Created by vidalbenjoe on 29/09/2015.
 */
public class SearchProductFragment extends Fragment implements SearchView.OnQueryTextListener {
    SharedPref sharedPref;

    ArrayList<HashMap<String, String>> arraylist, arrayListBatch;
    SearchListAdapter adapter;
    JSONArray productsArray;
    public static JSONObject jsonObject;
    public AletDBHelper aletDBHelper;
    public AletDataSource aletDataSource;
    AletDataSource dbsource;
    AletDBHelper dbhelper;
    Cursor cursor = null;
    HashMap<String, String> map;

    ListView listview;
    View mainView;

    Animation slide_up, slide_down;

    String strOrderProdId, strOrderId, strProdId, strStoreId, strProdStoreName, strProdName, strProdPrice, strProdTotal, strProdDesc,
            strProdSKU, strProdImage, strProdStockStatus;
    String url, TAG = "SpecialProductFragment";
    public static int browseInvPos;
    int intProdQuantity;
    int rowCount;
    int limit = 10;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        aletDBHelper = new AletDBHelper(getActivity());
        aletDataSource = new AletDataSource(getActivity());
        aletDataSource.open();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.listview_reusable, container, false);
        sharedPref = new SharedPref(getActivity());
        CustomUtils.showDialog(getActivity());

        dbhelper = new AletDBHelper(getActivity());
        dbsource = new AletDataSource(getActivity());
        dbsource.open();
//
//        slide_down = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
//
//        slide_up = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);

        cursor = AletDataSource.cartOrderCount(UserModel.getEmail());
        if (cursor != null) {
            try {
                rowCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0))));
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
        Log.i("rowCountInventory", String.valueOf(rowCount));

        /**
         * Get data based on the user tapped drawer item from the
         * MainNavigationDrawer
         * 0 = Search By Brand
         * 1 = Search By Alcohol Type
         * 3 = Search By Store
         */

        if (browseInvPos == 0) {
            url = Constants.apiURL + Constants.getAllProductBrand;
        } else if (browseInvPos == 1) {
            url = Constants.apiURL + Constants.getAllProductTypes;
        } else if (browseInvPos == 2) {
            url = Constants.apiURL + Constants.getAllStores;
        }
        Constants.volleyTAG = "viewtypes";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);

        return mainView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.main_menu, menu); // removed to not double the menu items
        MenuItem item = menu.findItem(R.id.action_search);
        final MenuItem cartItem = menu.findItem(R.id.action_cart);

        LayerDrawable icon = (LayerDrawable) cartItem.getIcon();
        CustomUtils.setBadgeCount(getActivity(), icon, rowCount);
        SearchView sv = new SearchView(((MainNavigationDrawer) getActivity()).getSupportActionBar().getThemedContext());
        View searchplate = sv.findViewById(android.support.v7.appcompat.R.id.search_plate);
        searchplate.setBackgroundResource(R.color.md_white_1000);

        ImageView searchCloseIcon = (ImageView) sv.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        searchCloseIcon.setImageResource(R.drawable.searchclosebtn);
        /**
         *  ImageView searchIcon = (ImageView) sv.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
         searchIcon.setImageResource(R.drawable.navbar_search);
         */
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, sv);
        sv.setOnQueryTextListener(this);
        sv.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                getActivity().invalidateOptionsMenu();
                // Do something when collapsed
//                Utils.LogDebug("Closed: ");
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                // User chose the "Settings" item, show the app settings UI...
                getCartItems();
                // CustomUtils.toastMessage(getActivity(), "Sync data from server");
                /**
                 *
                 if (rowCount > 0) {
                 syncCartItems();
                 CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                 } else {
                 syncCartItems();
                 Toast.makeText(getActivity(), "No Item in Cart", Toast.LENGTH_LONG).show();
                 }
                 */
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void getCartItems() {
        listview = (ListView) getActivity().findViewById(R.id.reusableListView);
        url = Constants.apiURL + Constants.viewCart;
        Log.i(TAG, url);
        Constants.volleyTAG = "getcartitems";


        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());
    }

    public void onStart() {
        super.onStart();
        if (browseInvPos == 0) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Brands");
        } else if (browseInvPos == 1) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Alcohol Types");
        } else if (browseInvPos == 2) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Stores");
        }

        cursor = AletDataSource.cartOrderCount(UserModel.getEmail());
        if (cursor != null) {
            try {
                rowCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0))));
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void deleteAllCartItems() {
        HttpStack httpStack;
        if (Build.VERSION.SDK_INT > 19) {
            httpStack = new CustomHurlStack();
        } else if (Build.VERSION.SDK_INT >= 9 && Build.VERSION.SDK_INT <= 19) {
            httpStack = new OkHttpHurlStack();
        } else {
            httpStack = new HttpClientStack(AndroidHttpClient.newInstance("Android"));
        }
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), httpStack);
        String url = Constants.apiURL + Constants.deleteAllCartItems;
        Log.i(TAG, url);
        Constants.volleyTAG = "deleteAllCartItems";

        JsonObjectRequest deleteRequest = new JsonObjectRequest(
                com.android.volley.Request.Method.DELETE,
                url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("OkHttp-ItemDeleted: ", response.toString());

                dbsource.deleteAllCartByEmail(UserModel.getEmail());
                // Toast.makeText(getActivity(), "All cart items deleted", Toast.LENGTH_SHORT).show();
                CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                CustomUtils.hideDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("ErrorMessage: ", error.getMessage());
                try {
                    if (error.getMessage() != null) {
                        String responseBody = String.valueOf(error.getMessage());
                        JSONObject jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");

                        String errorCode = jsonObject.getString("error_code");
                        String errorDesc = jsonObject.getString("error_description");

                        Log.i("errorCode:", errorCode);
                        Log.i("errorDesc:", errorDesc);

                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();

                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                // do something here with the value...
                                // Toast.makeText(getActivity(), currentDynamicValue, Toast.LENGTH_SHORT).show();
                                CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                CustomUtils.hideDialog();

                            }
                            Log.i("Error-JSONResponse", "" + errorResult.toString());
                        }

                        Log.e("jsonObject", errorResult + "");
                    } else {
                        Toast.makeText(getActivity(), "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                        CustomUtils.hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                    Toast.makeText(getActivity(), "Please check your internet connection",
                            Toast.LENGTH_SHORT).show();
                }
                CustomUtils.hideDialog();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("x-auth-token", UserModel.getAuthToken());
                return headers;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                String json;
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    try {
                        json = new String(volleyError.networkResponse.data,
                                HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
                    } catch (UnsupportedEncodingException e) {
                        return new VolleyError(e.getMessage());
                    }
                    return new VolleyError(json);
                }
                return volleyError;
            }
        };

        requestQueue.add(deleteRequest);
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");

                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);
                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                    if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                        CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                        gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ActivityCompat.finishAffinity(getActivity());
                                        getActivity().finish();
                                        startActivity(gotoLogin);
                                    } else {
                                        CustomUtils.hideDialog();
                                        CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                                    }

                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

                            Log.e("jsonObject", errorResult + "");
                            //}
                        } else {
                            Toast.makeText(getActivity(), "Can't sync data from server.\nPlease check your internet connection", Toast.LENGTH_SHORT).show();
                            // CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                        Toast.makeText(getActivity(), "Invalid Response from server", Toast.LENGTH_SHORT).show();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Success-SyncCart", response.toString());

                if (Constants.volleyTAG == "viewtypes") {
                    arraylist = new ArrayList<HashMap<String, String>>();
                    if (response != null) {
                        try {
                            Constants.status = response.getBoolean("status");
                            JSONArray prodResult = response.getJSONArray("results");

                            Log.i("Volley-responseRes", "" + response.toString(4));
                            Log.i("Volley-resultsArray", "" + prodResult.toString(4));
                            //loop content of JSONArray
                            for (int i = 0; i < prodResult.length(); i++) {
                                JSONObject resultsObj = prodResult.getJSONObject(i);
                                productsArray = resultsObj.optJSONArray("products");
                                JSONObject imageObj = resultsObj.getJSONObject("image");
                                map = new HashMap<String, String>();
                                map.put("object_name", resultsObj.optString("name")); // store // brand / type
                                map.put("object_image", imageObj.getString("url"));
                                map.put("products_array", String.valueOf(productsArray));
                                arraylist.add(map);
                            }

                            if (Constants.status.toString().contentEquals("true")) {
                                CustomUtils.hideDialog();
                                if (UserModel.getAuthToken() != null) {

                                    listview = (ListView) getActivity().findViewById(R.id.reusableListView);
                                    arrayListBatch = new ArrayList<HashMap<String, String>>();
//                                    if (arraylist.size() > 0) {
//                                        for (int i = 0; i < limit; i++) {
//                                            HashMap<String, String> hm = new HashMap<String, String>();
//                                            hm.put("object_name", arraylist.get(i).get("object_name"));
//                                            hm.put("object_image", arraylist.get(i).get("object_image"));
//                                            hm.put("products_array", String.valueOf(productsArray));
//                                            arrayListBatch.add(hm);
//                                        }
//                                    }


                                    if (limit < arraylist.size()) {
                                        int lastIndex = limit;
                                        int remainingIndex = (arraylist.size() - limit);
                                        //if (remainingIndex >= 10) {
                                        //limit += 10;
                                        for (int i = 0; i < limit; i++) {
                                            HashMap<String, String> hm = new HashMap<String, String>();
                                            hm.put("object_name", arraylist.get(i).get("object_name"));
                                            hm.put("object_image", arraylist.get(i).get("object_image"));
                                            hm.put("products_array", arraylist.get(i).get("products_array"));
                                            arrayListBatch.add(hm);
                                        }
//                                        } else {
//                                            limit += remainingIndex;
//                                            for (int i = 0; i < arraylist.size(); i++) {
//                                                HashMap<String, String> hm = new HashMap<String, String>();
//                                                hm.put("object_name", arraylist.get(i).get("object_name"));
//                                                hm.put("object_image", arraylist.get(i).get("object_image"));
//                                                hm.put("products_array", String.valueOf(productsArray));
//                                                arrayListBatch.add(hm);
//                                            }
//                                        }
                                    } else {
                                        int lastIndex = limit;
                                        //limit += 10;
                                        for (int i = 0; i < arraylist.size(); i++) {
                                            HashMap<String, String> hm = new HashMap<String, String>();
                                            hm.put("object_name", arraylist.get(i).get("object_name"));
                                            hm.put("object_image", arraylist.get(i).get("object_image"));
                                            hm.put("products_array", arraylist.get(i).get("products_array"));
                                            arrayListBatch.add(hm);
                                        }
                                    }

                                    // Pass the results into ListViewAdapter.java
                                    adapter = new SearchListAdapter(getActivity(), arrayListBatch, browseInvPos);

                                    // Set the adapter to the ListView
                                    if (listview != null) {
                                        listview.setAdapter(adapter);

//                                        // This line is for load more functionality of listview
                                        listview.setOnScrollListener(new AbsListView.OnScrollListener() {

                                            @Override
                                            public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                                                int threshold = 1;
                                                int count = listview.getCount();
                                                if (scrollState == SCROLL_STATE_IDLE) {
                                                    if (listview.getLastVisiblePosition() >= count
                                                            - threshold) {
                                                        // Execute LoadMoreDataTask AsyncTask
                                                        new LoadMoreDataTask().execute();
                                                        Snackbar snackbar = Snackbar
                                                                .make(view, "Loading more data...", Snackbar.LENGTH_SHORT);
                                                        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
                                                        snackbarLayout.addView(new ProgressBar(getActivity()));
                                                        snackbar.show();
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onScroll(AbsListView view, int firstVisibleItem,
                                                                 int visibleItemCount, int totalItemCount) {
                                                // TODO Auto-generated method stub
                                            }
                                        });
                                    }
                                    CustomUtils.hideDialog();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getActivity(), "No Result", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //getitemCart
                    try {
                        jsonObject = new JSONObject(response.toString()).getJSONObject("results");
                        JSONArray jsonArr = new JSONArray();
                        jsonArr = jsonObject.getJSONArray("order_product");

                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject jsonObj = jsonArr.getJSONObject(i);

                            if (jsonObj.has("product")) {
                                JSONObject jsonObjProduct = jsonObj.getJSONObject("product");

                                // JSONObject -- jsonObj
                                strOrderProdId = jsonObj.getString("id");
                                strOrderId = jsonObj.getString("order_id");
                                intProdQuantity = Integer.parseInt(jsonObj.getString("quantity"));
                                strProdTotal = jsonObj.getString("total");
                                strProdStoreName = jsonObj.getString("name");

                                // JSONObject -- jsonObjProduct
                                strProdId = jsonObjProduct.getString("id");
                                strStoreId = jsonObjProduct.getString("store_id");
                                strProdName = jsonObjProduct.getString("name");
                                strProdPrice = jsonObjProduct.getString("price");
                                strProdDesc = jsonObjProduct.getString("description");
                                strProdSKU = jsonObjProduct.getString("sku");
                                strProdImage = jsonObjProduct.getJSONObject("image").getString("url");
                                strProdStockStatus = jsonObjProduct.getString("in_stock");

//                                Log.i("ProductID-S",
//                                        strOrderProdId + " "
//                                                + strOrderId + " "
//                                                + strProdId + " "
//                                                + strStoreId + " "
//                                                + strProdName + " "
//                                                + intProdQuantity + " "
//                                                + strProdPrice + " "
//                                                + strProdTotal + " "
//                                                + strProdDesc + " "
//                                                + strProdSKU + " "
//                                                + strProdImage + " "
//                                                + strProdStockStatus);


                                dbsource.syncCartDB(strOrderProdId, strOrderId, strProdId, strStoreId, strProdStoreName, strProdName, intProdQuantity, strProdPrice, strProdTotal, strProdDesc, strProdSKU, strProdImage, strProdStockStatus, UserModel.getEmail());
                                CustomUtils.hideDialog();
                            } else {
                                Log.i(TAG, "Product key is not available");
                                deleteAllCartItems();
                            }
                        }

                        CustomUtils.loadFragment(new CartViewFragment(), true, getActivity());

                    } catch (JSONException e) {
//                        CustomUtils.toastMessage(getActivity(), "No Value for product");
                        CustomUtils.hideDialog();
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (browseInvPos == 0) {
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.filter(newText);
        return false;
    }

    // This class is for load more functionality of listview
    private class LoadMoreDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TITLES", String.valueOf(arraylist.size()));
            Log.i("TITLES-LIMIT", String.valueOf(limit));

            if (limit < arraylist.size()) {
                int lastIndex = limit;
                int remainingIndex = (arraylist.size() - limit);
                if (remainingIndex >= 5) {
                    limit += 5;
                    for (int j = lastIndex; j < limit; j++) {
                        HashMap<String, String> hm = new HashMap<String, String>();
                        hm.put("object_name", arraylist.get(j).get("object_name"));
                        hm.put("object_image", arraylist.get(j).get("object_image"));
                        hm.put("products_array", String.valueOf(productsArray));
                        arrayListBatch.add(hm);
                    }
                } else {
                    limit += remainingIndex;
                    for (int j = lastIndex; j < arraylist.size(); j++) {
                        HashMap<String, String> hm = new HashMap<String, String>();
                        hm.put("object_name", arraylist.get(j).get("object_name"));
                        hm.put("object_image", arraylist.get(j).get("object_image"));
                        hm.put("products_array", String.valueOf(productsArray));
                        arrayListBatch.add(hm);
                    }
                }
            } else {
                int lastIndex = limit;
                for (int j = lastIndex; j < arraylist.size(); j++) {
                    HashMap<String, String> hm = new HashMap<String, String>();
                    hm.put("object_name", arraylist.get(j).get("object_name"));
                    hm.put("object_image", arraylist.get(j).get("object_image"));
                    hm.put("products_array", String.valueOf(productsArray));
                    arrayListBatch.add(hm);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Locate listview last item
            int position = listview.getLastVisiblePosition();
            // Pass the results into ListViewAdapter.java
            adapter = new SearchListAdapter(getActivity(), arrayListBatch, browseInvPos);
            // Binds the Adapter to the ListView
//            listview.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            // Show the latest retrived results on the top
            listview.setSelectionFromTop(position, 0);
            // Close the progressdialog


        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dbsource.close();
    }

    public class SearchListAdapter extends BaseAdapter {

        Context context;
        LayoutInflater inflater;
        ArrayList<HashMap<String, String>>
                data, tempData;

        private RequestQueue mRequestQueue;
        //com.ph.jumpdigital.alet.Utilities.UI.ImageLoaders.ImageLoader imageLoader;
        String imgURL;
        int browseInvPos;


        public SearchListAdapter(Context context,
                                 ArrayList<HashMap<String, String>> arraylist, int browseInvPos) {
            this.context = context;
            data = arraylist;
            this.tempData = new ArrayList<HashMap<String, String>>();
            this.tempData.addAll(data);
            this.browseInvPos = browseInvPos;
            //imageLoader = new ImageLoader(context);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        public class ViewHolder {
            ImageView productImage;
            TextView listTxtTitle, listTxtTitleBrands;
            HashMap<String, String> resultp = new HashMap<String, String>();
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {

            final ViewHolder holder;
            if (view == null) {
                holder = new ViewHolder();
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.search_main_layout, viewGroup, false);
                holder.productImage = (ImageView) view.findViewById(R.id.brandImage);
                holder.listTxtTitle = (TextView) view.findViewById(R.id.listTxtTitle);
                holder.listTxtTitleBrands = (TextView) view.findViewById(R.id.listTxtTitleBrands);
//                holder.listTxtTitle.setVisibility(View.INVISIBLE);
//                holder.listTxtTitleBrands.setVisibility(View.INVISIBLE);
                // Set gradient color for listTxtTitle
//                int[] color = {Color.parseColor("#a2984b"), Color.parseColor("#FFFFFF")};
//                float[] pos = {0, 1};
//                Shader.TileMode tile_mode = Shader.TileMode.MIRROR; // or TileMode.REPEAT;
//                LinearGradient lin_grad = new LinearGradient(0, 20, 0, 50, color, pos, tile_mode);
//                Shader shader_gradient = lin_grad;
//                holder.listTxtTitleBrands.getPaint().setShader(shader_gradient);

                view.setTag(holder);

            } else {
                holder = (ViewHolder) view.getTag();

            }

            holder.resultp = data.get(position);
            holder.listTxtTitle.setText(holder.resultp.get("object_name"));
            if (browseInvPos == 0) {
                holder.listTxtTitleBrands.setText(holder.resultp.get("object_name"));
            } else {
                holder.listTxtTitleBrands.setVisibility(View.GONE);
            }
            imgURL = Constants.baseURL + holder.resultp.get("object_image");
//            holder.listTxtTitle.setShadowLayer(20, 0, 0, Color.BLACK);
//            holder.productImage.setDefaultImageResId(R.drawable.defaultimage);
//            holder.productImage.setErrorImageResId(R.drawable.defaulterror);

            Glide.with(getActivity())
                    .load(imgURL)
                    .centerCrop()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(holder.productImage);

//            mRequestQueue = Volley.newRequestQueue(context);
//            mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
//                private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
//
//                public void putBitmap(String url, Bitmap bitmap) {
//                    mCache.put(url, bitmap);
//                }
//
//                public Bitmap getBitmap(String url) {
//                    return mCache.get(url);
//                }
//            });
//
//            mImageLoader = AppController.getInstance().getImageLoader();
//            // Set the URL of the image that should be loaded into this view, and
//            // specify the ImageLoader that will be used to make the request.
//            holder.productImage.setImageUrl(imgURL, mImageLoader);
//            holder.productImage.setDefaultImageResId(R.drawable.defaultimage);
////            holder.productImage.setErrorImageResId(R.drawable.defaulterror);
//
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inSampleSize = 8;
//           // Bitmap preview_bitmap = BitmapFactory.decodeStream(imgURL, null, options);
//
//
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // Get the position
                    holder.resultp = data.get(position);
                    ProductListFragment prodFrag = new ProductListFragment();
                    //put product_list to model
                    CustomUtils.loadFragment(prodFrag, true, context);
                    Bundle bundle = new Bundle();

                    if (browseInvPos == 2) {
                        ProductModel.setStoreName(holder.resultp.get("object_name"));
                    }
                    bundle.putString("product_list", holder.resultp.get("products_array"));
                    bundle.putString("object_name", holder.resultp.get("object_name"));
                    bundle.putString("object_header_image", holder.resultp.get("object_image"));
                    prodFrag.setArguments(bundle);

                }
            });

            return view;

        }

        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            data.clear();
            if (charText.length() == 0) {
                data.addAll(tempData);
            } else {
                for (HashMap<String, String> wp : tempData) {
                    if (wp.get("object_name").toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        data.add(wp);
                    } else {
                        Log.i("Start Filter :", "ERROR");
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

}
