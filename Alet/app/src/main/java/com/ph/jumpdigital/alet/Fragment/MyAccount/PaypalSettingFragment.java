package com.ph.jumpdigital.alet.Fragment.MyAccount;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalOAuthScopes;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.CustomHurlStack;
import com.ph.jumpdigital.alet.Utilities.Networking.OkHttpHurlStack;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.PaypalUtils;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by vidalbenjoe on 06/12/2015.
 */
public class PaypalSettingFragment extends Fragment {
    public static JSONObject jsonObject;
    String response_code;
    Button paypalSettingBtn;
    ProgressBar paypalProgressCircle;
    private static PayPalConfiguration config = new PayPalConfiguration()

            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Constants.CONFIG_CLIENT_ID)
            // Minimally, you will need to set three merchant information properties.
            // These should be the same values that you provided to PayPal when you registered your app.
            .merchantName("Alet")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.alet.com.ph/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.alet.com.ph/legal"));

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        getActivity().startService(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.paypal_settings_layout, viewGroup, false);

        paypalSettingBtn = (Button) mainView.findViewById(R.id.paypalSettingBtn);
        paypalProgressCircle = (ProgressBar) mainView.findViewById(R.id.paypalProgressCircle);
        checkPaypalLogin();

        return mainView;

    }

    private void checkPaypalLogin() {
        String checkpaypalLoginURL = Constants.apiURL + Constants.check_paypal_login;
        Constants.volleyTAG = "checkpaypallogin";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, checkpaypalLoginURL,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
    }


    private void removePaypal() {
        CustomUtils.showDialog(getActivity());
        HttpStack httpStack;
        if (Build.VERSION.SDK_INT > 19) {
            httpStack = new CustomHurlStack();
        } else if (Build.VERSION.SDK_INT >= 9 && Build.VERSION.SDK_INT <= 19) {
            httpStack = new OkHttpHurlStack();
        } else {
            httpStack = new HttpClientStack(AndroidHttpClient.newInstance("Android"));
        }

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), httpStack);
        String removePaypalAccntURL = Constants.apiURL + Constants.remove_paypal_account;
        Constants.volleyTAG = "deleteCartItems";
        final JSONObject jsonObject = new JSONObject();

        JsonObjectRequest deleteRequest = new JsonObjectRequest(
                com.android.volley.Request.Method.DELETE,
                removePaypalAccntURL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("OkHttp-ItemDeleted: ", response.toString());
                checkPaypalLogin();
                CustomUtils.hideDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    if (error.getMessage() != null) {
                        String responseBody = String.valueOf(error.getMessage());
                        JSONObject jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");
                        String errorCode = jsonObject.getString("error_code");
                        String errorDesc = jsonObject.getString("error_description");
                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();
                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                // do something here with the value...
                                Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                //Toast.makeText(getActivity(), currentDynamicValue, Toast.LENGTH_SHORT).show();
                                CustomUtils.hideDialog();
                            }
                            Log.i("Error-JSONResponse", "" + errorResult.toString());
                        }
                        Log.e("jsonObject", errorResult + "");
                    } else {
                        Toast.makeText(getActivity(), "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                    Toast.makeText(getActivity(), "Please check your internet connection",
                            Toast.LENGTH_SHORT).show();
                }
                CustomUtils.hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("x-auth-token", UserModel.getAuthToken());
                return headers;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                String json;
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    try {
                        json = new String(volleyError.networkResponse.data,
                                HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
                    } catch (UnsupportedEncodingException e) {
                        return new VolleyError(e.getMessage());
                    }
                    return new VolleyError(json);
                }
                return volleyError;
            }
        };

        requestQueue.add(deleteRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                Log.i("resultcode", String.valueOf(resultCode));
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);

                Log.i("confirm", String.valueOf(confirm));
                if (confirm != null) {
                    try {
                        Log.i("TOSTRING 4", confirm.toJSONObject().toString(4));
                        Log.i("TOSTRING 4 GETPAYMENT", confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        Toast.makeText(
                                getActivity(),
                                "PaymentConfirmation info received from PayPal", Toast.LENGTH_LONG)
                                .show();
                    } catch (JSONException e) {
                        Log.i(Constants.TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(Constants.TAG, "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        Constants.TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == Constants.REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        String authorization_code = auth.getAuthorizationCode();
                        sendAuthorizationToServer(auth);
                        Log.i("sendAuth:", authorization_code);
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));
                        String metadataIdfuture = PayPalConfiguration.getClientMetadataId(getActivity());
                        Log.i("metadataIdfuture", String.valueOf(metadataIdfuture));

                        JSONObject clientObj = auth.toJSONObject().getJSONObject("client");
                        String paypalenvironment = clientObj.getString("environment");
                        String paypalsdkversion = clientObj.getString("paypal_sdk_version");
                        String paypalplatform = clientObj.getString("platform");
                        String paypalprodname = clientObj.getString("product_name");

                        JSONObject paypalRespObj = auth.toJSONObject().getJSONObject("response");
                        String paypalcode = paypalRespObj.getString("code");
                        String paypaltype = auth.toJSONObject().getString("response_type");

                        PaypalUtils.setPaypalEnvironment(paypalenvironment);
                        PaypalUtils.setPaypalSDKVersion(paypalsdkversion);
                        PaypalUtils.setPaypalPlatform(paypalplatform);
                        PaypalUtils.setPaypalProdName(paypalprodname);
                        PaypalUtils.setPaypalRespCode(paypalcode);
                        PaypalUtils.setPaypalRespType(paypaltype);
                        PaypalUtils.setPaypalMetaID(metadataIdfuture);
                        PaypalUtils.sendAuthorizationToServer(auth);

                        HashMap<String, String> params = new HashMap<String, String>();

                        params.put("payment_list_id", "1");
                        params.put("environment", paypalenvironment);
                        params.put("paypal_sdk_version", paypalsdkversion);
                        params.put("platform", paypalplatform);
                        params.put("product_name", paypalprodname);
                        params.put("correlation_id", metadataIdfuture);

                        params.put("response_code", paypalcode);
                        params.put("response_type", paypaltype);
                        String url = Constants.apiURL + Constants.user_payment_paypal;
//                        CustomUtils.showDialog(getActivity());
                        Constants.volleyTAG = "paypal";
                        VolleyRequest request = new VolleyRequest(getActivity(),
                                Constants.postMethod, url,
                                params, null,
                                createRequestSuccessListener(),
                                createRequestErrorListener(),
                                getActivity(), true, Constants.volleyTAG);
                        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
                        Toast.makeText(
                                getActivity(),
                                "Future Payment code received from PayPal", Toast.LENGTH_LONG)
                                .show();
                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == Constants.REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingxampleRet", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        PaypalUtils.sendAuthorizationToServer(auth);
                        Toast.makeText(
                                getActivity(),
                                "Profile Sharing code received from PayPal", Toast.LENGTH_LONG)
                                .show();

                    } catch (JSONException e) {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

        Log.i("authToServer: ", String.valueOf(authorization));
        // TODO:
        // Send the authorization response to your server, where it can exchange the authorization code
        // for OAuth access and refresh tokens.
        //
        // Your server must then store these tokens, so that your server code can execute payments
        // for this user in the future.

    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.i("SuccesPaypalLogin", response.toString(4));
                    jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                    Constants.status = response.getBoolean("status");

                    if (Constants.volleyTAG.toString().contentEquals("checkpaypallogin")) {
                        String id = jsonObject.optString("id");
                        String environment = jsonObject.optString("environment");
                        String paypal_sdk_version = jsonObject.optString("paypal_sdk_version");
                        String platform = jsonObject.optString("platform");
                        String product_name = jsonObject.optString("product_name");
                        response_code = jsonObject.optString("response_code");
                        String response_type = jsonObject.optString("response_type");
                        String user_id = jsonObject.optString("user_id");

                        Log.i("paypal_sdk_versionlog", environment);
                        Log.i("paypal_sdk_version", paypal_sdk_version);
                        Log.i("platform", platform);
                        Log.i("product_name", product_name);
                        Log.i("response_code", response_code);
                        Log.i("response_type", response_type);
                        if (Constants.status.toString().contentEquals("true")) {
                            if (response_code != null) {
                                paypalSettingBtn.setText("Remove");
                                paypalSettingBtn.setBackgroundColor(getResources().getColor(R.color.md_red_600));
                                paypalSettingBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Log.i("removePaypale", "remove");
                                        removePaypal();
                                    }
                                });
                            }
                        } else {
                            paypalSettingBtn.setText("Add");
                            paypalSettingBtn.setBackgroundColor(getResources().getColor(R.color.theme_color));
                            paypalSettingBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Log.i("addEDPay", "ad");
                                    onFuturePaymentPressed();
                                }
                            });
                        }
                    } else {
                        checkPaypalLogin();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                CustomUtils.hideDialog();
                paypalProgressCircle.setVisibility(View.INVISIBLE);
                paypalSettingBtn.setVisibility(View.VISIBLE);
            }
        };
    }


    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");

                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);
                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                    Toast.makeText(getActivity(), currentDynamicValue,
                                            Toast.LENGTH_LONG).show();

                                    if (currentDynamicValue.contentEquals("You must login first")) {
                                        Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(gotoLogin);
                                        getActivity().finish();
                                    }

                                    if (Constants.volleyTAG.toString().contentEquals("checkpaypallogin")) {
                                        if (currentDynamicKey.contentEquals("You need to login first in Paypal")) {
                                            paypalSettingBtn.setText("Add");
                                            paypalSettingBtn.setBackgroundColor(getResources().getColor(R.color.theme_color));
                                            paypalSettingBtn.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    onFuturePaymentPressed();
                                                }
                                            });
                                        }
                                    }

                                    if (errorCode.contains("2")) {
                                        paypalSettingBtn.setText("Add");
                                        paypalSettingBtn.setBackgroundColor(getResources().getColor(R.color.theme_color));
                                        paypalSettingBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                onFuturePaymentPressed();
                                            }
                                        });
                                    }
                                }
                            }

                            Log.e("jsonObject", errorResult + "");
                        } else {
//                            CustomUtils.toastMessage(getAc(), "Please check your internet connection");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                paypalProgressCircle.setVisibility(View.INVISIBLE);
                paypalSettingBtn.setVisibility(View.VISIBLE);
                CustomUtils.hideDialog();
            }
        };
    }

    public void onFuturePaymentPressed() {
//        String metadataIdfuture = PayPalConfiguration.getClientMetadataId(this);
//        Log.i("metadataIdfuture", String.valueOf(metadataIdfuture));
        Intent intent = new Intent(getActivity(), PayPalFuturePaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        // send the same configuration for restart resiliency
//        intent.putExtra(PayPalProfileSharingActivity.EXTRA_REQUESTED_SCOPES, getOauthScopes());
//        startActivityForResult(intent, Constants.REQUEST_CODE_PROFILE_SHARING);
        startActivityForResult(intent, Constants.REQUEST_CODE_FUTURE_PAYMENT);
    }

    private PayPalOAuthScopes getOauthScopes() {
    /* create the set of required scopes
     * Note: see https://developer.paypal.com/docs/integration/direct/identity/attributes/ for mapping between the
     * attributes you select for this app in the PayPal developer portal and the scopes required here.
     */
        Set<String> scopes = new HashSet<String>(
                Arrays.asList(
                        PayPalOAuthScopes.PAYPAL_SCOPE_EMAIL,
                        PayPalOAuthScopes.PAYPAL_SCOPE_ADDRESS));
        return new PayPalOAuthScopes(scopes);
    }


}
