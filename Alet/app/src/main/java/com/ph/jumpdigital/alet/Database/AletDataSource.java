package com.ph.jumpdigital.alet.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ph.jumpdigital.alet.model.ProductModel;
import com.ph.jumpdigital.alet.model.UserModel;

import java.util.ArrayList;

/**
 * Created by vidalbenjoe on 04/11/2015.
 */
public class AletDataSource {
    private static SQLiteDatabase database;
    AletDBHelper helper;
    private String[] allColumns = {
            AletDBHelper.COLUMN_ENTRY_ID, AletDBHelper.COLUMN_ORDER_PROD_ID,
            AletDBHelper.COLUMN_PROD_ID, AletDBHelper.COLUMN_STORE_ID,
            AletDBHelper.COLUMN_PROD_STORE_NAME, AletDBHelper.COLUMN_PROD_NAME,
            AletDBHelper.COLUMN_PROD_QUANTITY, AletDBHelper.COLUMN_PROD_PRICE,
            AletDBHelper.COLUMN_PROD_TOTAL, AletDBHelper.COLUMN_PROD_DESC,
            AletDBHelper.COLUMN_PROD_SKU, AletDBHelper.COLUMN_PROD_IMAGE,
            AletDBHelper.COLUMN_PROD_STOCK_STATUS, AletDBHelper.COLUMN_USER_EMAIL,
            AletDBHelper.COLUMN_ORDER_ID, AletDBHelper.PROD_STOCK_AVAIL, AletDBHelper.PROD_TEMP_AVAIL_STOCK};

    public AletDataSource(Context context) {
        helper = new AletDBHelper(context);
    }

    /**
     * @param orderprodID
     * @param orderID
     * @param prodID
     * @param storeID
     * @param prodStoreName
     * @param prodName
     * @param prodQuantity
     * @param prodPrice
     * @param prodTotal
     * @param prodDesc
     * @param prodSKU
     * @param prodImage
     * @param stockStatus
     * @param userEmail
     * @param prodStockQuantity
     */
    public void addcartDB(String orderprodID, String orderID, String prodID, String storeID, String prodStoreName,
                          String prodName, int prodQuantity, String prodPrice, String prodTotal,
                          String prodDesc, String prodSKU, String prodImage, String stockStatus,
                          String userEmail, int prodStockQuantity, int tempProductAvailable) {
        //Check Cart Item if already exist before adding it to database
        String checkItem = "SELECT * FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " where " + AletDBHelper.COLUMN_PROD_ID + " =? and " + AletDBHelper.COLUMN_USER_EMAIL + "=?";
        // Put it in an array to avoid an unrecognized token error
        Cursor cursor = database.rawQuery(checkItem, new String[]{orderprodID, UserModel.getEmail()});

        if (cursor.moveToFirst()) {
            //If record already exist ~> update the record based on the productID
            upDateCartByID(orderprodID, orderID, prodID, storeID, prodStoreName, prodName, Integer.parseInt(String.valueOf(prodQuantity)), prodPrice, prodTotal, prodDesc, prodSKU, prodImage, stockStatus, userEmail, prodStockQuantity, tempProductAvailable);
        } else {
            // record not found
            // we are using ContentValues to avoid sql format errors

            ContentValues contentValues = new ContentValues();
            contentValues.put(allColumns[1], orderprodID);
            contentValues.put(allColumns[2], prodID);
            contentValues.put(allColumns[3], storeID);
            contentValues.put(allColumns[4], prodStoreName);
            contentValues.put(allColumns[5], prodName);
            contentValues.put(allColumns[6], prodQuantity);
            contentValues.put(allColumns[7], prodPrice);
            contentValues.put(allColumns[8], prodTotal);
            contentValues.put(allColumns[9], prodDesc);
            contentValues.put(allColumns[10], prodSKU);
            contentValues.put(allColumns[11], prodImage);
            contentValues.put(allColumns[12], stockStatus);
            contentValues.put(allColumns[13], userEmail);
            contentValues.put(allColumns[14], orderID);
            contentValues.put(allColumns[15], prodStockQuantity);
            contentValues.put(allColumns[16], tempProductAvailable);

            database.insert(AletDBHelper.TABLE_ORDER_PRODUCT, null, contentValues);
        }
        cursor.close();

    }

    public void syncCartDB(String orderprodID, String orderID, String prodID, String storeID, String prodStoreName,
                           String prodName, int prodQuantity, String prodPrice, String prodTotal,
                           String prodDesc, String prodSKU, String prodImage, String stockStatus,
                           String userEmail) {
        //Check Cart Item if already exist before adding it to database
        String checkItem = "SELECT * FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " where " + AletDBHelper.COLUMN_PROD_ID + " =? and " + AletDBHelper.COLUMN_USER_EMAIL + "=?";
        // Put it in an array to avoid an unrecognized token error
        Cursor cursor = database.rawQuery(checkItem, new String[]{prodID, UserModel.getEmail()});

        if (cursor.moveToFirst()) {
            //If record already exist ~> update the record based on the productID
            //upDateCartByID(orderprodID, prodID, storeID, prodStoreName, prodName, Integer.parseInt(String.valueOf(prodQuantity)), prodPrice, prodTotal, prodDesc, prodSKU, prodImage, stockStatus, userEmail);

            updateAfterSyncByProdIdEmail(orderprodID, orderID, prodID, userEmail);
            Log.i("Exist!", "Exist!");
        } else {
            /**
             * record not found
             * we are using ContentValues to avoid sql format errors
             */
            ContentValues contentValues = new ContentValues();
            contentValues.put(allColumns[1], orderprodID);
            contentValues.put(allColumns[2], prodID);
            contentValues.put(allColumns[3], storeID);
            contentValues.put(allColumns[4], prodStoreName);
            contentValues.put(allColumns[5], prodName);
            contentValues.put(allColumns[6], prodQuantity);
            contentValues.put(allColumns[7], prodPrice);
            contentValues.put(allColumns[8], prodTotal);
            contentValues.put(allColumns[9], prodDesc);
            contentValues.put(allColumns[10], prodSKU);
            contentValues.put(allColumns[11], prodImage);
            contentValues.put(allColumns[12], stockStatus);
            contentValues.put(allColumns[13], userEmail);
            contentValues.put(allColumns[14], orderID);

            Log.i("Not Exist!", "Not Exist!");
            database.insert(AletDBHelper.TABLE_ORDER_PRODUCT, null, contentValues);
        }
        cursor.close();

    }

    public void deleteCartByOrderID(String prodID) {
        String whereClause = AletDBHelper.COLUMN_PROD_ID + "=? and " + AletDBHelper.COLUMN_USER_EMAIL + "=?";
        String[] whereArgs = new String[]{prodID, UserModel.getEmail()};
        database.delete(AletDBHelper.TABLE_ORDER_PRODUCT, whereClause, whereArgs);

    }

    public void deleteAllCartByEmail(String email) {
        String whereClause = "user_email" + "=?";
        String[] whereArgs = new String[]{email};
        database.delete(AletDBHelper.TABLE_ORDER_PRODUCT, whereClause, whereArgs);

    }

    /**
     * Update cart database if OrderID is already exist
     *
     * @param order_prodID
     * @param prodID
     * @param storeID
     * @param prodStoreName
     * @param prodName
     * @param prodQuantity
     * @param prodPrice
     * @param prodTotal
     * @param prodDesc
     * @param prodSKU
     * @param prodImage
     * @param stockStatus
     * @param userEmail
     */
    public void upDateCartByID(String order_prodID, String orderID, String prodID, String storeID, String prodStoreName,
                               String prodName, int prodQuantity, String prodPrice, String prodTotal,
                               String prodDesc, String prodSKU, String prodImage, String stockStatus,
                               String userEmail, int prodStockQuantity, int tempProductAvailable) {
        //Get current count of order quantity
        Cursor getOrderQuantity = getCurrentOrderQuantity(prodID, userEmail);
        Cursor getTotalItemPrice = getTotalOrderPrice(prodID, userEmail);


        int currentQuantity = Integer.parseInt(getOrderQuantity.getString(getOrderQuantity.getColumnIndex(getOrderQuantity.getColumnName(0))));
        int currentTotalOrderPrice = Integer.parseInt(getTotalItemPrice.getString(getTotalItemPrice.getColumnIndex(getTotalItemPrice.getColumnName(0))));
        int newQuantity = currentQuantity + prodQuantity;
        double newTotalPrice = currentTotalOrderPrice + Double.parseDouble(prodTotal);

        ContentValues contentValues = new ContentValues();
        contentValues.put(allColumns[1], order_prodID);
        contentValues.put(allColumns[2], prodID);
        contentValues.put(allColumns[3], storeID);
        contentValues.put(allColumns[4], prodStoreName);
        contentValues.put(allColumns[5], prodName);
        contentValues.put(allColumns[6], newQuantity);
        contentValues.put(allColumns[7], prodPrice);
        contentValues.put(allColumns[8], newTotalPrice);
        contentValues.put(allColumns[9], prodDesc);
        contentValues.put(allColumns[10], prodSKU);
        contentValues.put(allColumns[11], prodImage);
        contentValues.put(allColumns[12], stockStatus);
        contentValues.put(allColumns[13], userEmail);
        contentValues.put(allColumns[14], orderID);
        contentValues.put(allColumns[15], prodStockQuantity);
        contentValues.put(allColumns[16], tempProductAvailable);

        String where = "product_id=? and " + AletDBHelper.COLUMN_USER_EMAIL + "=?";
        String[] whereArgs = new String[]{order_prodID, UserModel.getEmail()};

        try {
            database.update(AletDBHelper.TABLE_ORDER_PRODUCT, contentValues, where, whereArgs);
        } catch (Exception e) {
            String error = e.getMessage().toString();
            Log.i("dbUpdateError:", error);
        }
    }

    /**
     * @param order_prodID
     * @param prodQuantity
     * @param itemTotalOrder
     */
    public void updateOrderQuantityPrice(int order_prodID, int prodQuantity, float itemTotalOrder) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(allColumns[6], prodQuantity);
        contentValues.put(allColumns[8], itemTotalOrder);

        String where = "product_id=?";
        String[] whereArgs = new String[]{String.valueOf(order_prodID)};

        try {
            database.update(AletDBHelper.TABLE_ORDER_PRODUCT, contentValues, where, whereArgs);
        } catch (Exception e) {
            String error = e.getMessage().toString();
            Log.i("dbUpdateQuantityError:", error);
        }

    }


    public void updateTempStockAvailableByOrderQuantity(int prodID, int prodQuantity, float itemTotalOrder, int tempProdStock) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(allColumns[6], prodQuantity);
        contentValues.put(allColumns[8], itemTotalOrder);
        contentValues.put(allColumns[16], tempProdStock);

        String where = "product_id=?";
        String[] whereArgs = new String[]{String.valueOf(prodID)};

        try {
            database.update(AletDBHelper.TABLE_ORDER_PRODUCT, contentValues, where, whereArgs);
        } catch (Exception e) {
            String error = e.getMessage().toString();
            Log.i("dbUpdateStockError:", error);
        }
    }

    /**
     * @param orderProdID
     * @param productId
     * @param prodUserEmail
     */
    public void updateAfterSyncByProdIdEmail(String orderProdID, String orderId, String productId, String prodUserEmail) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(allColumns[1], orderProdID);
        contentValues.put(allColumns[14], orderId);

        String where = "product_id=? and " + AletDBHelper.COLUMN_USER_EMAIL + "=?";
        String[] whereArgs = new String[]{String.valueOf(productId), String.valueOf(prodUserEmail)};

        try {
            database.update(AletDBHelper.TABLE_ORDER_PRODUCT, contentValues, where, whereArgs);
        } catch (Exception e) {
            String error = e.getMessage().toString();
            Log.i("dbUpdateSyncError:", error);
        }

    }

    /**
     * SUM all the total item ordered
     *
     * @return
     */
    public Cursor getOrderSubtotal() {
        Cursor cursor = database.rawQuery(
                "SELECT SUM(item_total) FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE user_email =?", new String[]{UserModel.getEmail()});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
            }
        } else {
            Log.i("itemtotalCur:", String.valueOf(cursor));
        }
        return cursor;
    }

    /**
     * Get the total price of the specific item
     *
     * @param orderProdID
     * @return
     */
    public Cursor getItemTotal(String orderProdID) {
        Cursor cursor = database.rawQuery(
                "SELECT item_total FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE product_id =?", new String[]{orderProdID});
        if (cursor != null) {
            if (cursor.moveToFirst()) {

            }
        } else {
        }

        return cursor;
    }

    public static Cursor getCurrentOrderQuantity(String prodID, String Email) {
        Cursor cursor = database.rawQuery(
                "SELECT SUM(quantity) FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE product_id =? and user_email =?", new String[]{prodID, Email});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
            }
        } else {
            Log.i("itemtotalCur:", String.valueOf(cursor));
        }
        return cursor;

    }

    public Cursor getTotalOrderPrice(String prodID, String Email) {
        Cursor cursor = database.rawQuery(
                "SELECT SUM(item_total) FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE product_id =? and user_email =?", new String[]{prodID, Email});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
            }
        } else {
            Log.i("itemtotalCur:", String.valueOf(cursor));
        }
        return cursor;

    }

    public static Cursor getTempProductAvailable(String prodID) {
        Cursor cursor = database.rawQuery(
                "SELECT SUM(temp_available_prod) FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE product_id =? and user_email =?", new String[]{prodID, UserModel.getEmail()});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
            }
        } else {
            Log.i("itemtotalCur:", String.valueOf(cursor));
        }
        return cursor;

    }


    public static Cursor getOriginalStockAvailalbe(String prodID) {
        Cursor cursor = database.rawQuery(
                "SELECT SUM(prod_stock_available) FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE product_id =? and user_email =?", new String[]{prodID, UserModel.getEmail()});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
            }
        } else {
            Log.i("itemtotalCur:", String.valueOf(cursor));
        }
        return cursor;

    }

    /**
     * Query the count of the item in database to display the count of ordered item in badge menu
     *
     * @param Email
     * @return
     */
    public static Cursor cartOrderCount(String Email) {
        Cursor cursor = database.rawQuery(
                "SELECT SUM(quantity) FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE user_email =?", new String[]{Email});
        if (cursor != null) {
            cursor.moveToFirst();

        } else {
            Log.i("itemtotalCur:", String.valueOf(cursor));
            return cursor;
        }
        return cursor;
    }

    /**
     * Select all item in cart
     *
     * @param Email
     * @return
     */

    public static int cartRowItemCount(String Email) {
        String countQuery = "SELECT * FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE user_email=?";
        Cursor cursor = database.rawQuery(countQuery, new String[]{Email});
        Log.i("countQuery:", String.valueOf(cursor));
        int cnt = cursor.getCount();
        cursor.moveToFirst();
        cursor.close();
        return cnt;
    }

    /**
     * Select all ordered item in cart if based on product_id and user_email
     *
     * @param prodID
     * @return
     */
    public static int checkIteminCart(int prodID) {
        String countQuery = "SELECT * FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE product_id=? and user_email=?";
        Cursor cursor = database.rawQuery(countQuery, new String[]{String.valueOf(prodID), UserModel.getEmail()});
        Log.i("countQuery:", String.valueOf(cursor));
        int cnt = cursor.getCount();
        cursor.moveToFirst();
        cursor.close();
        return cnt;
    }

    /**
     * Display all list of item in cart based on user email
     *
     * @return
     */
    public Cursor getAllCartItem() {
        String selectitemCart = "SELECT * FROM " + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE " + AletDBHelper.COLUMN_USER_EMAIL + " =?" + " ORDER BY " + AletDBHelper.COLUMN_PROD_NAME;
        return database.rawQuery(selectitemCart, new String[]{UserModel.getEmail()});
    }

    public ArrayList<ProductModel> getAllOrderList() {
        ArrayList<ProductModel> order_prod = new ArrayList<ProductModel>();
        Cursor cursor = database.rawQuery("SELECT * FROM "
                + AletDBHelper.TABLE_ORDER_PRODUCT + " WHERE " + AletDBHelper.COLUMN_USER_EMAIL + " =?", new String[]{UserModel.getEmail()});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ProductModel pm = cursorToComment(cursor);
            order_prod.add(pm);
            cursor.moveToNext();
        }
        return order_prod;
    }

    // open database
    public void open() {
        database = helper.openDataBase();
    }

    // close databse
    public void close() {
        database.close();
        helper.close();
    }

    private ProductModel cursorToComment(Cursor cursor) {
        ProductModel pm = new ProductModel();
        pm.setOrderEmail(cursor.getString((cursor.getColumnIndex(AletDBHelper.COLUMN_USER_EMAIL))));
        pm.setId(cursor.getString((cursor.getColumnIndex(AletDBHelper.COLUMN_ENTRY_ID))));
        pm.setOrderProductId(cursor.getString((cursor.getColumnIndex(AletDBHelper.COLUMN_PROD_ID))));
        pm.setOrderStoreId(cursor.getString((cursor.getColumnIndex(AletDBHelper.COLUMN_STORE_ID))));
        pm.setOrderProductName(cursor.getString((cursor.getColumnIndex(AletDBHelper.COLUMN_PROD_NAME))));
        pm.setOrderQuantity(cursor.getString((cursor.getColumnIndex(AletDBHelper.COLUMN_PROD_QUANTITY))));

        return pm;
    }

}
