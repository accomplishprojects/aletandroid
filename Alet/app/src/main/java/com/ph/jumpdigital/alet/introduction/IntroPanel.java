package com.ph.jumpdigital.alet.introduction;

/**
 * Created by vidalbenjoe on 9/2/15.
 */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class IntroPanel extends Fragment {

    final static String LAYOUT_ID = "layoutid";

    public static IntroPanel newInstance(int layoutId) {
        IntroPanel pane = new IntroPanel();
        Bundle args = new Bundle();
        args.putInt(LAYOUT_ID, layoutId);
        pane.setArguments(args);
        return pane;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(getArguments().getInt(LAYOUT_ID, -1), container, false);
        return rootView;
    }
}
