package com.ph.jumpdigital.alet.model;

import java.io.Serializable;

/**
 * Created by luigigo on 9/4/15.
 */
public class UserModel implements Serializable {
    public static String GCMToken;
    public static String email, password;
    public static String firstname, lastname, gender, mobile, facebookId;
    public static String birthday;
    public static String addressLabel, streetAdd, city, zipCode;
    public static String user_id, authToken;
    public static Boolean status;
    public static Boolean registering;
    public static String month, day, years;
    public static String latitude, longitude;

    public static String getProfilePic() {
        return profilePic;
    }

    public static void setProfilePic(String profilePic) {
        UserModel.profilePic = profilePic;
    }

    public static String profilePic;

    public static String getGCMToken() {
        return GCMToken;
    }

    public static void setGCMToken(String GCMToken) {
        UserModel.GCMToken = GCMToken;
    }

    public static String getLatitude() {
        return latitude;
    }

    public static void setLatitude(String latitude) {
        UserModel.latitude = latitude;
    }

    public static String getLongitude() {
        return longitude;
    }

    public static void setLongitude(String longitude) {
        UserModel.longitude = longitude;
    }

    public static String getFacebookId() {
        return facebookId;
    }

    public static void setFacebookId(String facebookId) {
        UserModel.facebookId = facebookId;
    }

    public static Boolean getRegistering() {
        return registering;
    }

    public static void setRegistering(Boolean registering) {
        UserModel.registering = registering;
    }

    public static String getMonth() {
        return month;
    }

    public static void setMonth(String month) {
        UserModel.month = month;
    }

    public static String getDay() {
        return day;
    }

    public static void setDay(String day) {
        UserModel.day = day;
    }

    public static String getYears() {
        return years;
    }

    public static void setYears(String years) {
        UserModel.years = years;
    }

    public static String getUser_id() {
        return user_id;
    }

    public static void setUser_id(String user_id) {
        UserModel.user_id = user_id;
    }

    public static String getAuthToken() {
        return authToken;
    }

    public static void setAuthToken(String authToken) {
        UserModel.authToken = authToken;
    }

    public static Boolean getStatus() {
        return status;
    }

    public static void setStatus(Boolean status) {
        UserModel.status = status;
    }


    public static String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        UserModel.email = email;
    }

    public static String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        UserModel.password = password;
    }

    public static String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        UserModel.firstname = firstname;
    }

    public static String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        UserModel.lastname = lastname;
    }

    public static String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        UserModel.gender = gender;
    }

    public static String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        UserModel.mobile = mobile;
    }

    public static String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        UserModel.birthday = birthday;
    }

    public static String getAddressLabel() {
        return addressLabel;
    }

    public static void setAddressLabel(String addressLabel) {
        UserModel.addressLabel = addressLabel;
    }

    public static String getStreetAdd() {
        return streetAdd;
    }

    public void setStreetAdd(String streetAdd) {
        UserModel.streetAdd = streetAdd;
    }

    public static String getCity() {
        return city;
    }

    public void setCity(String city) {
        UserModel.city = city;
    }

    public static String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        UserModel.zipCode = zipCode;
    }
}
