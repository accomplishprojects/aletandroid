package com.ph.jumpdigital.alet.Fragment.OrderPlacement;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.ProductModel;
import com.ph.jumpdigital.alet.model.UserModel;
import com.ph.jumpdigital.alet.registration.GooglePlacesAutocompleteAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils.checkPlayServices;

/**
 * Created by luigigo on 10/27/15.
 */
public class OrderNewAddressFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback {

    public static JSONObject jsonObject, jsonObjectProducts;
    static String streetAddress, city, label, url;
    public static String classIndicator;
    EditText edtLabel;
    TextView txtCantFindCity;
    Button updateAddressBt, cancelSubscribe, subscribeButton;
    ImageView imgShowMap;
    Dialog dialog;
    AutoCompleteTextView edtStreetAdd;
    Spinner spnCity;
    CheckBox chkSaveAddressToAccount;
    Button btnAddNewAddress;
    static ArrayList<HashMap<String, String>> resultList1 = null;
    Toolbar toolbar;
    boolean canAddAddress = false;

    MapFragment supportmapfragment;
    GoogleMap supportMap;
    Marker marker;
    LatLng _point;
    CameraPosition cameraPosition;

    // Google Places API TAGS
    static HttpURLConnection conn = null;
    static StringBuilder jsonResponse = null;
    static String GOOGLE_PLACES_TAG = "GooglePlaces";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.order_addnewaddress, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Add a New Address");

        imgShowMap = (ImageView) mainView.findViewById(R.id.imgShowMap);
        imgShowMap.setOnClickListener(this);
        btnAddNewAddress = (Button) mainView.findViewById(R.id.btnAddNewAddress);
        btnAddNewAddress.setOnClickListener(this);
        edtStreetAdd = (AutoCompleteTextView) mainView.findViewById(R.id.edtStreetAdd);
        edtStreetAdd.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));

        // edtStreetAdd.setOnItemClickListener(this);
        spnCity = (Spinner) mainView.findViewById(R.id.spnCity);
        edtLabel = (EditText) mainView.findViewById(R.id.edtLabel);
        chkSaveAddressToAccount = (CheckBox) mainView.findViewById(R.id.chkSaveAddressToAccount);
        txtCantFindCity = (TextView) mainView.findViewById(R.id.txtCantFindCity);
        txtCantFindCity.setOnClickListener(this);

        return mainView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnAddNewAddress:

                streetAddress = edtStreetAdd.getText().toString();
                city = spnCity.getSelectedItem().toString();
                label = edtLabel.getText().toString();

                if (streetAddress.isEmpty() && label.isEmpty()) {
                    edtStreetAdd.setError("Fields can't be left blank");
                    edtLabel.setError("Fields can't be left blank");
                } else if (streetAddress.isEmpty()) {
                    edtStreetAdd.setError("Fields can't be left blank");
                } else if (label.isEmpty()) {
                    edtLabel.setError("Fields can't be left blank");
                } else {
                    ProductModel.setAddressLabel(label);
                    ProductModel.setAddressStreet(streetAddress);
                    ProductModel.setAddressCity(city);
                    String strDeliveryAddress = ProductModel.getAddressStreet() + " " + ProductModel.getAddressCity();
                    ProductModel.setDeliveryAddress(strDeliveryAddress);
                    //getLatLong();

                    if (UserModel.getLatitude() == null && UserModel.getLongitude() == null) {
                        Toast.makeText(getActivity(), "Please pick a location first", Toast.LENGTH_SHORT).show();

                    } else {
                        if (chkSaveAddressToAccount.isChecked()) {
                            if (canAddAddress) {
                                addUser(UserModel.getLatitude(), UserModel.getLongitude());
                            } else {
                                Toast.makeText(getActivity(), "Sorry selected address is not applicable", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (classIndicator.equals("fromProfileInfoFragment")) {
                                CustomUtils.toastMessage(getActivity(), "Please check the box to save address");
                                CustomUtils.hideDialog();
                            } else {
                                CustomUtils.loadFragment(new OrderSpecialInstructFragment(), true, getActivity());
                                CustomUtils.hideDialog();
                            }
                        }
                    }


                }

                break;

            case R.id.txtCantFindCity:
                streetAddress = edtStreetAdd.getText().toString();
                city = spnCity.getSelectedItem().toString();
                label = edtLabel.getText().toString();

                if (streetAddress.isEmpty() && label.isEmpty()) {
                    edtStreetAdd.setError("Fields can't be left blank");
                    edtLabel.setError("Fields can't be left blank");
                } else if (streetAddress.isEmpty()) {
                    edtStreetAdd.setError("Fields can't be left blank");
                } else if (label.isEmpty()) {
                    edtLabel.setError("Fields can't be left blank");
                } else {
                    showSubscribeDialog();

                }
                break;

            case R.id.imgShowMap:
                imgShowMap.setEnabled(false);
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_map);
                dialog.setCancelable(false);

                Button btnDismissMap = (Button) dialog.findViewById(R.id.btnDismissMap);
                btnDismissMap.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(getActivity(), Locale.getDefault());
                        try {
                            if (_point != null) {
                                addresses = geocoder.getFromLocation(_point.latitude, _point.longitude, 1);
                                String city = addresses.get(0).getAddressLine(1);
                                //CustomUtils.toastMessage(getActivity(), "LongLat:" + city + ": " + _point.latitude + ", " + _point.longitude);
                                Log.i("MarkerPos: ", "LAT: " + marker.getPosition().latitude + " LNG: " + marker.getPosition().longitude);

                                if (city.contains("Quezon City")) {
                                    canAddAddress = true;
                                    spnCity.setSelection(0);
                                    Toast.makeText(getActivity(), "Address located", Toast.LENGTH_SHORT).show();

                                } else if (city.contains("Makati")) {
                                    canAddAddress = true;
                                    spnCity.setSelection(1);
                                    Toast.makeText(getActivity(), "Address located", Toast.LENGTH_SHORT).show();

                                } else if (city.contains("Pasig")) {
                                    canAddAddress = true;
                                    spnCity.setSelection(2);
                                    Toast.makeText(getActivity(), "Address located", Toast.LENGTH_SHORT).show();

                                } else {
                                    canAddAddress = false;
                                    Toast.makeText(getActivity(), "Sorry selected address is not applicable", Toast.LENGTH_SHORT).show();
                                }


                                UserModel.setLatitude(String.valueOf(_point.latitude));
                                UserModel.setLongitude(String.valueOf(_point.longitude));
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Can't get location. Please check your internet connection", Toast.LENGTH_SHORT).show();
                            CustomUtils.hideDialog();
                        }
                        imgShowMap.setEnabled(true);
                        dialog.dismiss();

                        android.app.Fragment fragment = (getFragmentManager().findFragmentById(R.id.map));
                        android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.remove(fragment);
                        ft.commit();
                    }
                });

                if (checkPlayServices(getActivity())) {
                    setUpMap();
                } else {
                    CustomUtils.toastMessage(getActivity(), "Google Services not Installed");
                }

                dialog.show();
                break;
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void setUpMap() {
        supportmapfragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map);

        supportMap = supportmapfragment.getMap();
        supportmapfragment.getMapAsync(this);
        supportMap.getUiSettings().setMapToolbarEnabled(false);
        supportMap.getUiSettings().setZoomControlsEnabled(false);

        GoogleMapOptions options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_SATELLITE)
                .compassEnabled(false)
                .rotateGesturesEnabled(false)
                .tiltGesturesEnabled(true);

        if (UserModel.getLatitude() != null && UserModel.getLongitude() != null) {
            marker = supportMap.addMarker(new MarkerOptions().position(
                    new LatLng(Double.parseDouble(UserModel.getLatitude()), Double.parseDouble(UserModel.getLongitude()))));

            // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(UserModel.getLatitude()), Double.parseDouble(UserModel.getLongitude())))      // Sets the center of the map to Mountain View
                    .zoom(11)                   // Sets the zoom
                    .bearing(360)                // Sets the orientation of the camera to east
//                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
        } else {
            // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
            cameraPosition = new CameraPosition.Builder()
                    .target(Constants.METRO_MANILA_LOC)      // Sets the center of the map to Mountain View
                    .zoom(11)                   // Sets the zoom
                    .bearing(360)                // Sets the orientation of the camera to east
//                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
        }

        supportMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        supportMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                _point = point;
                supportMap.clear();
                marker = supportMap.addMarker(new MarkerOptions().position(
                        new LatLng(point.latitude, point.longitude)));


            }
        });

    }

    public void showSubscribeDialog() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_subscribe);
        dialog.setCancelable(true);
//        dialog.getWindow().setBackgroundDrawable(
//                new ColorDrawable(
//                        android.graphics.Color.TRANSPARENT));

        cancelSubscribe = (Button) dialog.findViewById(R.id.cancelSubscribe);
        cancelSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        subscribeButton = (Button) dialog.findViewById(R.id.subscribeButton);
        subscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubscribeToNewsLetter();
            }
        });

        dialog.show();
    }

    public void SubscribeToNewsLetter() {

        url = Constants.apiURL + Constants.subscribeNewsLetter;
        Log.i("URL", url);
        Constants.volleyTAG = "subscribenewsletter";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.postMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());
        dialog.dismiss();
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            Log.i("sasasa", responseBody);
                            jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");

                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);

                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);

                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);

                                    if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                        CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                        Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                        gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ActivityCompat.finishAffinity(getActivity());
                                        startActivity(gotoLogin);
                                        getActivity().finish();

                                    } else {
                                        CustomUtils.hideDialog();
                                        dialog.dismiss();
//                                        if (classIndicator.equals("fromProfileInfoFragment")) {
//                                            ProfileInfo.page = 4;
//                                            CustomUtils.loadFragment(new ProfileInfo(), true, getActivity());
//                                        } else {
                                        // CustomUtils.loadFragment(new OrderNewAddressFragment(), true, getActivity());
                                        //}

                                    }

                                    CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

                            Log.e("jsonObject", errorResult + "");
                        } else {
//                            CustomUtils.toastMessage(getActivity(), "Please check your internet connection");
                            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.i("Succ", response.toString());

                if (Constants.volleyTAG.equals("getLatLng")) {
                    //Parsing JSONResponse
                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        JSONArray jsonArray = jsonObject.getJSONArray("results");

                        if (jsonObject.getString("status").equals("OK")) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonLocation = jsonArray.getJSONObject(i).getJSONObject("geometry").getJSONObject("location");
                                String strLat = jsonLocation.getString("lat");
                                String strLng = jsonLocation.getString("lng");
                                UserModel.setLatitude(strLat);
                                UserModel.setLongitude(strLng);
                                Log.i("LAT", UserModel.getLatitude() + " index" + i + " LNG " + UserModel.getLongitude());

                                if (chkSaveAddressToAccount.isChecked()) {
                                    addUser(strLat, strLng);
                                } else {
                                    if (classIndicator.equals("fromProfileInfoFragment")) {
                                        CustomUtils.toastMessage(getActivity(), "Please check the box to save address");
                                        CustomUtils.hideDialog();
                                    } else {
                                        CustomUtils.loadFragment(new OrderSpecialInstructFragment(), true, getActivity());
                                        CustomUtils.hideDialog();
                                    }
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), "Invalid Address", Toast.LENGTH_SHORT).show();
                            CustomUtils.hideDialog();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    // Toast.makeText(getActivity(), "Registration successful", Toast.LENGTH_LONG).show();

                    if (response != null) {
//                        CustomUtils.toastMessage(getActivity(), "Address successfully saved");
                        UserModel.setLatitude(null);
                        UserModel.setLongitude(null);
                        Toast.makeText(getActivity(), "Address successfully saved", Toast.LENGTH_SHORT).show();
                        CustomUtils.hideDialog();
                        getActivity().onBackPressed();

                    }
                }
            }

        };
    }

    private void getLatLong() {
        String strDesc = null;
        streetAddress = edtStreetAdd.getText().toString();

        try {
            strDesc = URLEncoder.encode(streetAddress, "utf8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = Constants.GOOGLE_PLACES_BASE_URL + Constants.GOOGLE_GEOCODING + strDesc + Constants.GOOGLE_GEOCODING_API_KEY;
        Constants.volleyTAG = "getLatLng";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), false, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());

    }

    private void addUser(String lat, String lng) {
        /*We set our params as jsonObject instead of HashMap
          because of some difficulties of sending jsonArray data
          into our API*/
        JSONObject jobj = new JSONObject();
        JSONArray jarr = new JSONArray();
        JSONObject params = new JSONObject();

        try {
            // We created a JSONO bject
            jobj.put("street_address", streetAddress);
            jobj.put("city", city);
            jobj.put("label", label);
            jobj.put("province", "");
            jobj.put("latitude", lat);
            jobj.put("longtitude", lng);
            // We add the created JSONObject within the JSONArray
            jarr.put(jobj);
            params.accumulate("user_address", jarr);
            // Logs
            Log.i("jobj", jobj.toString());
            Log.i("JArr", jarr.toString());
            Log.i("params", params.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        url = Constants.apiURL + Constants.addAddress;
        Log.i("URL: ", url);
        Constants.volleyTAG = "addnewaddress";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.postMethod, url,
                null, params,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());
    }


    public static ArrayList<HashMap<String, String>> autocomplete(String input) {

        try {
            jsonResponse = new StringBuilder();
            StringBuilder sb = new StringBuilder(Constants.GOOGLE_PLACES_BASE_URL + Constants.GOOGLE_PLACES_AUTOCOMPLETE);
            sb.append(Constants.GOOGLE_PLACES_API_KEY);
            sb.append("&components=country:ph");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());


            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResponse.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(GOOGLE_PLACES_TAG, "Error processing Places API URL", e);
            return resultList1;
        } catch (IOException e) {
            Log.e(GOOGLE_PLACES_TAG, "Error connecting to Places API", e);
            return resultList1;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Log.i("GOOGLE-PLACES_RESULT", jsonResponse.toString());
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResponse.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList1 = new ArrayList<HashMap<String, String>>();
            for (int i = 0; i < predsJsonArray.length(); i++) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("desc", predsJsonArray.getJSONObject(i).getString("description"));
                hm.put("place_id", predsJsonArray.getJSONObject(i).getString("place_id"));
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println(predsJsonArray.getJSONObject(i).getString("place_id"));
                System.out.println("============================================================");
                resultList1.add(hm);
            }
        } catch (JSONException e) {
            Log.e(GOOGLE_PLACES_TAG, "Cannot process JSON results", e);
        }

        return resultList1;


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

    }


}
