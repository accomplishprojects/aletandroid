package com.ph.jumpdigital.alet.Fragment.HelpAndSupport;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by vidalbenjoe on 09/10/2015.
 */
public class ReportProblemActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText orderNumVal, reportDetailsEdt;
    Button reportProblemBt;
    RadioGroup radioIssueRg;
    RadioButton issueRadioBt;
    String issuetype, issueName1;
    HashMap<String, String> map;

    @Override
    protected void onCreate(Bundle savedIsntanceState) {
        super.onCreate(savedIsntanceState);
        setContentView(R.layout.report_problem_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Report a Problem");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        radioIssueRg = (RadioGroup) findViewById(R.id.feedbackradiogroup);

        orderNumVal = (EditText) findViewById(R.id.orderNumVal);
        reportDetailsEdt = (EditText) findViewById(R.id.reportDetailsEdt);
        reportProblemBt = (Button) findViewById(R.id.reportProblemBt);
        issuetype = "3";
        reportProblemBt.setEnabled(true);
        CustomUtils.showDialog(ReportProblemActivity.this);
        Constants.volleyTAG = "issuetype";
        String url = Constants.apiURL + Constants.issueType;
        VolleyRequest request = new VolleyRequest(ReportProblemActivity.this,
                Constants.getMethod, url,
                map, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                ReportProblemActivity.this, true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);

        radioIssueRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int issueid = checkedId + 1;
                issuetype = String.valueOf(issueid);
            }
        });

        reportProblemBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String edtOrderNum = orderNumVal.getText().toString();
                String edtReportDetails = reportDetailsEdt.getText().toString();

                if (edtOrderNum.isEmpty() && edtReportDetails.isEmpty()) {
                    orderNumVal.setError("Field can't be blank");
                    reportDetailsEdt.setError("Field can't be blank");
                } else if (edtOrderNum.isEmpty()) {
                    orderNumVal.setError("Field can't be blank");
                } else if (edtReportDetails.isEmpty()) {
                    reportDetailsEdt.setError("Field can't be blank");
                } else {
                    reportProblemBt.setEnabled(false);
                    CustomUtils.showDialog(ReportProblemActivity.this);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("issue_type_id", issuetype);
                    map.put("order_id", orderNumVal.getText().toString());
                    map.put("details", reportDetailsEdt.getText().toString());

                    Constants.volleyTAG = "reportproblem";
                    String url = Constants.apiURL + Constants.reportProblem;
                    VolleyRequest request = new VolleyRequest(ReportProblemActivity.this,
                            Constants.postMethod, url,
                            map, null,
                            createRequestSuccessListener(),
                            createRequestErrorListener(),
                            ReportProblemActivity.this, true, Constants.volleyTAG);
                    AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
                }
            }
        });
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {

                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");

                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");
                            Log.i("errorCode:", errorCode);
                            Log.i("errorDesc:", errorDesc);

                            // GET ERROR BY INDEX
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // loop to get the dynamic key
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                    // get the value of the dynamic key
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    // do something here with the value...
                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                    CustomUtils.toastMessage(ReportProblemActivity.this, currentDynamicValue);
                                    reportProblemBt.setEnabled(true);

                                    if (currentDynamicValue == "You must login first") {
                                        CustomUtils.toastMessage(ReportProblemActivity.this, currentDynamicValue);
                                        Intent gotoLogin = new Intent(ReportProblemActivity.this, LoginActivity.class);
                                        gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ActivityCompat.finishAffinity(ReportProblemActivity.this);
                                        startActivity(gotoLogin);
                                        ReportProblemActivity.this.finish();

                                    }

                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

                            Log.e("jsonObject", errorResult + "");
                        } else {
                            Toast.makeText(ReportProblemActivity.this, "Please check your internet connection",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    CustomUtils.hideDialog();
                }
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                CustomUtils.hideDialog();

                if (Constants.volleyTAG == "issuetype") {
                    try {
//                        Log.i("ResponseProb:", response.toString(4));
                        JSONArray issueTypesArr = response.getJSONArray("results");
                        reportProblemBt.setEnabled(true);
                        for (int i = 0; i < issueTypesArr.length(); i++) {
                            JSONObject issueObj = issueTypesArr.getJSONObject(i);
                            issueName1 = issueObj.getString("name");
                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                                    RelativeLayout.LayoutParams.WRAP_CONTENT
                            );

                            params.setMargins(5, 5, 5, 5);
                            issueRadioBt = new RadioButton(ReportProblemActivity.this);
                            issueRadioBt.setId(i);
                            issueRadioBt.setTag(i);
                            issueRadioBt.setText(issueName1);
                            issueRadioBt.setTextColor(Color.parseColor("#585858"));
                            issueRadioBt.setTextSize(17);
                            issueRadioBt.setLayoutParams(params);
                            radioIssueRg.setOrientation(RadioGroup.VERTICAL);//
                            radioIssueRg.addView(issueRadioBt);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        if (response != null) {
                            Boolean status = response.getBoolean("status");
                            if (status.toString().contentEquals("true")) {
                                CustomUtils.toastMessage(ReportProblemActivity.this, "Report Successfully sent!");
                            } else {
                                CustomUtils.toastMessage(ReportProblemActivity.this, "Try Again");
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        ReportProblemActivity.this.finish();
        super.onBackPressed();

    }

}
