package com.ph.jumpdigital.alet;

/**
 * Created by luigigo on 9/15/15.
 */

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.ph.jumpdigital.alet.Utilities.Tools.LruBitmapCache;
import com.ph.jumpdigital.alet.Utilities.UI.FontsOverride;

import io.fabric.sdk.android.Fabric;

public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static synchronized AppController getInstance() {
        return mInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/TitilliumWeb-SemiBold.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/TitilliumWeb-Bold.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/TitilliumWeb-SemiBold.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/TitilliumWeb-SemiBold.ttf");

        Fabric.with(this, new Crashlytics());
        mRequestQueue = Volley.newRequestQueue(AppController.this);

        mInstance = this;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {


        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);


    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}