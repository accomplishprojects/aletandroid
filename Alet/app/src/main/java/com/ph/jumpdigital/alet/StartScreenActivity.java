package com.ph.jumpdigital.alet;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.ph.jumpdigital.alet.Drawer.MainNavigationDrawer;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.GCM.RegistrationIntentService;
import com.ph.jumpdigital.alet.Utilities.SharedPref;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.introduction.IntroPagerActivity;

import static com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils.checkPlayServices;

/**
 * Created by vidalbenjoe on 9/2/15.
 */
public class StartScreenActivity extends Activity implements View.OnClickListener {
    Button getServedBt, loginBt;
    // GCM Variables
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.start_screen_layout);
        sharedPref = new SharedPref(this);
        sharedPref.removePref();
        if (sharedPref.getAUTHToken() != null) {
            Intent mainActivity = new Intent(StartScreenActivity.this, MainNavigationDrawer.class);
            startActivity(mainActivity);
            StartScreenActivity.this.finish();
        } else {
            try {

            } catch (Exception e) {
                e.printStackTrace();
            }
            getServedBt = (Button) findViewById(R.id.getServedBt);
            loginBt = (Button) findViewById(R.id.loginBt);
            getServedBt.setOnClickListener(this);
            loginBt.setOnClickListener(this);
            CustomUtils.getFBHashKey(StartScreenActivity.this);


        }
        /**
         * Setup Registration and Configuration for GCM
         **/
        gcmSetup();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.getServedBt:
                Intent toIntro = new Intent(StartScreenActivity.this, IntroPagerActivity.class);
                startActivity(toIntro);
                StartScreenActivity.this.finish();
                break;
            case R.id.loginBt:
                Intent toLogin = new Intent(StartScreenActivity.this, LoginActivity.class);
                startActivity(toLogin);
                // StartScreenActivity.this.finish();

                break;
        }
    }

    private void gcmSetup() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(Constants.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
//                    Toast.makeText(StartScreenActivity.this, getString(R.string.gcm_send_message), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(StartScreenActivity.this, getString(R.string.token_error_message), Toast.LENGTH_SHORT).show();
                }
            }
        };
        if (checkPlayServices(StartScreenActivity.this)) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
