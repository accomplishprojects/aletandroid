package com.ph.jumpdigital.alet.Fragment.OrderPlacement.OrderHistory;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Fragment.OrderPlacement.OrderDetailsFragment;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.User.LoginActivity;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.ProductModel;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by vidalbenjoe on 03/11/2015.
 */
public class OrderHistoryFragment extends Fragment {
    String url;
    CustomUtils utils = new CustomUtils(getActivity());
    static ArrayList<HashMap<String, String>> arraylist;
    public JSONObject jsonObject;
    private ListView mListView;
    OrderHistoryAdapter adapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ViewOrderHistory();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.listview_reusable, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("History");
        // Inflate the layout for this fragment
        return mainView;

    }

    public void ViewOrderHistory() {
        url = Constants.apiURL + Constants.order_history;
        Constants.volleyTAG = "getorderhistory";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity());
    }


    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "UTF-8");
                            jsonObject = new JSONObject(responseBody);
                            Log.i("JSONResponse", jsonObject.toString());
                            JSONArray errorResult = jsonObject.getJSONArray("error");
                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();

                                while (keys.hasNext()) {
                                    // LOOP TO GET DYNAMIC KEY
                                    String currentDynamicKey = (String) keys.next();
                                    // GET THE VALUE OF DYNAMIC KEY
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                    Toast.makeText(getActivity(), currentDynamicValue,
                                            Toast.LENGTH_LONG).show();
                                    if (currentDynamicValue.toString().contentEquals("You must login first")) {
                                        Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(gotoLogin);
                                        getActivity().finish();
                                    }

                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }

                            Log.e("jsonObject", errorResult + "");
                        } else {
                            CustomUtils.toastMessage(getActivity(), "Please check your internet connection");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e("errorEncoding", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("Volley-SuccViewHistory", response.toString());
                arraylist = new ArrayList<HashMap<String, String>>();

                try {
                    double dblDiscountedAmount = 0;
                    JSONArray resultsArray = response.getJSONArray("results");
                    Constants.status = response.getBoolean("status");

                    for (int i = 0; i < resultsArray.length(); i++) {
                        JSONObject historyResults = resultsArray.getJSONObject(i);
                        JSONArray orderRatings_Arr = historyResults.getJSONArray("rating");
                        JSONArray orderHistory_Arr = historyResults.getJSONArray("order_history");
                        JSONArray orderProdArr = historyResults.getJSONArray("order_product");

                        // Getting list of data to be displayed in Order History
                        if (orderProdArr.length() > 0) {
                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("total", historyResults.getString("total"));
                            map.put("tax", historyResults.getString("tax"));
                            Log.i("TAX", historyResults.getString("tax"));

                            // Getting the value of rating
                            for (int a = 0; a < orderHistory_Arr.length(); a++) {
                                JSONObject ratingsOBJ = orderRatings_Arr.optJSONObject(a);
                                if (ratingsOBJ != null) {
                                    map.put("order_rating", ratingsOBJ.getString("rating"));
                                }
                            }

                            // Getting the value of order_product array
                            for (int j = 0; j < orderHistory_Arr.length(); j++) {
                                JSONObject newOrderHistoryOBj = orderHistory_Arr.getJSONObject(j);
                                JSONObject orderStat = orderHistory_Arr.getJSONObject(j);

                                if (orderStat.has("order_status")) {
                                    JSONObject orderstatusOBJ = orderStat.getJSONObject("order_status");
                                    map.put("order_id", newOrderHistoryOBj.getString("order_id"));
                                    map.put("order_status", orderstatusOBJ.getString("name"));
                                    Log.i("orderstatusOBJ:", String.valueOf(orderstatusOBJ));

                                    for (int k = 0; k < orderProdArr.length(); k++) {
                                        JSONObject OrderProdOBj = orderProdArr.getJSONObject(k);

                                        String strDiscountedAmount = OrderProdOBj.getString("discounted_amount");
                                        if (strDiscountedAmount.contentEquals("null")) {
                                            map.put("discounted_amount", "0.00");
                                        } else {
                                            dblDiscountedAmount += Double.parseDouble(strDiscountedAmount);
                                            map.put("discounted_amount", String.valueOf(dblDiscountedAmount));
                                        }

                                        if (OrderProdOBj.has("product")) {
                                            map.put("hasProduct", "yes");
                                            JSONObject prodObj = OrderProdOBj.getJSONObject("product");
                                            map.put("quantity", OrderProdOBj.getString("quantity"));
                                            map.put("created_at", OrderProdOBj.getString("created_at"));
                                            map.put("order_list", historyResults.toString());
                                        } else {
                                            map.put("hasProduct", "no");
                                        }

                                    }

                                    Log.i("OHF-OrderList", historyResults.toString());
                                    arraylist.add(map);
                                }
                            }
                        }
                    }

                    // Close the progressdialog
                    CustomUtils.hideDialog();
                    if (Constants.status.toString().contentEquals("true")) {
                        if (UserModel.getAuthToken() != null) {
                            mListView = (ListView) getActivity().findViewById(R.id.reusableListView);
                            adapter = new OrderHistoryAdapter(getActivity(), arraylist);
                            // Pass the results into OrderHistoryAdapter.java
                            // Set the adapter to the ListView
                            if (mListView != null) {
                                mListView.setAdapter(adapter);
                            }

                            // Close the progressdialog
                            CustomUtils.hideDialog();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public class OrderHistoryAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflater;
        ArrayList<HashMap<String, String>> data;
        ProductModel productModel = new ProductModel();
        double dblDiscountedAmount = 0;

        public OrderHistoryAdapter(Context context,
                                   ArrayList<HashMap<String, String>> arraylist) {
            this.context = context;
            data = arraylist;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final ViewHolder holder;

            if (view == null) {
                holder = new ViewHolder();
                inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.order_history_layout, viewGroup, false);

                holder.orderTrackID = (TextView) view.findViewById(R.id.orderTrackID);
                holder.orderHistoryStat = (TextView) view.findViewById(R.id.orderHistoryStat);
                holder.orderitemTotal = (TextView) view.findViewById(R.id.orderitemPrice);
                holder.orderDate = (TextView) view.findViewById(R.id.orderDate);
                holder.orderitemCount = (TextView) view.findViewById(R.id.orderitemCount);
                holder.orderHistoryRatingBar = (RatingBar) view.findViewById(R.id.orderHistoryRatingBar);
                view.setTag(holder);

            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.resultp = data.get(i);
            holder.orderTrackID.setText("Order #" + holder.resultp.get("order_id"));
            Log.i("HistoryTotal", holder.resultp.get("discounted_amount") + "");
            if (holder.resultp.get("total") != null) {
                dblDiscountedAmount = Double.parseDouble(holder.resultp.get("discounted_amount"));
                Double dblTotalAddedTax = (Double.parseDouble(holder.resultp.get("total")) + Double.parseDouble(holder.resultp.get("tax")) - dblDiscountedAmount);

                holder.orderitemTotal.setText("₱ " + CustomUtils.decimalFormat(getActivity(), dblTotalAddedTax));
            } else {
                holder.orderitemTotal.setText("N/A");
            }

            holder.orderitemCount.setText(holder.resultp.get("quantity") + " items");
            Date now = new Date();
            DateFormat sdf;
            sdf = new SimpleDateFormat("dd LLLL yyyy");
            String strDate = sdf.format(now);
            strDate = sdf.format(now);
            holder.orderDate.setText(strDate);

            if (holder.resultp.get("order_rating") != null) {
                holder.orderHistoryRatingBar.setRating(Float.parseFloat(holder.resultp.get("order_rating")));
            } else {
                holder.orderHistoryRatingBar.setRating(0);
            }
            holder.orderHistoryStat.setText(holder.resultp.get("order_status"));
            holder.orderHistoryStat.setTextColor(getResources().getColor(R.color.md_red_600));
            if (holder.orderHistoryStat.getText().toString().contentEquals("Pending")) {
                holder.orderHistoryStat.setTextColor(getResources().getColor(R.color.md_red_600));
            } else if (holder.orderHistoryStat.getText().toString().contentEquals("Cart")) {
                holder.orderHistoryStat.setTextColor(getResources().getColor(R.color.md_deep_orange_600));
            } else if (holder.orderHistoryStat.getText().toString().contentEquals("Completed")) {
                holder.orderHistoryStat.setTextColor(getResources().getColor(R.color.md_green_600));
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.resultp.get("hasProduct") != null) {
                        if (holder.resultp.get("hasProduct").equals("yes")) {
                            ProductModel.setOrderDetails(holder.resultp.get("order_list"));
                            OrderDetailsFragment.strIndicator = "orderHistory";
                            CustomUtils.loadFragment(new OrderDetailsFragment(), true, context);
                        } else {
                            CustomUtils.toastMessage(getActivity(), "There is no available data for product");
                        }
                    } else {
                        CustomUtils.toastMessage(getActivity(), "There is no available data for product");
                    }
                }
            });

            return view;
        }

        public class ViewHolder {
            TextView orderTrackID;
            TextView orderHistoryStat;
            TextView orderDate;
            TextView orderitemTotal;
            TextView orderitemCount;
            RatingBar orderHistoryRatingBar;
            HashMap<String, String> resultp = new HashMap<String, String>();
        }
    }

}
