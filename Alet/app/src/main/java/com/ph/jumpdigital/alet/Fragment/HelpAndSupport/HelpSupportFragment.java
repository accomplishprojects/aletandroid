package com.ph.jumpdigital.alet.Fragment.HelpAndSupport;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.ph.jumpdigital.alet.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vidalbenjoe on 14/12/2015.
 */
public class HelpSupportFragment extends Fragment {

    // Array of strings storing country names
    String[] text = new String[]{
            "Disclaimer",
            "Terms of Agreement",
            "Privacy and Policy",
            "FAQ",
            "REPORT"
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.listview_reusable, container, false);

        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < text.length; i++) {
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("txt", "" + text[i]);
            aList.add(hm);
        }

        // Keys used in Hashmap
        String[] from = {"txt"};
        // Ids of views in listview_layout
        int[] to = {R.id.helpSupportTxt};

        // Instantiating an adapter to store each items
        // R.layout.help_support_layout defines the layout of each item
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.help_support_layout, from, to);
        // Getting a reference to listview of main.xml layout file
        ListView listView = (ListView) mainView.findViewById(R.id.reusableListView);


        // Setting the adapter to the listView
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String Slecteditem = text[+position];

                if (position == 0) {
                    FAQsActivity content = new FAQsActivity();
                    FAQsActivity.Content = "disclaimer";
                    Intent toFAQ = new Intent(getActivity(), FAQsActivity.class);
                    startActivity(toFAQ);
                } else if (position == 1) {


                } else if (position == 2) {

                } else if (position == 3) {
                    FAQsActivity content = new FAQsActivity();
                    FAQsActivity.Content = "faq";
                    Intent toFAQ = new Intent(getActivity(), FAQsActivity.class);
                    startActivity(toFAQ);
                } else if (position == 4) {
                    Intent toReport = new Intent(getActivity(), ReportProblemActivity.class);
                    startActivity(toReport);
                }

            }
        });

        return mainView;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {

        super.onPause();
    }

    /**
     * Called before the activity is destroyed.
     */
    @Override
    public void onDestroy() {

        super.onDestroy();
    }

}