package com.ph.jumpdigital.alet.Fragment.ItemListing;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ph.jumpdigital.alet.AppController;
import com.ph.jumpdigital.alet.Database.AletDBHelper;
import com.ph.jumpdigital.alet.Database.AletDataSource;
import com.ph.jumpdigital.alet.Fragment.Inventory.SearchProductFragment;
import com.ph.jumpdigital.alet.Fragment.OrderPlacement.OrderType;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Networking.CustomHurlStack;
import com.ph.jumpdigital.alet.Utilities.Networking.OkHttpHurlStack;
import com.ph.jumpdigital.alet.Utilities.Networking.VolleyRequest;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.ProductModel;
import com.ph.jumpdigital.alet.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by vidalbenjoe on 06/11/2015.
 */
public class CartViewFragment extends Fragment implements View.OnClickListener {

    public static CartCursorAdapter cartAdapter;
    public AletDBHelper aletdbHelper;
    public static AletDataSource aletDBSource;
    private ListView cartlistView;
    ProductModel productModel = new ProductModel();
    JSONObject jsonObject, params;
    JSONArray jsonArrayStoreId;
    RelativeLayout relListOfItems, relNoCartItems;
    int rowCount;
    EditText edtPromoCode;
    ProgressBar pbPromoCode;
    ImageView imgPromoCode;
    Button checkoutbt, btnNoCartItems;
    public static TextView subtotalValue;
    Cursor cursor = null;
    String strPromoCode, TAG = "CartViewFragment";
    Toolbar toolbar;
    static Context context;
    View view;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        aletdbHelper = new AletDBHelper(getActivity());
        aletDBSource = new AletDataSource(getActivity());
        // rowCount = AletDataSource.countItemCart(UserModel.getEmail());
        aletDBSource.open();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.shopping_cart_listview, container, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Shopping Cart");
        // Inflate the layout for this fragment
        return view;
    }

    public void onViewCreated(View view, Bundle savebundleInstances) {
        super.onViewCreated(view, savebundleInstances);


        imgPromoCode = (ImageView) view.findViewById(R.id.imgPromoCode);
        pbPromoCode = (ProgressBar) view.findViewById(R.id.pbPromoCode);
        pbPromoCode.setVisibility(View.INVISIBLE);
        subtotalValue = (TextView) view.findViewById(R.id.subtotalValue);
        checkoutbt = (Button) view.findViewById(R.id.checkoutbt);
        checkoutbt.setOnClickListener(this);
        cartlistView = (ListView) view.findViewById(R.id.shoppingcartlv);
        relListOfItems = (RelativeLayout) view.findViewById(R.id.relListOfItems);
        relNoCartItems = (RelativeLayout) view.findViewById(R.id.relNoCartItems);
        btnNoCartItems = (Button) view.findViewById(R.id.btnNoCartItems);
        btnNoCartItems.setOnClickListener(this);
        edtPromoCode = (EditText) view.findViewById(R.id.edtPromoCode);
        strPromoCode = edtPromoCode.getText().toString();

        edtPromoCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                imgPromoCode.setVisibility(View.INVISIBLE);
                pbPromoCode.setVisibility(View.VISIBLE);
                //checkoutbt.setEnabled(false);
                if (edtPromoCode.getText().toString().length() >= 3) {
                    AppController.getInstance().getRequestQueue().cancelAll("jsonformat");
                    AppController.getInstance().getRequestQueue().cancelAll("validatepromocode");
                    ArrayList<ProductModel> productModels = aletDBSource.getAllOrderList();
                    JSONArray jsonArr = new JSONArray();
                    jsonArrayStoreId = new JSONArray();
                    try {
                        for (int l = 0; l < productModels.size(); l++) {
                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put("quantity", productModels.get(l).getOrderQuantity());
                            jsonObj.put("product_id", productModels.get(l).getOrderProductId());
                            jsonArr.put(jsonObj);

                            JSONObject jsonObjStoreId = new JSONObject();
                            jsonObjStoreId.put("store_id", productModels.get(l).getOrderStoreId());
                            jsonArrayStoreId.put(jsonObjStoreId);

                        }
                        ProductModel.setOrderListToAddCart(jsonArr.toString());
                        ProductModel.setOrderPromoCode(null);
                        Log.i("", "----------------------------------------------------");
                        Log.i("orderListToAddCart", jsonArr.toString());
                        Log.i("jsonArrayStoreId", jsonArrayStoreId.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    addToCart();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtPromoCode.getText().toString().isEmpty()) {
                    imgPromoCode.setVisibility(View.INVISIBLE);
                    pbPromoCode.setVisibility(View.INVISIBLE);
                    checkoutbt.setEnabled(true);
                }
            }
        });

        checkIfCartHasItems();
        checkoutbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtPromoCode.getText().toString().isEmpty() || edtPromoCode.getText().toString().length() < 3)
                    productModel.setOrderPromoCode(null);

                Log.i(TAG, "The promo code is: " + ProductModel.getOrderPromoCode());

                OrderType orderFrag = new OrderType();
                OrderType.page = 0;
                CustomUtils.loadFragment(orderFrag, true, getActivity());

                ArrayList<ProductModel> productModels = aletDBSource.getAllOrderList();
                JSONArray jsonArr = new JSONArray();

                try {
                    for (int i = 0; i < productModels.size(); i++) {
                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("quantity", productModels.get(i).getOrderQuantity());
                        jsonObj.put("product_id", productModels.get(i).getOrderProductId());
                        jsonArr.put(jsonObj);
                    }

                    ProductModel.setOrderListToAddCart(jsonArr.toString());
                    Log.i("orderListToAddCart", jsonArr.toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        cartlistView.setLongClickable(true);
        cartlistView.setItemsCanFocus(true);
        cartlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("clicked on item: ", String.valueOf(position));
            }
        });


        cartlistView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> adapterView, View view, int i, long l) {
                Cursor cursor = aletDBSource.getAllCartItem();
                cursor.moveToPosition(i);
                final String strOrderProdId, strOrderId, strProdId;
                strOrderProdId = (cursor.getString(cursor.getColumnIndex(AletDBHelper.COLUMN_ORDER_PROD_ID)));
                strOrderId = (cursor.getString(cursor.getColumnIndex(AletDBHelper.COLUMN_ORDER_ID)));
                strProdId = (cursor.getString(cursor.getColumnIndex(AletDBHelper.COLUMN_PROD_ID)));
                //Toast.makeText(getActivity(), String.valueOf(i), Toast.LENGTH_LONG).show();
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                // Setting Dialog Title
                alertDialog.setTitle("Confirm Delete...");
                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want to delete this item?");
                // Setting Icon to Dialog
//                alertDialog.setIcon(R.drawable.delete);
                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteCartItems(strOrderId, strProdId, strOrderProdId);
                        CustomUtils.showDialog(getActivity());
//                        aletDBSource.deleteCartByOrderID(strProdId);
//                        cartAdapter.notifyDataSetChanged();
//                        displayItemCart();
                    }
                });
                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });
                // Showing Alert Message
                alertDialog.show();

                return false;
            }
        });

    }

    private void addToCart() {
        try {
            JSONArray jsonArr = new JSONArray(ProductModel.getOrderListToAddCart());
            params = new JSONObject();
            params = new JSONObject();
            params.put("add_cart", jsonArr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("JSON", params.toString());
        Constants.volleyTAG = "jsonformat";
        String url = Constants.apiURL + Constants.order_product;
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.postMethod, url,
                null, params,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        //CustomUtils.showDialog(getActivity());
    }


    private void deleteCartItems(String orderID, final String prodID, String orderProdID) {
        HttpStack httpStack;
        if (Build.VERSION.SDK_INT > 19) {
            httpStack = new CustomHurlStack();
        } else if (Build.VERSION.SDK_INT >= 9 && Build.VERSION.SDK_INT <= 19) {
            httpStack = new OkHttpHurlStack();
        } else {
            httpStack = new HttpClientStack(AndroidHttpClient.newInstance("Android"));
        }
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), httpStack);
        String url = Constants.apiURL + Constants.deleteCartItems;
        Constants.volleyTAG = "deleteCartItems";
        //String URL = "http://alet.jumpdigital.ph/api/order_product_delete";
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("order_id", orderID);
            jsonObject.put("product_id", prodID);
            jsonObject.put("order_product_id", orderProdID);
            Log.i(TAG + " DeleteCartItems", String.valueOf(jsonObject));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest deleteRequest = new JsonObjectRequest(
                com.android.volley.Request.Method.DELETE,
                url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("OkHttp-ItemDeleted: ", response.toString());

                aletDBSource.deleteCartByOrderID(prodID);
                cartAdapter.notifyDataSetChanged();
                checkIfCartHasItems();
                CustomUtils.hideDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("ErrorMessage: ", error.getMessage());
                try {
                    if (error.getMessage() != null) {
                        String responseBody = String.valueOf(error.getMessage());
                        JSONObject jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");

                        String errorCode = jsonObject.getString("error_code");
                        String errorDesc = jsonObject.getString("error_description");

                        Log.i("errorCode:", errorCode);
                        Log.i("errorDesc:", errorDesc);

                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();

                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                // do something here with the value...
                                // Toast.makeText(getActivity(), currentDynamicValue, Toast.LENGTH_SHORT).show();
                                // CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                aletDBSource.deleteCartByOrderID(prodID);
                                cartAdapter.notifyDataSetChanged();
                                checkIfCartHasItems();
                                CustomUtils.hideDialog();

                            }
                            Log.i("Error-JSONResponse", "" + errorResult.toString());
                        }

                        Log.e("jsonObject", errorResult + "");
                    } else {
                        Toast.makeText(getActivity(), "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                        CustomUtils.hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                    Toast.makeText(getActivity(), "Please check your internet connection",
                            Toast.LENGTH_SHORT).show();
                }
                CustomUtils.hideDialog();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("x-auth-token", UserModel.getAuthToken());
                return headers;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                String json;
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    try {
                        json = new String(volleyError.networkResponse.data,
                                HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
                    } catch (UnsupportedEncodingException e) {
                        return new VolleyError(e.getMessage());
                    }
                    return new VolleyError(json);
                }
                return volleyError;
            }
        };
        requestQueue.add(deleteRequest);
    }

    private void checkIfCartHasItems() {
        cursor = AletDataSource.cartOrderCount(UserModel.getEmail());
        cursor.close();
        rowCount = AletDataSource.cartRowItemCount(UserModel.getEmail());
        if (rowCount > 0) {
            relNoCartItems.setVisibility(View.INVISIBLE);
            relListOfItems.setVisibility(View.VISIBLE);
            displayItemCart();
        } else {
            relNoCartItems.setVisibility(View.VISIBLE);
            relListOfItems.setVisibility(View.INVISIBLE);
        }

    }

    private Response.ErrorListener createRequestErrorListener() {

        return new Response.ErrorListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyLog.d("VolleyError", error.getLocalizedMessage() + error.getMessage());
                try {
                    if (error.networkResponse != null) {
                        CustomUtils.hideDialog();
                        String responseBody = new String(error.networkResponse.data, "UTF-8");
                        jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");

                        String errorCode = jsonObject.getString("error_code");
                        String errorDesc = jsonObject.getString("error_description");

                        Log.i("errorCode:", errorCode);
                        Log.i("errorDesc:", errorDesc);
                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();
                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                // do something here with the value...
                                //Toast.makeText(getActivity(), currentDynamicValue, Toast.LENGTH_SHORT).show();
                                CustomUtils.toastMessage(getActivity(), currentDynamicValue);
                                pbPromoCode.setVisibility(View.GONE);
                                imgPromoCode.setVisibility(View.VISIBLE);
                                final int sdk = android.os.Build.VERSION.SDK_INT;
                                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                    imgPromoCode.setBackgroundDrawable(getResources().getDrawable(R.drawable.notvalidated));
                                    //checkoutbt.setEnabled(false);
                                } else {
                                    imgPromoCode.setBackground(getResources().getDrawable(R.drawable.notvalidated));
                                    //checkoutbt.setEnabled(false);
                                }

                                if (edtPromoCode.getText().toString().isEmpty()) {
                                    imgPromoCode.setVisibility(View.INVISIBLE);
                                    pbPromoCode.setVisibility(View.INVISIBLE);
                                    checkoutbt.setEnabled(true);
                                }
                            }
                            Log.i("Error-JSONResponse", "" + errorResult.toString());
                        }
                        Log.e("jsonObject-Cart", errorResult + "");
                    } else {
                        Toast.makeText(getActivity(), "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                        pbPromoCode.setVisibility(View.GONE);
                        imgPromoCode.setVisibility(View.VISIBLE);
                        final int sdk = Build.VERSION.SDK_INT;
                        if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
                            imgPromoCode.setBackgroundDrawable(getResources().getDrawable(R.drawable.notvalidated));
                            productModel.setOrderPromoCode(null);
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                imgPromoCode.setBackground(getResources().getDrawable(R.drawable.notvalidated));
                                productModel.setOrderPromoCode(null);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.e("errorEncoding", "" + e);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(JSONObject response) {

                Log.i("DELETEDVOLLEY", response.toString());

                if (Constants.volleyTAG.equals("validatepromocode")) {
                    Log.d("PROMOCODE: ", edtPromoCode.getText().toString());
                    Log.d("Success-ValidatedCode", response.toString());
                    pbPromoCode.setVisibility(View.GONE);
                    imgPromoCode.setVisibility(View.VISIBLE);
                    final int sdk = android.os.Build.VERSION.SDK_INT;
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        imgPromoCode.setBackgroundDrawable(getResources().getDrawable(R.drawable.validated));
                        productModel.setOrderPromoCode(edtPromoCode.getText().toString());
                        checkoutbt.setEnabled(true);
                    } else {
                        imgPromoCode.setBackground(getResources().getDrawable(R.drawable.validated));
                        productModel.setOrderPromoCode(edtPromoCode.getText().toString());
                        checkoutbt.setEnabled(true);
                    }
                } else {
                    try {
                        jsonObject = new JSONObject(response.getJSONObject("results").toString());
                        String strOrderId = jsonObject.getString("id");
                        if (!edtPromoCode.getText().toString().isEmpty())
                            validatePromoCode(strOrderId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    private void validatePromoCode(String orderId) {
        try {
            Cursor subtotalCur = aletDBSource.getOrderSubtotal();
            params = new JSONObject();
            params.put("order_id", orderId);
            if (subtotalCur != null)
                params.put("total_price", subtotalCur.getString(subtotalCur.getColumnIndex(subtotalCur.getColumnName(0))));
            params.put("code", edtPromoCode.getText().toString());
            params.put("store", jsonArrayStoreId);
            Log.i(TAG, params + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.apiURL + Constants.validate_promo_code;
        Constants.volleyTAG = "validatepromocode";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.postMethod, url,
                null, params,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
    }

    private void displayItemCart() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {

                Log.i("DisplayCartItems: ", "true");
                Cursor listCursor = aletDBSource.getAllCartItem();
                Cursor subtotalCur = aletDBSource.getOrderSubtotal();
                if (subtotalCur != null || listCursor != null) {
                    try {
                        DecimalFormat formatter;
                        formatter = new DecimalFormat("#,###.00");

                        float prodPrice = Float.parseFloat(subtotalCur.getString(subtotalCur.getColumnIndex(subtotalCur.getColumnName(0))));
                        String priceFormat = formatter.format(prodPrice);
                        subtotalValue.setText("₱ " + priceFormat);
                        CustomUtils.hideDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                        CustomUtils.hideDialog();
                        subtotalValue.setText("₱ " + subtotalCur.getString(subtotalCur.getColumnIndex(subtotalCur.getColumnName(0))));
                    }
                } else {
                    CustomUtils.hideDialog();
                }


                cartAdapter = new CartCursorAdapter(getActivity(), listCursor);
                cartlistView.setAdapter(cartAdapter);

                Log.i("SUBTOTALCURSOR", subtotalCur.toString());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        aletDBSource.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        displayItemCart();
    }

    public static void updateSubtotal() {
        Cursor subtotalCur = aletDBSource.getOrderSubtotal();
        if (subtotalCur != null) {
            String subtotalStr = subtotalCur.getString(subtotalCur.getColumnIndex(subtotalCur.getColumnName(0)));
            subtotalValue.setText("₱ " + CustomUtils.decimalFormat(context, Double.parseDouble(subtotalStr)));
        }
        ProductModel.setOrderSubtotal(Integer.parseInt(subtotalCur.getString(subtotalCur.getColumnIndex(subtotalCur.getColumnName(0)))));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNoCartItems:
                SearchProductFragment.browseInvPos = 2;
                CustomUtils.loadFragment(new SearchProductFragment(), true, getActivity());
                break;
        }
    }

    public class CartCursorAdapter extends CursorAdapter {
        private RequestQueue mRequestQueue;

        private LayoutInflater mInflater;
        Context mContext;

        public CartCursorAdapter(Context context, Cursor c) {
            super(context, c);
            mInflater = LayoutInflater.from(context);
            mContext = context;

        }

        @Override
        public long getItemId(int id) {
            return id;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        View rowView;

        class ViewHolder {
            ImageView itemtobuyImg;
            TextView storeNameTxtView;
            TextView itemtobuytitle;
            TextView itemtotalprice;
            TextView numofunit;

            LinearLayout increunitBT;
            LinearLayout decreunitBt;
            LinearLayout delItemCartBt;

            public int position;
            int prodID;
            int itemCount;
            int orderProdID;
            int orderID;
            float prodPrice;
            int prodTotal;
            String storename, imageURL, prodtitle;
            int prodStockQuantity;

            ProgressBar cartImageProgress;

        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            mInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = mInflater.inflate(R.layout.shoppingcart_main_layout, viewGroup, false);
            ViewHolder holder = new ViewHolder();

            holder.cartImageProgress = (ProgressBar) rowView.findViewById(R.id.cartImageProgress);

            holder.storeNameTxtView = (TextView) rowView.findViewById(R.id.storeName);
            holder.itemtobuyImg = (ImageView) rowView.findViewById(R.id.itemtobuyImg);
            holder.itemtobuytitle = (TextView) rowView.findViewById(R.id.itemtobuytitle);
            holder.itemtotalprice = (TextView) rowView.findViewById(R.id.itemtotalprice);
            holder.numofunit = (TextView) rowView.findViewById(R.id.numofunit);

            holder.increunitBT = (LinearLayout) rowView.findViewById(R.id.increunitBT);
            holder.decreunitBt = (LinearLayout) rowView.findViewById(R.id.decreunitBt);
            holder.delItemCartBt = (LinearLayout) rowView.findViewById(R.id.delItemCartBt);
            rowView.setTag(holder);
            return rowView;
        }

        @Override
        public void bindView(final View view, final Context context, final Cursor cursor) {
            final ViewHolder holder = (ViewHolder) view.getTag();
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    // Code here will run in UI thread
                    holder.cartImageProgress.setVisibility(View.INVISIBLE);
                }
            });
            holder.itemtobuyImg.setVisibility(View.VISIBLE);
            holder.orderProdID = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(1))));
            holder.prodID = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(2))));
            holder.storename = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(4)));
            holder.prodtitle = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(5)));
            holder.itemCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(6))));//prodQuantity
            holder.prodPrice = Float.parseFloat(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(7))));
            holder.prodTotal = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(8))));
            holder.imageURL = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(11)));

            holder.orderID = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(14))));

            if (cursor.getString(cursor.getColumnIndex(cursor.getColumnName(15))) != null) {
                holder.prodStockQuantity = Integer.parseInt(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(15))));
            }

            ProductModel.setProdOrderID(String.valueOf(holder.prodID));
            //String itemFormatedPrice = priceFormat.format(holder.prodTotal);
            String itemFormatedPrice = CustomUtils.decimalFormat(getActivity(), holder.prodTotal);

            holder.storeNameTxtView.setText(holder.storename);
            holder.itemtobuytitle.setText(holder.prodtitle);
            holder.numofunit.setText(holder.itemCount + " Qty");
            holder.itemtotalprice.setText("₱ " + itemFormatedPrice);

            String imgURL = Constants.baseURL + holder.imageURL;

            Glide.with(getActivity())
                    .load(imgURL)
                    .placeholder(R.drawable.defaultimage)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.itemtobuyImg);

//            mRequestQueue = Volley.newRequestQueue(context);
//            mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
//                private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
//
//                public void putBitmap(String url, Bitmap bitmap) {
//                    mCache.put(url, bitmap);
//                }
//
//                public Bitmap getBitmap(String url) {
//                    return mCache.get(url);
//                }
//            });
////
////            /**
////             //mImageLoader = AppController.getInstance().getImageLoader();
////             Set the URL of the image that should be loaded into this view, and
////             specify the ImageLoader that will be used to make the request.
////             */
//            holder.itemtobuyImg.setImageUrl(imgURL, mImageLoader);//Volley


//            imageLoader.DisplayImage(imgURL, holder.itemtobuyImg);

//            if (holder.itemtobuyImg.getDrawable().isVisible()) {
//                holder.cartImageProgress.setVisibility(View.INVISIBLE);
//                holder.itemtobuyImg.setVisibility(View.VISIBLE);
//            }
            holder.increunitBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = cursor.getPosition();
                    cursor.moveToPosition(pos);
                    //orderQuantity in database
//                    ProductListFragment.productTotalQuantity;
                    if (holder.itemCount < holder.prodStockQuantity) {
                        holder.itemCount++;
                        int ItemCount = holder.itemCount;
                        float priceTotal;
                        Cursor getitemTotal = aletDBSource.getItemTotal(String.valueOf(holder.prodID));
                        int prodTotal = Integer.parseInt((getitemTotal.getString(getitemTotal.getColumnIndex(getitemTotal.getColumnName(0)))));
                        priceTotal = prodTotal + holder.prodPrice;
                        CartViewFragment.aletDBSource.updateOrderQuantityPrice(holder.prodID, ItemCount, priceTotal);
                        CartViewFragment.updateSubtotal();
                        holder.numofunit.setText(holder.itemCount + " Qty");
                        holder.itemtotalprice.setText("₱ " + CustomUtils.decimalFormat(context, priceTotal));
                        Cursor tempAvailableProd = AletDataSource.getTempProductAvailable(String.valueOf(holder.prodID));
                        int ProductStock = Integer.parseInt(tempAvailableProd.getString(tempAvailableProd.getColumnIndex(tempAvailableProd.getColumnName(0))));
                        ProductStock--;
                        CartViewFragment.aletDBSource.updateTempStockAvailableByOrderQuantity(holder.prodID, holder.itemCount, priceTotal, ProductStock);

                    } else if (holder.itemCount == holder.prodStockQuantity) {
                        CustomUtils.toastMessage(getActivity(), "Sorry, we only have " + holder.prodStockQuantity + " " + holder.prodtitle + " in stock");
                    }
                }
            });

            holder.decreunitBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("itemCountTap: ", String.valueOf(holder.itemCount));
                    int pos = cursor.getPosition();
                    cursor.moveToPosition(pos);
                    if (holder.itemCount > 1) {
                        //orderQuantity in database
                        holder.itemCount--;
                        int IC = holder.itemCount;
                        Cursor getitemTotal = aletDBSource.getItemTotal(String.valueOf(holder.prodID));
                        int prodTotal = Integer.parseInt((getitemTotal.getString(getitemTotal.getColumnIndex(getitemTotal.getColumnName(0)))));
                        float priceTotal = prodTotal - holder.prodPrice;
                        holder.numofunit.setText(holder.itemCount + " Qty");
                        holder.itemtotalprice.setText("₱ " + CustomUtils.decimalFormat(context, priceTotal));
                        CartViewFragment.aletDBSource.updateOrderQuantityPrice(holder.prodID, IC, priceTotal);
                        CartViewFragment.updateSubtotal();
                        Cursor tempAvailableProd = AletDataSource.getTempProductAvailable(String.valueOf(holder.prodID));
                        int ProductStock = Integer.parseInt(tempAvailableProd.getString(tempAvailableProd.getColumnIndex(tempAvailableProd.getColumnName(0))));
                        ProductStock++;
                        CartViewFragment.aletDBSource.updateTempStockAvailableByOrderQuantity(holder.prodID, holder.itemCount, priceTotal, ProductStock);
                    }
                }
            });

            holder.delItemCartBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    // Setting Dialog Title
                    alertDialog.setTitle("Confirm Delete...");
                    // Setting Dialog Message
                    alertDialog.setMessage("Are you sure you want to delete this item?");
                    // Setting Icon to Dialog
//                  alertDialog.setIcon(R.drawable.delete);
                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deleteCartItems(holder.orderID, holder.prodID, holder.orderProdID);
                            CustomUtils.showDialog(context);
                        }
                    });
                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    // Showing Alert Message
                    alertDialog.show();
                }
            });
        }


        private void deleteCartItems(int orderID, final int prodID, int orderProdID) {
            HttpStack httpStack;
            if (Build.VERSION.SDK_INT > 19) {
                httpStack = new CustomHurlStack();
            } else if (Build.VERSION.SDK_INT >= 9 && Build.VERSION.SDK_INT <= 19) {
                httpStack = new OkHttpHurlStack();
            } else {
                httpStack = new HttpClientStack(AndroidHttpClient.newInstance("Android"));
            }
            RequestQueue requestQueue = Volley.newRequestQueue(mContext, httpStack);
            String url = Constants.apiURL + Constants.deleteCartItems;
            Constants.volleyTAG = "deleteCartItems";
            final JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("order_id", orderID);
                jsonObject.put("product_id", prodID);
                jsonObject.put("order_product_id", orderProdID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest deleteRequest = new JsonObjectRequest(
                    com.android.volley.Request.Method.DELETE,
                    url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    aletDBSource.deleteCartByOrderID(String.valueOf(prodID));
                    cartAdapter.notifyDataSetChanged();
                    checkIfCartHasItems();
                    CustomUtils.hideDialog();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        if (error.getMessage() != null) {
                            String responseBody = String.valueOf(error.getMessage());
                            JSONObject jsonObject = new JSONObject(responseBody);
                            JSONArray errorResult = jsonObject.getJSONArray("error");
                            String errorCode = jsonObject.getString("error_code");
                            String errorDesc = jsonObject.getString("error_description");
                            //get errror by index
                            for (int i = 0; i < errorResult.length(); i++) {
                                JSONObject errorObj = errorResult.getJSONObject(i);
                                Iterator keys = errorObj.keys();
                                while (keys.hasNext()) {
                                    // loop to get the dynamic key
                                    String currentDynamicKey = (String) keys.next();
                                    Log.i("Error-currentDynamicKey", "" + currentDynamicKey);
                                    // get the value of the dynamic key
                                    String currentDynamicValue = errorObj.getString(currentDynamicKey);

                                    // do something here with the value...
                                    Log.i("Error-DynamicKey", "" + currentDynamicKey);
                                    Log.i("Error-DynamicValue", "" + currentDynamicValue);
                                    //Toast.makeText(getActivity(), currentDynamicValue, Toast.LENGTH_SHORT).show();

                                    aletDBSource.deleteCartByOrderID(String.valueOf(prodID));
                                    cartAdapter.notifyDataSetChanged();
                                    Log.i("RowCount-holder", rowCount + "");
                                    checkIfCartHasItems();
                                    CustomUtils.hideDialog();
                                }
                                Log.i("Error-JSONResponse", "" + errorResult.toString());
                            }
                            Log.e("jsonObject", errorResult + "");
                        } else {
                            Toast.makeText(mContext, "Please check your internet connection",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("errorJSON", "" + e);
                        Toast.makeText(mContext, "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                    }
                    CustomUtils.hideDialog();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    headers.put("Accept", "application/json");
                    headers.put("x-auth-token", UserModel.getAuthToken());
                    return headers;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    String json;
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        try {
                            json = new String(volleyError.networkResponse.data,
                                    HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
                        } catch (UnsupportedEncodingException e) {
                            return new VolleyError(e.getMessage());
                        }
                        return new VolleyError(json);
                    }
                    return volleyError;
                }
            };

            requestQueue.add(deleteRequest);

        }

        private void updateCartItem() {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    Cursor listCursor = aletDBSource.getAllCartItem();
                    Cursor subtotalCur = aletDBSource.getOrderSubtotal();

                    subtotalValue.setText("₱ " + CustomUtils.decimalFormat(getActivity(), Double.parseDouble(subtotalCur.getString(subtotalCur.getColumnIndex(subtotalCur.getColumnName(0))))));
                    cartAdapter = new CartCursorAdapter(getActivity(), listCursor);
                    cartlistView.setAdapter(cartAdapter);
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        aletDBSource.close();
    }

}
