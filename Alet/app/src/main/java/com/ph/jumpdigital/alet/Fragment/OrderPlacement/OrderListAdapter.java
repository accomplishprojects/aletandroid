package com.ph.jumpdigital.alet.Fragment.OrderPlacement;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.Utilities.Constants;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class OrderListAdapter extends BaseAdapter {

    public static String indicator;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    private RequestQueue mRequestQueue;
    //com.ph.jumpdigital.alet.Utilities.UI.ImageLoaders.ImageLoader imageLoader;
    com.android.volley.toolbox.ImageLoader mImageLoader;

    public OrderListAdapter(Context context,
                            ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        //imageLoader = new ImageLoader(context);

        Log.i("Data-OrderListAdapter", data.toString());
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        final ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.order_details_item, viewGroup, false);
            // View itemView = inflater.inflate(R.layout.order_details_item, viewGroup, false);
            holder.txtStoreName = (TextView) view.findViewById(R.id.txtStoreName);
            holder.txtProdName = (TextView) view.findViewById(R.id.txtProdName);
            holder.txtSnippet = (TextView) view.findViewById(R.id.txtSnippet);
            holder.txtPrice = (TextView) view.findViewById(R.id.txtPrice);
            holder.productItemThumb = (ImageView) view.findViewById(R.id.imgProduct);


        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.resultp = data.get(position);

        view.setTag(holder);
        holder.formatter = new DecimalFormat("#,###.00");
        float prodPrice = Float.parseFloat(holder.resultp.get("price"));
        float prodTotalPrice = Float.parseFloat(holder.resultp.get("product_total"));
        String priceFormat = holder.formatter.format(prodPrice);
        String totalPriceFormat = holder.formatter.format(prodTotalPrice);

        String strStoreName, strProdName, strSnippet, strProdPrice;
        strStoreName = holder.resultp.get("store_name");
        strProdName = holder.resultp.get("title");
        strSnippet = holder.resultp.get("quantity") + " Qty(₱ " + CustomUtils.decimalFormat(context, Double.parseDouble(holder.resultp.get("price"))) + " per Qty)";
        strProdPrice = "₱ " + CustomUtils.decimalFormat(context, Double.parseDouble(holder.resultp.get("product_total")));


        holder.txtStoreName.setText(strStoreName);
        holder.txtProdName.setText(strProdName);
        holder.txtSnippet.setText(strSnippet);
        holder.txtPrice.setText(strProdPrice);

        String imgURL = Constants.baseURL + holder.resultp.get("prod_imageurl");
        Log.i("ImageURL: ", imgURL);
        Glide.with(context)
                .load(imgURL)
                .placeholder(R.drawable.defaultimage)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.productItemThumb);

//        Log.i("ImageURL: ", imgURL);
//        mRequestQueue = Volley.newRequestQueue(context);
//        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
//            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
//
//            public void putBitmap(String url, Bitmap bitmap) {
//                mCache.put(url, bitmap);
//            }
//
//            public Bitmap getBitmap(String url) {
//                return mCache.get(url);
//            }
//        });
//
//        /**
//         //mImageLoader = AppController.getInstance().getImageLoader();
//         Set the URL of the image that should be loaded into this view, and
//         specify the ImageLoader that will be used to make the request.
//         */
//
//        holder.productItemThumb.setImageUrl(imgURL, mImageLoader);
//        holder.productItemThumb.setDefaultImageResId(R.drawable.defaultimage);
//        holder.productItemThumb.setErrorImageResId(R.drawable.defaulterror);

 //       imageLoader.DisplayImage(imgURL, holder.productItemThumb);


        return view;
    }

    // Setting up Fragment Class
    public void setFragment(Class<? extends android.app.Fragment> fragmentClass) {
        try {
            android.app.Fragment fragment = fragmentClass.newInstance();
            android.app.FragmentManager fragmentManager = ((FragmentActivity) context).getFragmentManager();
            android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_content, fragment, fragment.getClass().getSimpleName());
            fragmentTransaction.commit();
        } catch (Exception ex) {
            Log.e("setFragment", ex.getMessage());
        }

    }

    public class ViewHolder {
        TextView txtStoreName, txtProdName, txtSnippet, txtPrice;
        HashMap<String, String> resultp = new HashMap<String, String>();
        ImageView productItemThumb;
        DecimalFormat formatter;

    }


}
