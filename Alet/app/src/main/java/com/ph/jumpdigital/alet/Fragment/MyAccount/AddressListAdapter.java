package com.ph.jumpdigital.alet.Fragment.MyAccount;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ph.jumpdigital.alet.Fragment.OrderPlacement.OrderSpecialInstructFragment;
import com.ph.jumpdigital.alet.R;
import com.ph.jumpdigital.alet.Utilities.Tools.CustomUtils;
import com.ph.jumpdigital.alet.model.ProductModel;

import java.util.ArrayList;
import java.util.HashMap;

public class AddressListAdapter extends BaseAdapter {

    public static String indicator;
    Context context;
    Activity activity;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<>();
    String addressId, streetAddress, city, zipcode, strDefaultAddress;

    public AddressListAdapter(Context context,
                              ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        activity = (Activity) context;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.profile_saved_address_item, viewGroup, false);

            holder.addressTitle = (TextView) view.findViewById(R.id.addressTitle);
            holder.streetAddTxt = (TextView) view.findViewById(R.id.streetAddTxt);
            holder.cityZipTxt = (TextView) view.findViewById(R.id.cityZipTxt);
            holder.relDefAddressIndicator = (RelativeLayout) view.findViewById(R.id.relDefAddressIndicator);


        } else {
            holder = (ViewHolder) view.getTag();
        }

        view.setTag(holder);
        resultp = data.get(position);

        streetAddress = resultp.get("street_address");
        city = resultp.get("city");
        zipcode = resultp.get("zip_code");
        strDefaultAddress = resultp.get("default_address");

        holder.addressTitle.setText(resultp.get("label"));
        holder.streetAddTxt.setText(streetAddress);
        holder.cityZipTxt.setText(resultp.get("zip_code").toString().isEmpty() ? city : city + "");

        if (strDefaultAddress.equals("1")) {
            holder.relDefAddressIndicator.setVisibility(View.VISIBLE);
        } else {
            holder.relDefAddressIndicator.setVisibility(View.INVISIBLE);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View arg0) {
                // Get the position
                resultp = data.get(position);

                if (indicator.equals("paymentMethod")) {
                    // Toast.makeText(context, "paymentMethod", Toast.LENGTH_SHORT).show();

                    addressId = resultp.get("id");
                    streetAddress = resultp.get("street_address");
                    city = resultp.get("city");
                    zipcode = resultp.get("zip_code");

                    ProductModel.setAddressId(addressId);
                    ProductModel.setDeliveryAddress(streetAddress + ", " + city + " ");
                    Log.i("Address ID", ProductModel.getAddressId());
                    Log.i("Delivery Address", ProductModel.getDeliveryAddress());
                    CustomUtils.loadFragment(new OrderSpecialInstructFragment(), true, context);

                } else {
                    // Toast.makeText(context, "savedAddress", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(activity, UpdateAddressActivity.class);
                    // Pass all data
                    intent.putExtra("id", resultp.get("id"));
                    intent.putExtra("label", resultp.get("label"));
                    intent.putExtra("street_address", resultp.get("street_address"));
                    intent.putExtra("city", resultp.get("city"));
                    intent.putExtra("zip_code", resultp.get("zip_code"));
                    intent.putExtra("latitude", resultp.get("latitude"));
                    intent.putExtra("longtitude", resultp.get("longtitude"));
                    // Start UpdateAddressActivity Class
                    activity.startActivity(intent);
//                    activity.getFragmentManager().popBackStack();
                }
            }
        });

        return view;
    }

    public class ViewHolder {
        TextView addressTitle, streetAddTxt, cityZipTxt;
        RelativeLayout relDefAddressIndicator;
    }


}
